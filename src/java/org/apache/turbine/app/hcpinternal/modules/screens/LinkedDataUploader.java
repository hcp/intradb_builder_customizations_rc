package org.apache.turbine.app.hcpinternal.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class LinkedDataUploader extends SecureReport {

	@Override
	public void finalProcessing(RunData data, Context context) {
		
		XnatProjectdata proj = null;
		XnatSubjectdata subj = null;
		XnatSubjectassessordata expt = null;
		
		if (om instanceof XnatProjectdata) {
			
			proj=((XnatProjectdata)om);
			
		} else  if (om instanceof XnatSubjectdata) {
			
			subj=((XnatSubjectdata)om);
			proj=subj.getProjectData();
			
		} else  if (om instanceof XnatSubjectassessordata) {
			
			expt=(XnatSubjectassessordata)om;
			subj=expt.getSubjectData();
			proj=expt.getProjectData();
			
		}
		if (proj != null) {
			StringBuilder path = new StringBuilder("/services/import?import-handler=LINKED_DATA");
			if (proj!=null) {
				path.append("&project=").append(proj.getId());
			}
			if (subj!=null) {
				path.append("&subject=").append(subj.getLabel());
			}
			if (expt!=null) {
				path.append("&experiment=").append(expt.getId());
			}
			context.put("user_path", path);
			context.put("serverRoot", TurbineUtils.GetRelativeServerPath(data));
		}
		
	}
	
}


package org.apache.turbine.app.hcpinternal.modules.screens;

import java.util.ArrayList;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;

import org.nrg.hcp.utils.ScreenUtils;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

import com.google.common.collect.Lists;

public class PipelineScreen_ReleaseHCP extends DefaultPipelineScreen{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PipelineScreen_ReleaseHCP.class);

    public void finalProcessing(RunData data, Context context) {
    	
        XnatSubjectdata sub = new XnatSubjectdata(item);
    	try {
    		//search for workflow entries with a matching ID
			org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	        cc.addClause("wrk:workflowData.ID",sub.getId());
	        org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(cc,null,false);
	        //Sort by Launch Time
	        @SuppressWarnings("unchecked")
			List<XFTItem> workitems = items.getItems("wrk:workflowData.launch_time","DESC");
	        for (XFTItem wrk:workitems) {
	        	WrkWorkflowdata workflow = new WrkWorkflowdata(wrk);
	        	if (workflow.isActive()) {
	        		context.put("activePipelines", true);
	        		return;
	        	}
	        }
    	} catch (Exception e) {	}
   		context.put("activePipelines", false);
    	
    }

}

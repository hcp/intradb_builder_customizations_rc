package org.apache.turbine.app.hcpinternal.modules.screens;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xnat.utils.UserUtils;

public class SatSurveyUploader extends SecureReport {

	@Override
	public void finalProcessing(RunData data, Context context) {
		
		XDATUser user=TurbineUtils.getUser(data);
		XnatProjectdata proj = null;
		XnatSubjectdata subj = null;
		XnatSubjectassessordata expt = null;
		
		File dir;
		
		if (om instanceof XnatProjectdata) {
			
			proj=((XnatProjectdata)om);
			
		} else  if (om instanceof XnatSubjectdata) {
			
			subj=((XnatSubjectdata)om);
			proj=subj.getProjectData();
			
		} else  if (om instanceof XnatSubjectassessordata) {
			
			expt=(XnatSubjectassessordata)om;
			subj=expt.getSubjectData();
			proj=expt.getProjectData();
			
		}
		if (proj != null) {
			StringBuilder path = new StringBuilder("/services/import?import-handler=SatSurvey");
			if (proj!=null) {
				path.append("&project=").append(proj.getId());
			}
			if (subj!=null) {
				path.append("&subject=").append(subj.getLabel());
			}
			if (expt!=null) {
				path.append("&experiment=").append(expt.getId());
			}
			context.put("user_path", path);
			context.put("serverRoot", TurbineUtils.GetRelativeServerPath(data));
		}
		
	}
	
}


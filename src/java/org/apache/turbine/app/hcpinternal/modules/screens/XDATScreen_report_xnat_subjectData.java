/*
 * org.nrg.xnat.turbine.modules.screens.XDATScreen_report_xnat_subjectData
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
 */
package org.apache.turbine.app.hcpinternal.modules.screens;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.hcp.utils.ScreenUtils;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.search.CriteriaCollection;
import com.google.common.collect.Lists;

/**
 * @author Tim
 *
 */
public class XDATScreen_report_xnat_subjectData extends SecureReport {
	static Logger logger = Logger.getLogger(XDATScreen_report_xnat_subjectData.class);
	
    /* (non-Javadoc)
     * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    public void finalProcessing(RunData data, Context context) {
        // For Intradb, the release pipeline is a subject level pipeline
       	ArrayList<WrkWorkflowdata> workflows = Lists.newArrayList();
        try {
            XnatSubjectdata sub = new XnatSubjectdata(item);
            context.put("subject",sub);
        	//search for workflow entries with a matching ID
			org.nrg.xft.search.CriteriaCollection cc = new CriteriaCollection("AND");
	        cc.addClause("wrk:workflowData.ID",sub.getId());
	        org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(cc,null,false);
	        //Sort by Launch Time
	        @SuppressWarnings("unchecked")
			List<XFTItem> workitems = items.getItems("wrk:workflowData.launch_time","DESC");
	        for (XFTItem wrk:workitems) {
	        	WrkWorkflowdata workflow = new WrkWorkflowdata(wrk);
	    		String projectId = ((String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter("project",data));
	    		// It's preferable to get project from the search string parameters, but if it's a search box search,
	    		// we'll just pull workflows for the owning project.  This may not be optimal for shared-in subjects,
	    		// but it should work for our current purposes, where pipelines are running on subjects that are owned 
	    		// by the project.  If we had a need to see workflows from the owning project for pipelines running on
	    		// a shared project, we would need to handle that differently
	    		if (projectId == null) {
	    			projectId = sub.getProject();
	    		}
	        	// Only load active, failed workflows at the subject level.  Just want to know if pipeline is currently running or failed.
	        	if ( (workflow.isActive() || workflow.getStatus().equalsIgnoreCase("Error") || workflow.getStatus().equalsIgnoreCase("Failed")) &&
	        			ScreenUtils.getProjectPipelineNames(projectId,item.getXSIType()).contains(workflow.getOnlyPipelineName()) 
	        			) {
	        		workflows.add(workflow);
	        	}
	        }
        } catch (Exception e) {
            logger.error("",e);
        }
        context.put("workflows",workflows);
    }
}

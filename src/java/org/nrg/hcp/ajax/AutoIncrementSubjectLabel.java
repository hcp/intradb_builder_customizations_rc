package org.nrg.hcp.ajax;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.turbine.services.rundata.RunDataService;
import org.apache.turbine.services.rundata.TurbineRunDataFacade;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.TurbineException;
import org.nrg.hcp.labelutils.GetterNotFoundException;
import org.nrg.hcp.labelutils.SubjectLabelGetterI;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.DBPoolException;

public class AutoIncrementSubjectLabel {
	
	static org.apache.log4j.Logger logger = Logger.getLogger(AutoIncrementSubjectLabel.class);
	
	private final String DEFAULT_GETTER = "org.nrg.hcp.labelutils.DefaultSubjectLabelGetter";
			
	private String project;
	private boolean generateSubjectLabel;
	private boolean subjectLabelReadOnly;
	private String subjectLabelPattern="";
	private String subjectLabelClass=DEFAULT_GETTER;
		
	public void getNextLabel(HttpServletRequest request, HttpServletResponse response, ServletConfig sc) { 
		
		try {
			
			this.initializeVars(request, response, sc);
			
			// Immediately expire headers
			response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			response.setHeader("Pragma","no-cache"); //HTTP 1.0
			response.setDateHeader ("Expires", -1); //prevents caching at the proxy serve
		
			// return if project generate does not subject label
			if (!generateSubjectLabel) {
				response.getWriter().write("");
				return;
			}
			// build getter and run method to return label
			final SubjectLabelGetterI getter = buildIdGetter(request,response,sc,Class.forName(subjectLabelClass));
			getter.getNextLabel(subjectLabelPattern,subjectLabelReadOnly);
		
		} catch (Exception e) {
			
			handleException(e,response);
			
		}
		
	}
		
	public void getReadOnlyStatus(HttpServletRequest request, HttpServletResponse response, ServletConfig sc) { 
		
		try {
			
			this.initializeVars(request, response, sc);
			
			// Immediately expire headers
			response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
			response.setHeader("Pragma","no-cache"); //HTTP 1.0
			response.setDateHeader ("Expires", -1); //prevents caching at the proxy serve
			
			response.getWriter().write(String.valueOf(subjectLabelReadOnly));
			
		} catch (TurbineException e) {
			handleException(e,response);
		} catch (SQLException e) {
			handleException(e,response);
		} catch (DBPoolException e) {
			handleException(e,response);
		} catch (IOException e) {
			handleException(e,response);
		}
		
	}
	
	private void handleException(Exception e,HttpServletResponse response) {
		
		logger.error("",e);
		try {	
			response.getWriter().write("");
		} catch (IOException e1) {
			logger.error("",e1);
		}
		
	}
	
	private void initializeVars(HttpServletRequest request, HttpServletResponse response, ServletConfig sc) throws TurbineException, SQLException, DBPoolException, IOException { 
		
		// Only one response type for this class & default getter
		response.setContentType("text/plain");
		
		this.project = request.getParameter("project");
		this.generateSubjectLabel=false;
		this.subjectLabelReadOnly=false;
		
		// Run query to retrieve latest (max) ID
		final RunDataService rundataService = TurbineRunDataFacade.getService();
		final RunData data = rundataService.getRunData(request, response, sc);
		final XDATUser user=TurbineUtils.getUser(data);
		
		// retrieve subject label projectData fields from DB
		final StringBuilder querySB=new StringBuilder("select name,field from xnat_projectdata_field where fields_field_xnat_projectdata_id='");
		querySB.append(project);
		querySB.append("' and name in ('generatesubjectlabel','subjectlabelclass','subjectlabelpattern','subjectlabelreadonly')"); 
		XFTTable slTable=XFTTable.Execute(querySB.toString(), user.getDBName(), user.getLogin());
		if (slTable==null || !slTable.hasMoreRows()) {
			// return if no project level auto-generate fields defined
			response.getWriter().write("");
			return;
		}
		// assing DB values to method values
		while (slTable.hasMoreRows()) {
			final Object[] rarray = slTable.nextRow();
			
			if (rarray[0].toString().equalsIgnoreCase("generatesubjectlabel")) {
				generateSubjectLabel=((rarray[1]!=null && rarray[1].toString().equals("1")));
			} else if (rarray[0].toString().equalsIgnoreCase("subjectlabelreadonly")) {
				subjectLabelReadOnly=((rarray[1]!=null && rarray[1].toString().equals("1")));
			} else if (rarray[0].toString().equalsIgnoreCase("subjectlabelpattern")) {
				if (rarray[1]!=null)
					subjectLabelPattern=rarray[1].toString();
			} else if (rarray[0].toString().equalsIgnoreCase("subjectlabelclass")) {
				if (rarray[1]!=null && rarray[1].toString().trim().length()>0)
					subjectLabelClass=rarray[1].toString();
			}	
			
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private SubjectLabelGetterI buildIdGetter(HttpServletRequest request, HttpServletResponse response, ServletConfig sc,Class getter) 
		throws ClassNotFoundException, GetterNotFoundException, SecurityException, NoSuchMethodException,
				IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		
		if(getter==null){
			throw new GetterNotFoundException("Unknown IdGetterI implementation specified: " + getter,new IllegalArgumentException());
		}
		
		final Constructor con=getter.getConstructor(HttpServletRequest.class,HttpServletResponse.class,ServletConfig.class);
		return (SubjectLabelGetterI)con.newInstance(request,response,sc);
	}
	
}

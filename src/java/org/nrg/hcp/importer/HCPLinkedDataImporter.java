package org.nrg.hcp.importer;

/*
 *   IMPORTANT!!!  When adding support for new types of files, it is important to determine whether the file should be handled as TEXT or binary,
 *   OTHERWISE FILES COULD BE CORRUPTED!  See relevent code in the "processFiles" method.  
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.StopTagInputHandler;
import org.dcm4che2.util.TagUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.dcm.DicomUtils;
import org.nrg.hcp.utils.HCPScriptExecUtils;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.BaseXnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xdat.om.base.auto.AutoXnatMrsessiondata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xnat.helpers.dicom.SiemensShadowHeader;
import org.nrg.xnat.helpers.file.StoredFile;
import org.nrg.xnat.helpers.resource.XnatResourceInfo;
import org.nrg.xnat.helpers.resource.direct.DirectExptResourceImpl;
import org.nrg.xnat.helpers.resource.direct.DirectScanResourceImpl;
import org.nrg.xnat.restlet.actions.importer.ImporterHandlerA;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xnat.utils.ResourceUtils;

import java.util.zip.ZipOutputStream;

import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.zip.TarUtils;
import org.nrg.xft.utils.zip.ZipI;
import org.nrg.xft.utils.zip.ZipUtils;
import org.mozilla.universalchardet.UniversalDetector;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Matches E-prime, physio, eye-tracker and head-tracker files to scans and uploads as a scan resource
 * @author Mike Hodge <hodgem@mir.wustl.edu>
 *
 */
public class HCPLinkedDataImporter extends ImporterHandlerA implements Callable<List<String>> {

	static final String[] ZIP_EXT={".zip",".jar",".rar",".ear",".gar",".xar"};
	static final String[] TASK_KEYS={"SOCIAL","RELATIONAL:RELATION","MOTOR","WM","GAMBLING","EMOTION","LANGUAGE",
									"StroopBas","StroopPro","StroopRea","CuedtsBas","CuedtsPro","CuedtsRea",
									"AxcptBas","AxcptPro","AxcptRea","SternBas","SternPro","SternRea"};
	
	static final String[] BOLD_TYPES={"tfMRI","BOLD"};
	
	static final String[] DMRI_TYPES={"dMRI"};
	static final String[] MOTION_TYPES={"T1w","T2w","tfMRI","rfMRI","BOLD","dMRI"};
	static final String[] EYETRACKER3T_TYPES={"rfMRI"};
	static final String[] EYETRACKERDMC_TYPES={"rfMRI"};
	static final String[] MATLAB7T_TYPES={"tfMRI","rfMRI"};
	static final String[] EYETRACKER7T_TYPES={"tfMRI","rfMRI"};
	static final String[] AUDIO_TYPES={"tfMRI"};
	
	static final String DICOM_EXT = ".dcm";
	
	public static final String DICOM_RESOURCE_LABEL="DICOM";
	public static final String LINKED_RESOURCE_LABEL="LINKED_DATA";
	static final String LINKED_RESOURCE_FORMAT="MISC";
	static final String LINKED_RESOURCE_CONTENT="RAW";
	static final String LINKED_CATXML_PREFIX="linkeddata_";
	static final String LINKED_CATXML_EXT="_catalog.xml";
	
	static final String PRACTICE_RESOURCE_LABEL="UNLINKED_DATA";
	static final String PRACTICE_RESOURCE_FORMAT="MISC";
	static final String PRACTICE_RESOURCE_CONTENT="RAW";
	static final String PRACTICE_CATXML_PREFIX="unlinkeddata_";
	static final String PRACTICE_CATXML_EXT="_catalog.xml";
	
	static final String MOTION_FN_REGEX="^[L0-9][S0-9]\\d{4}_.*_\\d{8}_\\d{6}_\\d{6}.*";
	static final String MOTION_EXT = "txt";
	
	static final String MATLAB7T_FN_REGEX="^[L0-9][S0-9]\\d{4}_(RET|FIX|MOV)[#]?[0-9]+_.*";
	static final String RETINOTOPY_FN_REGEX="^[L0-9][S0-9]\\d{4}_RET[#]?[0-9]+_.*";
	static final String MOVIE_FN_REGEX="^[L0-9][S0-9]\\d{4}_MOV[#]?[0-9]+_.*";
	static final String FIX_FN_REGEX="^[L0-9][S0-9]\\d{4}_FIX[#]?[0-9]+_.*";
	static final String MATLAB7T_SD_REGEX="^.*_(RET|FIX|MOVIE|REST|MOV)?[0-9]+_.*";
	static final String[] MATLAB7T_EXT = {".mat"};
	
	static final String EYETRACKER7T_FN_REGEX="^[L0-9][S0-9]\\d{4}_(RET|FIX|MOV)eyetrack[#]?[0-9]+_.*";
	static final String EYETRACKER7T_SD_REGEX="^.*_(RET|FIX|MOVIE|REST|MOV)?[0-9]+_.*";
	static final String EYETRACKERDMC_FN_REGEX="^[0-9][0-9]\\d{4}[rpbRPB]\\d?[.].*";
	static final String EYETRACKER3T_FN_REGEX="^[L0-9][S0-9]\\d{4}[abx]\\d?[.].*";
	static final String[] EYETRACKER_EXT = {".edf",".summary"};
	static final String[] EYETRACKERBINARY_EXT = {".edf"};
	
	static final String AUDIO_SD_REGEX="^.*_(Stroop|Cuedts|Axcpt|Stern).*";
	static final String AUDIO_FN_REGEX="^.*AudioRecord.*";
	static final String[] AUDIO_EXT = {".wav"};
	
	// Adding underscore to lbl pattern because it's part of file name
	static final String EPRIME_FN_REGEX="^[L0-9][S0-9]\\d\\d\\d\\d_.*";
	
	static final String EPRIME_DATE_FORMAT="MM-dd-yyyy HH:mm:ss";
	static final String EPRIME_TXT_EXT = "txt";
	static final String EPRIME_EDAT_EXT = "edat2";
	// Indicator for working memory files
	static final String WM_INDICATOR = "_WM";
	// Indicator for working memory files
	static final String REC_INDICATOR = "_REC";
	// Indicator for practice session files
	static final String PRACTICE_INDICATOR = "_runp";
	
	static final String PHYSIO_PREFIX = "Physio_log_";
	// NOTE:  We aren't keeping Physio ecg files.
	// 2016-03-08.  The DMC project is collectin///////thy definition/g them (along with ext2 files).  It won't hurt to keep them.
	// We just won't distribute them for HCP.
	static final String[] PHYSIO_EXT = { "ext","puls","resp","ecg","ext2" };
	
	static final String HEADTRACKER_PREFIX = "Motion_";
	static final String[] HEADTRACKER_EXT = { "log" };
	
	// E-prime file recieved thus far have been 16-bit 
	static final String DEFAULT_ENCODING = "UTF-16LE";
	static final String EVENT_REASON = "Upload Scan-Linked Files";

	static Logger logger = Logger.getLogger(HCPLinkedDataImporter.class);
	
	private static enum AcquisitionTimes {
		ACQ_START, ACQ_END
	}
	
	private static enum ScanModType {
		EPRIME, PHYSIO, MOTION, EYETRACKER, HEAD_TRACKER, RETINOTOPY, MOVIE, FIX, MATLAB, AUDIO, OTHER 
	}

	private final FileWriterWrapperI fw;
	private final XDATUser user;
	final Map<String,Object> params;
		private XnatProjectdata proj;
		private XnatMrsessiondata exp;
	private List<String> returnList = new ArrayList<String>();;
		
	private Map<String,DirectScanResourceImpl> scanModifiers = new HashMap<String,DirectScanResourceImpl>(); 
	private DirectExptResourceImpl sessionModifier;
	
	private static final String[] columns = {
			"tag1",  // tag name, never empty.
			"tag2",  // for normal, non-sequence DICOM tags this is the empty string.
			"vr",	// DICOM Value Representation  
			"value", // Contents of the tag
			"desc"	// Description of the tag
	};
	private final Map<Integer,Set<String>> fields = new HashMap<Integer,Set<String>>();
	Map<XnatImagescandataI,List<ScanModType>> modifiedScans;
	
	boolean anyEprime;
	boolean anyPhysio;
	
		
	/**
	 * 
	 * @param listenerControl
	 * @param u
	 * @param session
	 * @param overwrite:   'append' means overwrite, but preserve un-modified content (don't delete anything)
	 *                      'delete' means delete the pre-existing content.
	 * @param additionalValues: should include project (subject and experiment are expected to be found in the archive)
	 */
	public HCPLinkedDataImporter(Object listenerControl, XDATUser u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
		this.user=u;
		this.fw=fw;
		this.params=params;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<String> call() throws ClientException, ServerException {
		verifyAndGetExperiment();
		try {
			processUpload();
			this.completed("Success");
			return returnList;
		} catch (ClientException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (ServerException e) {
			logger.error("",e);
			this.failed(e.getMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("",e);
			throw new ServerException(e.getMessage(),new Exception());
		}
	}

	private void verifyAndGetExperiment() throws ClientException {
		String projID=null;
		if (params.get("project")!=null) {
			projID=params.get("project").toString();
		}
		String subjLbl=null;
		if (params.get("subject")!=null) {
			subjLbl=params.get("subject").toString();
		}
		if (params.get("experiment") == null) {
			clientFailed("ERROR:  experiment parameter (containing experiment label) must be supplied for import");
		}
		String expLbl=params.get("experiment").toString();
		
		this.exp =XnatMrsessiondata.getXnatMrsessiondatasById(expLbl, user, false);
		if(exp==null){
			ArrayList<XnatMrsessiondata> al=XnatMrsessiondata.getXnatMrsessiondatasByField("xnat:mrSessionData/label",expLbl, user, false);
			// Using iterator here because removing within for/next can be unpredictable
			Iterator<XnatMrsessiondata> aIter = al.iterator();
			while (aIter.hasNext()) {
				XnatMrsessiondata mrsess = aIter.next();
				if (projID!=null && !mrsess.getProject().equalsIgnoreCase(projID)) {
					aIter.remove();
					continue;
				}
				if (subjLbl!=null && 
						!(mrsess.getSubjectData().getId().equalsIgnoreCase(subjLbl) || mrsess.getSubjectData().getLabel().equalsIgnoreCase(subjLbl))) {
					aIter.remove();
					continue;
				}
				exp=mrsess;
			}
			if (al.size()>1) {
				clientFailed("ERROR:  Multiple sessions match passed parameters. Consider using experiment assession number or supplying project/subject parameters ");
			}
		}
			if (exp==null) {
			clientFailed("ERROR:  Could not find matching experiment for import");
		}
			proj=exp.getProjectData();
	}
	

	@SuppressWarnings("deprecation")
	private void clientFailed(final String fmsg) throws ClientException {
		this.failed(fmsg);
		throw new ClientException(fmsg,new Exception());
	}

	private void processUpload() throws ClientException,ServerException {
		
		String cachePath = ArcSpecManager.GetInstance().getGlobalCachePath();
		
		boolean doProcess = false;
		// Multiple uploads are allowed to same space (processing will take place when process parameter=true).  Use specified build path when
		// one is given, otherwise create new one
		String specPath=null;
		boolean invalidSpecpath = false;
		if (params.get("buildPath")!=null) {
			// If buildpath parameter is specified and valid, use it
			specPath=params.get("buildPath").toString();
			if (specPath.indexOf(cachePath)>=0 && specPath.indexOf("user_uploads")>=0 &&
					specPath.indexOf(File.separator + user.getXdatUserId() + File.separator)>=0 && new File(specPath).isDirectory()) {
				cachePath=specPath;
			} else {
				specPath=null;
				invalidSpecpath = true;
			}
		} 
		if (specPath==null) {
			final Date d = Calendar.getInstance().getTime();
			final java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat ("yyyyMMdd_HHmmss");
			final String uploadID = formatter.format(d);
			// Save input files to cache space
			cachePath+="user_uploads/"+user.getXdatUserId() + "/" + uploadID + "/";
		}
		// If uploading zip file with no process or build directory specified, will proceed with processing regardless unless told not 
		// to via process parameter.  Otherwise will only process when told to (when done uploading).
		String processParm = null;
		if (params.get("process")!=null) {
			processParm = params.get("process").toString();
		}
		
		final File cacheLoc = new File(cachePath);
		cacheLoc.mkdirs();
		
		// If uploading a file, process it.  Otherwise just set doProcess parameter (default=true)
		if (fw!=null) {
			doProcess=processFile(cacheLoc, specPath, processParm, doProcess, invalidSpecpath);
		} else {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		}
		
		// Conditionally process cache location files, otherwise return cache location
		if (doProcess) {
			processCacheFiles(cacheLoc);
			if (params.get("sendemail")!=null && params.get("sendemail").toString().equalsIgnoreCase("true")) {
				sendUserEmail(true);
			} else {
				sendAdminEmail();
			}
		} else {
			returnList.add(cachePath);
		}
		
	}
	
	private String returnListToHtmlString() {
		StringBuilder sb = new StringBuilder("<br/>");
		for (String s : returnList) {
			sb.append(s).append("<br/>\t");
		}
		return sb.toString();
	}
	
	private void sendUserEmail(boolean ccAdmin) {
			AdminUtils.sendUserHTMLEmail("LinkedDataUploader results (SESSION=" + exp.getLabel() + ")", returnListToHtmlString(), ccAdmin, new String[] { user.getEmail() });
	}
	
	private void sendAdminEmail() {
			AdminUtils.sendAdminEmail(user,"LinkedDataUploader results (SESSION=" + exp.getLabel() + ")", returnListToHtmlString());
	}

	private boolean processFile(final File cacheLoc,final  String specPath,final  String processParm,boolean doProcess,final  boolean invalidSpecpath) throws ClientException {
		final String fileName = fw.getName();
		if (specPath==null && isZipFileName(fileName)) {
			if (!(processParm!=null && (processParm.equalsIgnoreCase("NO") || processParm.equalsIgnoreCase("FALSE")))) {
				doProcess=true;
			}
		// If not uploading a zip file, only process when told to (via processParm) 
		} else if (!isZipFileName(fileName)) {
			if (processParm!=null && (processParm.equalsIgnoreCase("YES") || processParm.equalsIgnoreCase("TRUE"))) {
				doProcess=true;
			}
		}
		
		if (invalidSpecpath && !doProcess) {
			throw new ClientException("ERROR:  Specified build path is invalid");
		}
		
		if (isZipFileName(fileName)) {
			ZipI zipper = getZipper(fileName);
			try {
				zipper.extract(fw.getInputStream(),cacheLoc.getAbsolutePath());
			} catch (Exception e) {
				throw new ClientException("Archive file is corrupt or not a valid archive file type.");
			}
		} else {
			File cacheFile = new File(cacheLoc,fileName);
			try {
				final FileOutputStream fout = new FileOutputStream(cacheFile);
				OutputStreamWriter writer; 
				if (fileName.toLowerCase().endsWith("." + EPRIME_EDAT_EXT) || hasMatlabEnding(fileName) || hasEyetrackerBinaryEnding(fileName) || isAudioFile(fileName)) {  
					// Binary Copy
					final InputStream fin = fw.getInputStream();
					int noOfBytes = 0;
					byte[] b = new byte[1024];
					while ((noOfBytes = fin.read(b))!=-1) {
						fout.write(b,0,noOfBytes);
					}
					fin.close();
					fout.close();
				} else {
					// Preserve encoding
					writer = new OutputStreamWriter(fout,DEFAULT_ENCODING);
					IOUtils.copy(fw.getInputStream(), writer, DEFAULT_ENCODING);
					writer.close();
				}
			} catch (IOException e) {
				throw new ClientException("Could not write uploaded file.");
			}
		}
		return doProcess;
	}

	private boolean isAudioFile(String fileName) {
		for (String ext : Arrays.asList(AUDIO_EXT)) {
			if (fileName.toLowerCase().endsWith(ext.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	private boolean hasMatlabEnding(String ins) {
		for (String ret : Arrays.asList(MATLAB7T_EXT)) {
			if (ins.toLowerCase().endsWith(ret.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	private boolean hasEyetrackerBinaryEnding(String ins) {
		for (String ret : Arrays.asList(EYETRACKERBINARY_EXT)) {
			if (ins.toLowerCase().endsWith(ret.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	private boolean isZipFileName(final String fileName) {
		for (String ext : ZIP_EXT) {
			if (fileName.toLowerCase().endsWith(ext)) {
				return true;
			}
		}
		return false;
	}
	
	private void setScanModified(XnatImagescandata scan,ScanModType type) {
		if (!modifiedScans.containsKey(scan)) {
			modifiedScans.put(scan, new ArrayList<ScanModType>(Arrays.asList(type)));
		} else {
			List<ScanModType> typeList = modifiedScans.get(scan);
			if (!typeList.contains(type)) {
				typeList.add(type);
			}
		} 
	}

	private void processCacheFiles(final File cacheLoc) throws ClientException, ServerException {
		
		returnList.add("<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT = " + exp.getLabel() + ")</b>");
		
		modifiedScans = new HashMap<XnatImagescandataI,List<ScanModType>>();
		
		/////////////////////////////////
		// FIRST, PULL DICOM SCAN DATA //
		/////////////////////////////////
		
		// NOTE:  2012/10/18 - Have seen instances where acquisition date seems unreliable.  Making separate list giving priority for
		// study date in determining acquisition time.  Will try match according to that ordering when original match fails.
		
		// Pull scan acquisition times for tfMRI scans from DICOM
		final HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>> allScans = new HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>>();
		final HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>> sdAllScans = new HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>>();
		boolean sdDateDiff = false;
		List<XnatImagescandata> scans = getAllScans(exp);
		for (XnatImagescandata scan : scans) {
			final List<XnatAbstractresourceI> cats = scan.getFile();
			for (XnatAbstractresourceI cat : cats) {
				if (cat instanceof XnatResourcecatalog && cat.getLabel().equalsIgnoreCase(DICOM_RESOURCE_LABEL)) {
					final XnatResourcecatalog xcat = (XnatResourcecatalog)cat;
					final ArrayList<ResourceFile> fileResList = xcat.getFileResources(proj.getRootArchivePath());
					final ArrayList<File> fileList = new ArrayList<File>();
					for (ResourceFile rf : fileResList) {
						final File dcmFile = new File(rf.getAbsolutePath());
						if (dcmFile.exists()) {
							fileList.add(dcmFile);
						}
					}
					final Map<AcquisitionTimes, Date> acqTime = getAcquisitionTimeFromDicomFiles(fileList);
					// These are likely secondary captures or something.  All relevant scans should have acqTime.  
					if (acqTime == null || acqTime.get(AcquisitionTimes.ACQ_START) == null) {
						continue;
					}
					allScans.put(scan,acqTime);
					final Map<AcquisitionTimes, Date> sdAcqTime = getAcquisitionTimeFromDicomFilesStudyDatePriority(fileList);
					if (!acqTime.equals(sdAcqTime)) {
						sdDateDiff = true;
					}
					sdAllScans.put(scan,sdAcqTime);
				}
			}
		}
		
		// Order scan list by time
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oAllScans = getDateOrderedScanList(allScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oBoldScans = getBoldScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> sdoBoldScans = getBoldScans(getDateOrderedScanList(sdAllScans));
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oMotionScans = getMotionScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTracker3tScans = getEyeTracker3tScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTrackerDmcScans = getEyeTrackerDmcScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oMatlab7tScans = getMatlab7tScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTracker7tScans = getEyeTracker7tScans(oAllScans);
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oAudioScans = getAudioScans(oAllScans);
		
		// Pull scan acquisition times and session UUID from DICOM
		final Map<String,XnatImagescandata> scanUUIDs = new LinkedHashMap<String,XnatImagescandata>();
		scans = exp.getScansByXSIType("xnat:mrScanData");
		Collections.sort(scans, new Comparator<XnatImagescandata>() {
				public int compare(final XnatImagescandata o1,final XnatImagescandata o2) {
				try {
					Date d1 = getAcquisitionTimeFromScan(o1).get(AcquisitionTimes.ACQ_START);
					Date d2 = getAcquisitionTimeFromScan(o2).get(AcquisitionTimes.ACQ_START);
					if (d1.before(d2)) {
						return -1;
					} else if (d1.equals(d2)) {
						return 0;
					} else {
						return 1;
					}
				} catch (Exception e) {
					return 0;
				}
			}
		});
		for (XnatImagescandata scan : scans) {
			final List<XnatAbstractresourceI> cats = scan.getFile();
			for (XnatAbstractresourceI cat : cats) {
				if (cat instanceof XnatResourcecatalog && cat.getLabel().equalsIgnoreCase(DICOM_RESOURCE_LABEL)) {
					final XnatResourcecatalog xcat = (XnatResourcecatalog)cat;
					final ArrayList<ResourceFile> fileResList = xcat.getFileResources(proj.getRootArchivePath());
					final ArrayList<File> fileList = new ArrayList<File>();
					for (ResourceFile rf : fileResList) {
						final File dcmFile = new File(rf.getAbsolutePath());
						if (dcmFile.exists()) {
							fileList.add(dcmFile);
						}
					}
					final String UUID = getScanUuidFromDicomFiles(fileList);
					if (UUID!=null && UUID.length()>0) {
						scanUUIDs.put(UUID,scan);
					}
				}
			}
		}
		
		///////////////////////////////////
		// NOW PROCESS SCAN-LINKED FILES //
		///////////////////////////////////
		
		try {
			processMotionFiles(cacheLoc,oMotionScans);
		} catch (ClientException e) {
			returnList.add("Motion file processing exception - " + e);
		}
		
		try {
			processEyeTracker3tFiles(cacheLoc,oEyeTracker3tScans);
		} catch (ClientException e) {
			returnList.add("Eye tracker (3T) file processing exception - " + e);
		}
		
		try {
			processEyeTrackerDmcFiles(cacheLoc,oEyeTrackerDmcScans);
		} catch (ClientException e) {
			returnList.add("Eye tracker (DMC) file processing exception - " + e);
		}
		
		try {
			processEyeTracker7tFiles(cacheLoc,oEyeTracker7tScans);
		} catch (ClientException e) {
			returnList.add("Eye tracker (7T) file processing exception - " + e);
		}
		
		try {
			processAudioFiles(cacheLoc,oAudioScans);
		} catch (ClientException e) {
			returnList.add("Audio file processing exception - " + e);
		}
		
		try {
			processMatlab7tFiles(cacheLoc,oMatlab7tScans);
		} catch (ClientException e) {
			returnList.add("Matlab file processing exception - " + e);
		}
		
		anyEprime = false;
		anyPhysio = false;
		
		if (scanUUIDs==null || scanUUIDs.size()<1) {
			
			returnList.add("<br><b>WARNING:  No scans found containing UUIDs.  Won't attempt processing any E-prime or Physio/Head-Tracker files.</b>");
			
		} else {
			
			// E-prime sometimes clears out the return list, so create a temporary one (want to maintain motion records -- they need
			// to be processed first)
			List<String> rtList = returnList;
			List<String> eprimeRtList = new ArrayList<String>();
			returnList = eprimeRtList;
			
			try {
				
				try {
					processEprimeFiles(cacheLoc,oBoldScans);
				} catch (ClientException cex) {
					if (sdDateDiff) {
						returnList.clear();
						returnList.add("<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT = " + exp.getLabel() + ")</b>");
						processEprimeFiles(cacheLoc,sdoBoldScans);
					} else {
						throw cex;
					}
				}
				
			} catch (ClientException cex2) {
				returnList.add("E-prime processing exception - " + cex2);
			}
			
			returnList = rtList;
			returnList.addAll(eprimeRtList);
			
			// Currently physio, head-tracker files
			try {
				processOtherFiles(cacheLoc,scanUUIDs);
			} catch (ClientException e) {
				returnList.add("Physio/Head-Tracker processing exception - " + e);
				
			}
			
		}
		
		returnList.add("<br><b>REPORT ON ANY LEFTOVER FILES</b>");
		
		// Currently physio, head-tracker files
		reportLeftOvers(cacheLoc);
		
		returnList.add("<br><b>BEGIN GENERATION OF EV/PHYSIO FILES</b>");
		returnList.addAll(HCPScriptExecUtils.generateEVandPhysioTxtFiles(user, proj, exp, modifiedScans.keySet(), anyEprime, anyPhysio).getResultList());
		
		// NOTE:  These methods may not always be necessary.  XNAT code is in place that is currently calling the populateStats //
		// method as files are being added.  This issue likely is the same as I encountered in making this method in that       // 
		// the file counts needed to be cleared.  I discovered this during testing of these methods.  May want to consider      //
		// revisiting this with an XNAT-side change to fix the problem earlier on.  Future XNAT-supplied changesets may fix     //
		// the problem as well.                                                                                                 //
		refreshFileCountsInDB(modifiedScans.keySet());
		
		// Sanity checks
		returnList.add("<br><b>BEGIN RUNNING OF SANITY CHECKS FOR LINKED_DATA FILES</b>");
		returnList.addAll(HCPScriptExecUtils.runSanityChecks(user, proj, exp, true).getResultList());
		returnList.add("<br><b>FINISHED PROCESSING");
	}

	private void refreshFileCountsInDB(final Iterable<XnatImagescandataI> scans) {
		
		final String projectPath = ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc();
		// Refresh scan-level resources
		for (final XnatImagescandataI scan : scans) {
			for (final XnatAbstractresourceI rs : scan.getFile()) {
				if (!rs.getLabel().equals(LINKED_RESOURCE_LABEL)) {
					continue;
				}
				if (scan instanceof XnatImagescandata) {
					refreshCounts(((XnatImagescandata)scan).getItem(),rs,projectPath);
				}
			}
		}
		// Refresh session-level resource
		for (final XnatAbstractresourceI resource : exp.getResources_resource()) {
			if (!resource.getLabel().equals(LINKED_RESOURCE_LABEL)) {
				continue;
			}
			refreshCounts(exp.getItem(),resource,projectPath);
		}
		
	}
	
	public void refreshCounts(XFTItem it,XnatAbstractresourceI resource,String projectPath) {
			final PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, it,
				EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.PROCESS, "Catalog(s) Refreshed"));
			} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
				returnList.add("WARNING:  Could not update file counts for item");
				return;
			}
			final EventMetaI ci = wrk.buildEvent();
			try {
				// Clear current file counts and sizes so they will be recomputed instead of pulled from existing values
				if (resource instanceof XnatResourcecatalog) {
					((BaseXnatResourcecatalog)resource).clearCountAndSize();
					((XnatResourcecatalog)resource).clearFiles();
				}
				ResourceUtils.refreshResourceCatalog((XnatAbstractresource)resource, projectPath, true, false, false, false, user, ci);
			} catch (Exception e) {
				returnList.add("WARNING:  Could not update file counts for item");
			}
	}

	/////////////////////////
	// MOTION FILE METHODS //
	/////////////////////////

	private void processMotionFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oMotionScans) throws ClientException, ServerException {
		
		// Pull session times for motion files
		final Map<File,Map<AcquisitionTimes,Date>> datedLinkedFile = new HashMap<File,Map<AcquisitionTimes,Date>>();
		final List<File> linkedFiles = getMotionFiles(cacheLoc);
		for (File linkedF : linkedFiles) {
			Map<AcquisitionTimes,Date> acq = getAcquisitionTimeFromMotionFile(linkedF);
			if (acq != null) {
				datedLinkedFile.put(linkedF,acq);
			} else {
				returnList.add("WARNING:  Motion file " + linkedF.getName() + " is invalid.  Could not retrieve date information.");
			}	
		}
		
		// Order file list by time
		final List<Map<File,Map<AcquisitionTimes,Date>>> oDatedLinkedFiles = getDateOrderedMotionList(datedLinkedFile);
		
		if (oDatedLinkedFiles==null || oDatedLinkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING MOTION FILES</b>");
		
		if (oMotionScans==null || oMotionScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload motion files to.  Currently matching motion files to following scan types (" +
					Arrays.toString(getMotionTypes()) + ")");
		}
		
		// Check for files that appear to be from a different session.
		for (Map<File,Map<AcquisitionTimes,Date>> fMap : oDatedLinkedFiles) {
			for (File f : fMap.keySet()) {
				if (f.getName().matches(MOTION_FN_REGEX) && !f.getName().toLowerCase().startsWith(exp.getLabel().toLowerCase())) {
					returnList.add("ERROR:  File names suggest motion files may be from a different session (should start with experiment label)");
					throw new ClientException("ERROR:  File names suggest motion files may be from a different session (should start with experiment label)");
				}
			}
		}
		
		// Throw out files significantly shorter than the shortest scan
		weedOutShortFiles(oDatedLinkedFiles,oMotionScans);
		
		// If fewer files than scans, remove unusable scans for matching
		if (oMotionScans.size()>oDatedLinkedFiles.size()) {
			removeUnusableScans(oMotionScans);
		}
		if (oMotionScans.size()==oDatedLinkedFiles.size()) {
			// Use simple method for matching files to scans
			simpleMotionMatch(oDatedLinkedFiles,oMotionScans);
		} else if (oMotionScans.size()<oDatedLinkedFiles.size()) {
			throw new ClientException("ERROR:  Upload contains more motion files than BOLD scans.  Verify that upload is to the correct experiment and bold " + "" +
					"scan types are appropriately labeled (" + Arrays.toString(BOLD_TYPES) + ").  If necessary, upload data manually.");
		} else {
			// Use alternate method for matching.  Perform match if all motion files can be matched by time to a single scan where motion time 
			// and acquisition time are within two minutes of each other
			diffSizeMotionMatch(oDatedLinkedFiles,oMotionScans);
		}
	
	}

	private void simpleMotionMatch(
			List<Map<File, Map<AcquisitionTimes, Date>>> oDatedLinkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMotionScans) throws ClientException {
		// Motion files seem not to match as tightly on time as e-prime files do.  Here we'll 
		// just assign to scans by time order and not really do any further checking.  We've 
		// already deleted very short files and the file names should contain experiment label.
		for (int i = 0;i<oMotionScans.size();i++) {
			// Upload motion files as scan resource
			Map<File, Map<AcquisitionTimes, Date>> lMap = oDatedLinkedFiles.get(i);
			Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oMotionScans.get(i);
			if (mMap.keySet().size()>=1 && lMap.keySet().size()>=1) {
				uploadOtherFileToScan(mMap.keySet().toArray(new XnatImagescandata[0])[0],lMap.keySet().toArray(new File[0])[0],ScanModType.MOTION);
			}
		}
	}

	private void diffSizeMotionMatch(
			List<Map<File, Map<AcquisitionTimes, Date>>> oDatedLinkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMotionScans) throws ClientException {
		
		final List<Map<XnatImagescandata,File>> matches = new ArrayList<Map<XnatImagescandata,File>>();
		boolean toomany=false;
		final Map<XnatImagescandata,File> matchMap = new HashMap<XnatImagescandata,File>();
		// Loop over all motion files, add match if there is only one match where motion time and DICOM acquisition differ by less than X minutes
		for (int m=1;m<=6;m++) {
			// skip if already matched or scan has already matched to multiple motion files
			if (toomany || matches.size()==oDatedLinkedFiles.size()) {
				continue;
			}
			for (int i=0;i<oMotionScans.size();i++) {
				final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> sMap = oMotionScans.get(i);
				final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> sEntry = sMap.entrySet().iterator().next();
				final XnatImagescandata thisScan = sEntry.getKey();
				final Date scanTime = sEntry.getValue().get(AcquisitionTimes.ACQ_START);
				
				boolean already=false;
				for (Map<File,Map<AcquisitionTimes,Date>> eMap : oDatedLinkedFiles) {
					Map.Entry<File,Map<AcquisitionTimes,Date>> eEntry =  eMap.entrySet().iterator().next();
					File motionFile = eEntry.getKey();
					Date motionTime = eEntry.getValue().get(AcquisitionTimes.ACQ_START);
					if (scanTime!=null && motionTime!=null) {
						if (!toomany && !already && Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime())<(1000*60*m)) {
							matchMap.put(thisScan,motionFile);
							already=true;
						} else if (already && Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime())<(1000*60*m)) {
							toomany=true;
							break;
						}
					}
				}
			}
		}
		// If there's a single match for each motion file, proceed with upload
		if (matchMap.entrySet().size()==oDatedLinkedFiles.size()) {
			for (Map<XnatImagescandata, File> oMap : getScanOrderedList(matchMap)) {
				for (Map.Entry<XnatImagescandata,File> mEntry : oMap.entrySet()) {
					final XnatImagescandata imageScan = mEntry.getKey();
					final File fl = mEntry.getValue();
					if (imageScan!=null && fl!=null) {
						// Upload motion files as scan resource
						if (getTime(oMotionScans,imageScan)==null || getTimeM(oDatedLinkedFiles,fl)==null) {
							throw new ClientException("ERROR:  could not find motion files");
						}
						uploadOtherFileToScan(imageScan,fl,ScanModType.MOTION);
					}
				}
			}
			return;
		}
		// Otherwise, throw ClientError exception
		throw new ClientException("ERROR:  Mismatch between number of (usable) scans and Motion files,  and the motion files could not be matched reliably to existing " +
									"scans.  This could be due to a lack of synchronization between scanner time and motion file computer time, or variations in start " +
									"time of the recording of motion files");
									
	}


	private void weedOutShortFiles(
			List<Map<File, Map<AcquisitionTimes, Date>>> oDatedLinkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMotionScans) {
		// Files are ordered in date, rather than span time, so pass through three times, progressively 
		// removing longer files, until reach correct number.
		long sSpan = getShortestScanSpan(oMotionScans);
		for (int i=1;i<=3;i++) {
			// We're deleting very short files no matter what, but will keep processing for longer ones
			if (i==3 && oDatedLinkedFiles.size()<=oMotionScans.size()) {
				return;
			}
			for (Iterator<Map<File, Map<AcquisitionTimes,Date>>> iter = oDatedLinkedFiles.iterator(); iter.hasNext(); ) {
				Map<File, Map<AcquisitionTimes,Date>> lMap = iter.next();
				for (Entry<File, Map<AcquisitionTimes, Date>> lEntry : lMap.entrySet()) {
					if (lEntry.getValue().get(AcquisitionTimes.ACQ_END)!=null && lEntry.getValue().get(AcquisitionTimes.ACQ_START)!=null) {
						long fSpan = (lEntry.getValue().get(AcquisitionTimes.ACQ_END).getTime()-
								lEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime());			
						if (fSpan<(sSpan*(.10*i))) {
							iter.remove();
							if (i==3 && oDatedLinkedFiles.size()<=oMotionScans.size()) {
								return;
							}
							break;
						}
					}
				}
			}
		}
	}

	private long getShortestScanSpan(
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMotionScans) {
		long rSpan=Long.MIN_VALUE;
		for (Map<XnatImagescandata, Map<AcquisitionTimes,Date>> sMap : oMotionScans) {
			for (Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> sEntry : sMap.entrySet()) {
				long sSpan = (sEntry.getValue().get(AcquisitionTimes.ACQ_END).getTime()-
								sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime());			
				if (sSpan<rSpan || rSpan==Long.MIN_VALUE) {
					rSpan = sSpan;
				}
				
			}
		}
		return rSpan;
	}

	public List<File> getMotionFiles(final File dir) throws ClientException {
		final List<File> files=new ArrayList<File>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getMotionFiles(dir));
				
			} else {
				if (f.getName().matches(MOTION_FN_REGEX) && !f.getName().startsWith(exp.getLabel() + "_")) {
						throw new ClientException("ERROR:  One or more Motion files follows naming convention, but according to the name appears to be for a different subject.  Please check" + 
													"files and correct file names or upload manually.");
				}
				if (f.getName().toLowerCase().endsWith("." + MOTION_EXT) && f.getName().matches(MOTION_FN_REGEX)) {
					files.add(f);
				}
			}
		}
		return files;
	}


	private Map<AcquisitionTimes,Date> getAcquisitionTimeFromMotionFile(final File motionF) {
		
		Date acqStartDT = null;
		Date acqEndDT = null;
		
		LineIterator it;
		try {
			it = FileUtils.lineIterator(motionF, "UTF-8");
			try {
				String line = null;
				String prev = null;
				while (it.hasNext()) {
					if (line!=null && line.length()>5) {
						prev = line;
					}
					line = it.nextLine();
					if (acqStartDT == null) {
						String[] cols = line.split("\\s+");
						if (cols.length>1) {  
							acqStartDT = new Date(Long.valueOf(cols[1])*1000);
						}
					}
					if (!it.hasNext()) {
						String[] cols = line.split("\\s+");
						// Allow for extra line at end (depending on file conversion)
						if (cols.length<2 && prev!=null) {
							cols = prev.split("\\s+");
						}
						if (cols.length>1) {
							acqEndDT = new Date(Long.valueOf(cols[1])*1000);
						}
					}
				}
			} finally {
				LineIterator.closeQuietly(it);
			}
			Map<AcquisitionTimes,Date> rMap = new HashMap<AcquisitionTimes,Date>();
			rMap.put(AcquisitionTimes.ACQ_START, acqStartDT);
			rMap.put(AcquisitionTimes.ACQ_END, acqEndDT);
			return rMap;
		} catch (IOException e) {
			return null;
		}
		
	}

	////////////////////////////////
	// EYETRACKER 3T FILE METHODS //
	////////////////////////////////

	private void processEyeTracker3tFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTracker3tScans) throws ClientException, ServerException {
		
		final List<List<File>> linkedFiles = getEyeTracker3tFiles(cacheLoc);
		
		if (linkedFiles==null || linkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING EYE TRACKER FILES</b>");
		
		Collections.sort(linkedFiles,new Comparator<List<File>>() {
			@Override
			public int compare(List<File> o1, List<File> o2) {
				return o1.get(0).getName().compareTo(o2.get(0).getName());
			}
		});
		
		if (oEyeTracker3tScans==null || oEyeTracker3tScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload eye tracker files to.  Currently matching eye tracker files to following scan types (" +
					Arrays.toString(getEyeTracker3tTypes()) + ")");
		}
		
		// If fewer files than scans, remove unusable scans for matching
		if (oEyeTracker3tScans.size()>linkedFiles.size()) {
			removeUnusableScans(oEyeTracker3tScans);
		}
		// NOTE:  2015/02/05 This step is (I hope) no longer necessary.  Eye tracker files are being uploaded with 
		//        a trailing number that can be used to determine which scan the file goes with when there are multiple files.
		//if (oEyeTracker3tScans.size()>linkedFiles.size()) {
		//	// Don't have a great way with Eye Tracker of ensuring which scan the file goes with.  If files
		//	// are uploaded scan-by-scan, we'll skip scans that already have eye tracker files uploaded but have
		//	// a different file name than the current files
		//	removeScansWithDifferentlyNamedEyeTracker3tFiles(oEyeTracker3tScans,linkedFiles);
		//}
		eyeTracker3tMatch(linkedFiles,oEyeTracker3tScans);
	}

	@SuppressWarnings("unchecked")
	private void removeScansWithDifferentlyNamedEyeTracker3tFiles(
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oEyeTracker3tScans,
			List<List<File>> linkedFiles) {
		// Using iterator here because removing within for/next can be unpredictable
		final Iterator<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> sIter = oEyeTracker3tScans.iterator();
		while (sIter.hasNext()) {
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> eMap = sIter.next();
			final XnatImagescandata scan = eMap.keySet().iterator().next();
			breakloop:
			for (final XnatAbstractresourceI resource : scan.getFile()) {
				if (resource instanceof XnatResourcecatalog) {
					if (resource.getLabel().equals(LINKED_RESOURCE_LABEL)) {
						final XnatResourcecatalog thisC = (XnatResourcecatalog) resource;
						final ArrayList<File> files = thisC.getCorrespondingFiles("/");
						for (final File file : files) {
							if (file.getAbsolutePath().contains(ScanModType.EYETRACKER.toString())) {
								if (file.getName().matches(EYETRACKER3T_FN_REGEX)) {
									for (final List<File> fl : linkedFiles) {
										if (!file.getName().startsWith(fl.get(0).getName().replaceFirst("[.].*$",""))) {
											sIter.remove();
											break breakloop;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
			
	private void eyeTracker3tMatch(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oEyeTracker3tScans) throws ClientException {
		// Per Greg, There may be times when multiple scans are grouped in the same eye tracker file.  Per Greg,
		// these will be bundled under the earlier scan.  I'm just matching ordered files as we have them
		// to ordered scans, and stopping when out of files.
	    final List<XnatImagescandataI> eyetrackerScans = Lists.newArrayList();
	    int numEtScans = oEyeTracker3tScans.size();
	    //for (final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> m : oEyeTracker3tScans) {
	    //	for (final XnatImagescandata s : m.keySet()) {
	    //		numEtScans++;
	    //	}
	    //}
	    if (linkedFiles.size()>=numEtScans) {
	    	for (int i = 0;i<oEyeTracker3tScans.size();i++) {
	    		final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oEyeTracker3tScans.get(i);
				if (mMap.keySet().size()>=1) {
					if (linkedFiles.size()>i) {
						final List<File> files = linkedFiles.get(i);
						for (final File f : files) {
							final XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
							uploadOtherFileToScan(scan,f,ScanModType.EYETRACKER);
							if (!eyetrackerScans.contains(scan)) {
								eyetrackerScans.add(scan);
							}
						}
					}
				}
			}
		} else {
			// figure out best scan to assign to
			Map<Integer,List<File>> assnMap = getOptimal3tEyetrackerScanAssignment(linkedFiles,oEyeTracker3tScans);
	    	for (int i = 0;i<oEyeTracker3tScans.size();i++) {
	    		final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oEyeTracker3tScans.get(i);
				if (mMap.keySet().size()>=1) {
					if (assnMap.keySet().contains(i+1)) {
						final List<File> files = assnMap.get(i+1);
						for (final File f : files) {
							final XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
							uploadOtherFileToScan(scan,f,ScanModType.EYETRACKER);
							if (!eyetrackerScans.contains(scan)) {
								eyetrackerScans.add(scan);
							}
						}
					}
				}
			}
		}
		returnList.add("<br><b>BEGIN CONVERSION OF EYETRACKER EDF FILES TO ASC</b>");
		returnList.addAll(HCPScriptExecUtils.generateEyetrackerConvertedFiles(user, proj, exp, eyetrackerScans).getResultList());
	}

	private Map<Integer, List<File>> getOptimal3tEyetrackerScanAssignment(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oEyeTracker3tScans) {
		final Map<Integer,List<File>> returnMap = Maps.newHashMap();
		final Map<Integer,Integer> intMap = Maps.newHashMap();
		final Map<List<File>,Integer> flMap = Maps.newHashMap();
		boolean anyundef = false;
		int fPos = 0;
		for (final List<File> fl : linkedFiles) {
			fPos++;
			for (final File f : fl) {
				Integer fInt = -9999;
				if (f.getName().endsWith(".edf")) {
					final String fName = f.getName().substring(0,f.getName().indexOf(".edf"));
					try {
						fInt = Integer.parseInt(fName.replaceAll("^.*[^0-9]",""));
					} catch (NumberFormatException nfe) {
						anyundef = true;
					}
					if (fInt>0 && !intMap.containsKey(fInt)) {
						intMap.put(fInt,fPos);
					}
				}
				flMap.put(fl,fInt);
			}
		}
		for (final Integer i : intMap.keySet()) {
			if (Collections.min(intMap.keySet())>=3 && Collections.max(intMap.keySet())<=4 && oEyeTracker3tScans.size()<=2) {
				intMap.put(i,i-2);
			} else if (Collections.min(intMap.keySet())>=5 && Collections.max(intMap.keySet())<=8) {
				intMap.put(i,i-4);
			} else {
				intMap.put(i,i);
			}
		}
		for (final List<File> fl : flMap.keySet()) {
			if (anyundef) {
				returnMap.put(linkedFiles.indexOf(fl), fl);
			} else {
				returnMap.put(intMap.get(flMap.get(fl)),fl);
			}
		}
		return returnMap;
	}

	public List<List<File>> getEyeTracker3tFiles(final File dir) throws ClientException {
		final List<List<File>> files=new ArrayList<List<File>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getEyeTracker3tFiles(dir));
				
			} else {
				if (f.getName().matches(EYETRACKER3T_FN_REGEX) && !(f.getName().startsWith(exp.getSubjectData().getLabel()) &&
							(
								f.getName().substring(6,7).equalsIgnoreCase("a") && exp.getLabel().toLowerCase().endsWith("_a") ||
								f.getName().substring(6,7).equalsIgnoreCase("a") && exp.getLabel().toLowerCase().contains("_fnca") ||
								f.getName().substring(6,7).equalsIgnoreCase("b") && exp.getLabel().toLowerCase().endsWith("_b") ||
								f.getName().substring(6,7).equalsIgnoreCase("b") && exp.getLabel().toLowerCase().contains("_fncb") ||
								f.getName().substring(6,7).equalsIgnoreCase("x") && exp.getLabel().toLowerCase().contains("_xtr")
							))
						) {
						throw new ClientException("ERROR:  One or more Eye Tracker files follows naming convention, but according to the name appears to be for a different subject or session." +
													"  Please check files and correct file names or upload manually.");
				}
				for (final String ext : EYETRACKER_EXT) {
					if (f.getName().toLowerCase().endsWith(ext) && f.getName().matches(EYETRACKER3T_FN_REGEX)) {
						addEyeTracker3tFile(files,f);
						break;
					}
				}
			}
		}
		return files;
	}

	private void addEyeTracker3tFile(List<List<File>> files, File f) {
		for (final List<File> fl : files) {
			if (fl.get(0).getName().startsWith(f.getName().replaceFirst("[.].*$",""))) {
				fl.add(f);
				return;
			}
		}
		final ArrayList<File> al = new ArrayList<File>();
		al.add(f);
		files.add(al);
	}
	
	/////////////////////////////////
	// EYETRACKER DMC FILE METHODS //
	/////////////////////////////////

	private void processEyeTrackerDmcFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTrackerDmcScans) throws ClientException, ServerException {
		
		final List<List<File>> linkedFiles = getEyeTrackerDmcFiles(cacheLoc);
		
		if (linkedFiles==null || linkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING EYE TRACKER FILES</b>");
		
		Collections.sort(linkedFiles,new Comparator<List<File>>() {
			@Override
			public int compare(List<File> o1, List<File> o2) {
				return o1.get(0).getName().compareTo(o2.get(0).getName());
			}
		});
		
		if (oEyeTrackerDmcScans==null || oEyeTrackerDmcScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload eye tracker files to.  Currently matching eye tracker files to following scan types (" +
					Arrays.toString(getEyeTrackerDmcTypes()) + ")");
		}
		
		// If fewer files than scans, remove unusable scans for matching
		if (oEyeTrackerDmcScans.size()>linkedFiles.size()) {
			removeUnusableScans(oEyeTrackerDmcScans);
		}
		eyeTrackerDmcMatch(linkedFiles,oEyeTrackerDmcScans);
	}
			
	private void eyeTrackerDmcMatch(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oEyeTrackerDmcScans) throws ClientException {
	    final List<XnatImagescandataI> eyetrackerScans = Lists.newArrayList();
    	for (int i = 0;i<oEyeTrackerDmcScans.size();i++) {
    		final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oEyeTrackerDmcScans.get(i);
			if (mMap.keySet().size()>=1) {
				if (linkedFiles.size()>i) {
					outerloop:
					for (List<File> files : linkedFiles) {
						for (final File f : files) {
							final XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
								for (int j=1;j<=9;j++) {
									if (scan.getSeriesDescription().toLowerCase().contains("rest" + j + "_")) {
										final String REGEX="^[0-9][0-9]\\d{4}[rpbRPB]" + j + "[.].*";
										if (f.getName().matches(REGEX)) {
											uploadOtherFileToScan(scan,f,ScanModType.EYETRACKER);
											if (!eyetrackerScans.contains(scan)) {
												eyetrackerScans.add(scan);
											}
										break outerloop;
									}
								}
							}
						}
					}
				}
			}
		}
		returnList.add("<br><b>BEGIN CONVERSION OF EYETRACKER EDF FILES TO ASC</b>");
		returnList.addAll(HCPScriptExecUtils.generateEyetrackerConvertedFiles(user, proj, exp, eyetrackerScans).getResultList());
	}

	public List<List<File>> getEyeTrackerDmcFiles(final File dir) throws ClientException {
		final List<List<File>> files=new ArrayList<List<File>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getEyeTrackerDmcFiles(dir));
				
			} else {
				if (f.getName().matches(EYETRACKERDMC_FN_REGEX) && !(f.getName().startsWith(exp.getSubjectData().getLabel()) &&
							(
								f.getName().substring(6,7).equalsIgnoreCase("b") && exp.getLabel().toLowerCase().contains("baseline") ||
								f.getName().substring(6,7).equalsIgnoreCase("p") && exp.getLabel().toLowerCase().contains("proactive") ||
								f.getName().substring(6,7).equalsIgnoreCase("r") && exp.getLabel().toLowerCase().contains("reactive")
							))
						) {
						throw new ClientException("ERROR:  One or more Eye Tracker files follows naming convention, but according to the name appears to be for a different subject or session." +
													"  Please check files and correct file names or upload manually.");
				}
				for (final String ext : EYETRACKER_EXT) {
					if (f.getName().toLowerCase().endsWith(ext) && f.getName().matches(EYETRACKERDMC_FN_REGEX)) {
						addEyeTrackerDmcFile(files,f);
						break;
					}
				}
			}
		}
		return files;
	}

	private void addEyeTrackerDmcFile(List<List<File>> files, File f) {
		for (final List<File> fl : files) {
			if (fl.get(0).getName().startsWith(f.getName().replaceFirst("[.].*$",""))) {
				fl.add(f);
				return;
			}
		}
		final ArrayList<File> al = new ArrayList<File>();
		al.add(f);
		files.add(al);
	}
	
	////////////////////////////
	// MATLAB 7T FILE METHODS //
	////////////////////////////

	private void processMatlab7tFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oMatlab7tScans) throws ClientException, ServerException {
		
		final List<List<File>> linkedFiles = getMatlab7tFiles(cacheLoc);
		
		if (linkedFiles==null || linkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING MATLAB FILES</b>");
		
		Collections.sort(linkedFiles,new Comparator<List<File>>() {
			@Override
			public int compare(List<File> o1, List<File> o2) {
				return o1.get(0).getName().compareTo(o2.get(0).getName());
			}
		});
		
		if (oMatlab7tScans==null || oMatlab7tScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload matlab files to.  Currently matching matlab files to following scan types (" +
					Arrays.toString(getMatlab7tTypes()) + ")");
		}
		
		matlab7tMatch(linkedFiles,oMatlab7tScans);
	}
			
	private void matlab7tMatch(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMatlab7tScans) throws ClientException {
	    final List<XnatImagescandataI> matlabScans = Lists.newArrayList();
	    failOnDuplicatesAndRemoveUnusableIfHaveUsable(oMatlab7tScans);
	    // continue processing
		for (int i = 0;i<oMatlab7tScans.size();i++) {
			final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oMatlab7tScans.get(i);
			if (mMap.keySet().size()>=1) {
				for (List<File> files : linkedFiles) {
					for (final File f : files) {
						XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
						try {
							int s_num = Integer.valueOf(scan.getSeriesDescription().replaceFirst("^.*_(RET|MOVIE|FIX|REST|MOV)[#]?", "").replaceFirst("_.*$", ""));
							int f_num = Integer.valueOf(f.getName().replaceFirst("^.*_(RET|MOVIE|FIX|REST|MOV)[#]?", "").replaceFirst("_.*$", ""));
							if (s_num == f_num && descPartMatches(scan.getSeriesDescription(),f.getName())) {
								uploadOtherFileToScan(scan,f,getMatlab7tScanMod(f.getName()));
								if (!matlabScans.contains(scan)) {
									matlabScans.add(scan);
								}
							}
						} catch (NumberFormatException nfe) {
							// Do nothing for now
						}
					}
				}
			}
		}
		returnList.add("<br><b>BEGIN CONVERSION OF MATLAB FILES</b>");
		returnList.addAll(HCPScriptExecUtils.generateMatlabConvertedFiles(user, proj, exp, matlabScans).getResultList());
	}

	private void failOnDuplicatesAndRemoveUnusableIfHaveUsable(
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oMatlab7tScans) throws ClientException {
	    // Per Keith, 7T upload should fail if there's more than one usable scan with the same series description and one of them is not "unusable"
	    final List<String> sdListNU = Lists.newArrayList();
	    final List<String> sdListWU = Lists.newArrayList();
		for (int i = 0;i<oMatlab7tScans.size();i++) {
			final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oMatlab7tScans.get(i);
			if (mMap.keySet().size()>=1) {
				final XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
				String sd = scan.getSeriesDescription();
				if (scan.getQuality().equalsIgnoreCase("unusable")) {
					if (!sdListWU.contains(sd)) {
						sdListWU.add(sd);
					}
				} else {
					if (!sdListNU.contains(sd)) {
						sdListNU.add(sd);
					} else {
						throw new ClientException("ERROR:  For 7T, two scans should not have the same series description and neither be marked unusable");
					}
					if (!sdListWU.contains(sd)) {
						sdListWU.add(sd);
					}
				}
			}
		}
	    // Remove unusable scans if there is a usable one
		Iterator<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> it = oMatlab7tScans.iterator();
		while (it.hasNext()) {
			final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = it.next();
			if (mMap.keySet().size()>=1) {
				final XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
				final String sd = scan.getSeriesDescription();
				if (scan.getQuality().equalsIgnoreCase("unusable")) {
					if (sdListNU.contains(sd)) {
						it.remove();
					}
				}
			}
		}
		
	}

	private ScanModType getMatlab7tScanMod(String name) {
		// Commenting this out for now.  Let's put all these in a MATLAB directory.
		/*
		if (name.matches(RETINOTOPY_FN_REGEX)) {
			return ScanModType.RETINOTOPY;
		} else if (name.matches(MOVIE_FN_REGEX)) {
			return ScanModType.MOVIE;
		} else if (name.matches(FIX_FN_REGEX)) {
			return ScanModType.FIX;
		}
		*/
		return ScanModType.MATLAB;
	}

	private boolean descPartMatches(String seriesDescription, String name) {
		if ((seriesDescription.indexOf("_RET")>0 && name.indexOf("_RET")>0) ||
		    (seriesDescription.indexOf("_MOVIE")>0 && name.indexOf("_MOV")>0) ||
		    (seriesDescription.indexOf("_REST")>0 && name.indexOf("_FIX")>0)) {
			return true;
		}
		return false;
	}

	public List<List<File>> getMatlab7tFiles(final File dir) throws ClientException {
		final List<List<File>> files=new ArrayList<List<File>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getMatlab7tFiles(dir));
				
			} else {
				if (f.getName().matches(MATLAB7T_FN_REGEX) && !(f.getName().startsWith(exp.getSubjectData().getLabel()))) {
						throw new ClientException("ERROR:  One or more Matlab files follows naming convention, but according to the name appears to be for a different subject." +
													"  Please check files and correct file names or upload manually.");
				}
				for (final String ext : MATLAB7T_EXT) {
					if (f.getName().toLowerCase().endsWith(ext) && f.getName().matches(MATLAB7T_FN_REGEX)) {
						addMatlab7tFile(files,f);
						break;
					}
				}
			}
		}
		return files;
	}

	private void addMatlab7tFile(List<List<File>> files, File f) {
		for (final List<File> fl : files) {
			if (fl.get(0).getName().startsWith(f.getName().replaceFirst("[.].*$",""))) {
				fl.add(f);
				return;
			}
		}
		final ArrayList<File> al = new ArrayList<File>();
		al.add(f);
		files.add(al);
	}

	////////////////////////////////
	// EYETRACKER 7T FILE METHODS //
	////////////////////////////////

	private void processEyeTracker7tFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oEyeTracker7tScans) throws ClientException, ServerException {
		
		final List<List<File>> linkedFiles = getEyeTracker7tFiles(cacheLoc);
		
		if (linkedFiles==null || linkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING EYETRACKER(7T) FILES</b>");
		
		Collections.sort(linkedFiles,new Comparator<List<File>>() {
			@Override
			public int compare(List<File> o1, List<File> o2) {
				return o1.get(0).getName().compareTo(o2.get(0).getName());
			}
		});
		
		if (oEyeTracker7tScans==null || oEyeTracker7tScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload eyetracker files to.  Currently matching eyetracker files to following scan types (" +
					Arrays.toString(getEyeTracker7tTypes()) + ")");
		}
		
		eyeTracker7tMatch(linkedFiles,oEyeTracker7tScans);
	}
			
	private void eyeTracker7tMatch(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oEyeTracker7tScans) throws ClientException {
	    final List<XnatImagescandataI> eyetrackerScans = Lists.newArrayList();
	    failOnDuplicatesAndRemoveUnusableIfHaveUsable(oEyeTracker7tScans);
		for (int i = 0;i<oEyeTracker7tScans.size();i++) {
			final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oEyeTracker7tScans.get(i);
			if (mMap.keySet().size()>=1) {
				for (List<File> files : linkedFiles) {
					for (final File f : files) {
						XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
						try {
							int s_num = Integer.valueOf(scan.getSeriesDescription().replaceFirst("^.*_(RET|MOVIE|FIX|REST|MOV)[#]?", "").replaceFirst("_.*$", ""));
							int f_num = Integer.valueOf(f.getName().replaceFirst("^.*_(RET|MOVIE|FIX|REST|MOV)eyetrack[#]?", "").replaceFirst("_.*$", ""));
							if (s_num == f_num && descPartMatches(scan.getSeriesDescription(),f.getName())) {
								uploadOtherFileToScan(scan,f,ScanModType.EYETRACKER);
								if (!eyetrackerScans.contains(scan)) {
									eyetrackerScans.add(scan);
								}
							}
						} catch (NumberFormatException nfe) {
							// Do nothing for now
						}
					}
				}
			}
		}
		returnList.add("<br><b>BEGIN CONVERSION OF EYETRACKER EDF FILES TO ASC</b>");
		returnList.addAll(HCPScriptExecUtils.generateEyetrackerConvertedFiles(user, proj, exp, eyetrackerScans).getResultList());
	}

	public List<List<File>> getEyeTracker7tFiles(final File dir) throws ClientException {
		final List<List<File>> files=new ArrayList<List<File>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getEyeTracker7tFiles(dir));
				
			} else {
				if (f.getName().matches(EYETRACKER7T_FN_REGEX) && !(f.getName().startsWith(exp.getSubjectData().getLabel()))) {
						throw new ClientException("ERROR:  One or more EyeTracker files follows naming convention, but according to the name appears to be for a different subject." +
													"  Please check files and correct file names or upload manually.");
				}
				for (final String ext : EYETRACKER_EXT) {
					if (f.getName().toLowerCase().endsWith(ext) && f.getName().matches(EYETRACKER7T_FN_REGEX)) {
						addEyeTracker7tFile(files,f);
						break;
					}
				}
			}
		}
		return files;
	}

	private void addEyeTracker7tFile(List<List<File>> files, File f) {
		for (final List<File> fl : files) {
			if (fl.get(0).getName().startsWith(f.getName().replaceFirst("[.].*$",""))) {
				fl.add(f);
				return;
			}
		}
		final ArrayList<File> al = new ArrayList<File>();
		al.add(f);
		files.add(al);
	}

	////////////////////////
	// AUDIO FILE METHODS //
	////////////////////////

	private void processAudioFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oAudioScans) throws ClientException, ServerException {
		
		final List<List<File>> linkedFiles = getAudioFiles(cacheLoc);
		
		if (linkedFiles==null || linkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING AUDIO FILES</b>");
		
		Collections.sort(linkedFiles,new Comparator<List<File>>() {
			@Override
			public int compare(List<File> o1, List<File> o2) {
				return o1.get(0).getName().compareTo(o2.get(0).getName());
			}
		});
		
		if (oAudioScans==null || oAudioScans.size()<1) {
			throw new ClientException("Couldn't find any scans to upload eyetracker files to.  Currently matching eyetracker files to following scan types (" +
					Arrays.toString(getEyeTracker7tTypes()) + ")");
		}
		
		audioMatch(linkedFiles,oAudioScans);
	}
			
	private void audioMatch(
			List<List<File>> linkedFiles,
			List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> oAudioScans) throws ClientException {
	    final List<XnatImagescandataI> audioScans = Lists.newArrayList();
	    failOnDuplicatesAndRemoveUnusableIfHaveUsable(oAudioScans);
		for (int i = 0;i<oAudioScans.size();i++) {
			final Map<XnatImagescandata, Map<AcquisitionTimes, Date>> mMap = oAudioScans.get(i);
			if (mMap.keySet().size()>=1) {
				for (List<File> files : linkedFiles) {
					for (final File f : files) {
						XnatImagescandata scan = mMap.keySet().toArray(new XnatImagescandata[0])[0];
						try {
							if (audioSeriesMatches(scan.getSeriesDescription(),f.getName())) {
								uploadOtherFileToScan(scan,f,ScanModType.AUDIO);
								if (!audioScans.contains(scan)) {
									audioScans.add(scan);
								}
							}
						} catch (NumberFormatException nfe) {
							// Do nothing for now
						}
					}
				}
			}
		}
	}

	private boolean audioSeriesMatches(String seriesDescription, String name) {
		if ((seriesDescription.indexOf("_StroopBas1")>0 && name.indexOf("_StroopBas_run1")>0) ||
		    (seriesDescription.indexOf("_StroopBas2")>0 && name.indexOf("_StroopBas_run2")>0) ||
		    (seriesDescription.indexOf("_StroopBas3")>0 && name.indexOf("_StroopBas_run3")>0) ||
		    (seriesDescription.indexOf("_StroopBas4")>0 && name.indexOf("_StroopBas_run4")>0) ||
		    
		    (seriesDescription.indexOf("_StroopPro1")>0 && name.indexOf("_StroopPro_run1")>0) ||
		    (seriesDescription.indexOf("_StroopPro2")>0 && name.indexOf("_StroopPro_run2")>0) ||
		    (seriesDescription.indexOf("_StroopPro3")>0 && name.indexOf("_StroopPro_run3")>0) ||
		    (seriesDescription.indexOf("_StroopPro4")>0 && name.indexOf("_StroopPro_run4")>0) ||
		    
		    (seriesDescription.indexOf("_StroopRea1")>0 && name.indexOf("_StroopRea_run1")>0) ||
		    (seriesDescription.indexOf("_StroopRea2")>0 && name.indexOf("_StroopRea_run2")>0) ||
		    (seriesDescription.indexOf("_StroopRea3")>0 && name.indexOf("_StroopRea_run3")>0) ||
		    (seriesDescription.indexOf("_StroopRea4")>0 && name.indexOf("_StroopRea_run4")>0) || 
		    
		    (seriesDescription.indexOf("_CuedtsBas1")>0 && name.indexOf("_CuedtsBas_run1")>0) ||
		    (seriesDescription.indexOf("_CuedtsBas2")>0 && name.indexOf("_CuedtsBas_run2")>0) ||
		    (seriesDescription.indexOf("_CuedtsBas3")>0 && name.indexOf("_CuedtsBas_run3")>0) ||
		    (seriesDescription.indexOf("_CuedtsBas4")>0 && name.indexOf("_CuedtsBas_run4")>0) ||
		    
		    (seriesDescription.indexOf("_CuedtsPro1")>0 && name.indexOf("_CuedtsPro_run1")>0) ||
		    (seriesDescription.indexOf("_CuedtsPro2")>0 && name.indexOf("_CuedtsPro_run2")>0) ||
		    (seriesDescription.indexOf("_CuedtsPro3")>0 && name.indexOf("_CuedtsPro_run3")>0) ||
		    (seriesDescription.indexOf("_CuedtsPro4")>0 && name.indexOf("_CuedtsPro_run4")>0) ||
		    
		    (seriesDescription.indexOf("_CuedtsRea1")>0 && name.indexOf("_CuedtsRea_run1")>0) ||
		    (seriesDescription.indexOf("_CuedtsRea2")>0 && name.indexOf("_CuedtsRea_run2")>0) ||
		    (seriesDescription.indexOf("_CuedtsRea3")>0 && name.indexOf("_CuedtsRea_run3")>0) ||
		    (seriesDescription.indexOf("_CuedtsRea4")>0 && name.indexOf("_CuedtsRea_run4")>0) || 
		    
		    (seriesDescription.indexOf("_AxdptBas1")>0 && name.indexOf("_AxdptBas_run1")>0) ||
		    (seriesDescription.indexOf("_AxdptBas2")>0 && name.indexOf("_AxdptBas_run2")>0) ||
		    (seriesDescription.indexOf("_AxdptBas3")>0 && name.indexOf("_AxdptBas_run3")>0) ||
		    (seriesDescription.indexOf("_AxdptBas4")>0 && name.indexOf("_AxdptBas_run4")>0) ||
		    
		    (seriesDescription.indexOf("_AxdptPro1")>0 && name.indexOf("_AxdptPro_run1")>0) ||
		    (seriesDescription.indexOf("_AxdptPro2")>0 && name.indexOf("_AxdptPro_run2")>0) ||
		    (seriesDescription.indexOf("_AxdptPro3")>0 && name.indexOf("_AxdptPro_run3")>0) ||
		    (seriesDescription.indexOf("_AxdptPro4")>0 && name.indexOf("_AxdptPro_run4")>0) ||
		    
		    (seriesDescription.indexOf("_AxdptRea1")>0 && name.indexOf("_AxdptRea_run1")>0) ||
		    (seriesDescription.indexOf("_AxdptRea2")>0 && name.indexOf("_AxdptRea_run2")>0) ||
		    (seriesDescription.indexOf("_AxdptRea3")>0 && name.indexOf("_AxdptRea_run3")>0) ||
		    (seriesDescription.indexOf("_AxdptRea4")>0 && name.indexOf("_AxdptRea_run4")>0) || 
		    
		    (seriesDescription.indexOf("_SternBas1")>0 && name.indexOf("_SternBas_run1")>0) ||
		    (seriesDescription.indexOf("_SternBas2")>0 && name.indexOf("_SternBas_run2")>0) ||
		    (seriesDescription.indexOf("_SternBas3")>0 && name.indexOf("_SternBas_run3")>0) ||
		    (seriesDescription.indexOf("_SternBas4")>0 && name.indexOf("_SternBas_run4")>0) ||
		    
		    (seriesDescription.indexOf("_SternPro1")>0 && name.indexOf("_SternPro_run1")>0) ||
		    (seriesDescription.indexOf("_SternPro2")>0 && name.indexOf("_SternPro_run2")>0) ||
		    (seriesDescription.indexOf("_SternPro3")>0 && name.indexOf("_SternPro_run3")>0) ||
		    (seriesDescription.indexOf("_SternPro4")>0 && name.indexOf("_SternPro_run4")>0) ||
		    
		    (seriesDescription.indexOf("_SternRea1")>0 && name.indexOf("_SternRea_run1")>0) ||
		    (seriesDescription.indexOf("_SternRea2")>0 && name.indexOf("_SternRea_run2")>0) ||
		    (seriesDescription.indexOf("_SternRea3")>0 && name.indexOf("_SternRea_run3")>0) ||
		    (seriesDescription.indexOf("_SternRea4")>0 && name.indexOf("_StroopRea_run4")>0)
		    ) {
			return true;
		}
		return false;
	}

	public List<List<File>> getAudioFiles(final File dir) throws ClientException {
		final List<List<File>> files=new ArrayList<List<File>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getAudioFiles(dir));
				
			} else {
				if (f.getName().matches(AUDIO_FN_REGEX) && !(f.getName().startsWith(exp.getSubjectData().getLabel()))) {
						throw new ClientException("ERROR:  One or more EyeTracker files follows naming convention, but according to the name appears to be for a different subject." +
													"  Please check files and correct file names or upload manually.");
				}
				for (final String ext : AUDIO_EXT) {
					if (f.getName().toLowerCase().endsWith(ext) && f.getName().matches(AUDIO_FN_REGEX)) {
						addAudioFile(files,f);
						break;
					}
				}
			}
		}
		return files;
	}

	private void addAudioFile(List<List<File>> files, File f) {
		for (final List<File> fl : files) {
			if (fl.get(0).getName().startsWith(f.getName().replaceFirst("[.].*$",""))) {
				fl.add(f);
				return;
			}
		}
		final ArrayList<File> al = new ArrayList<File>();
		al.add(f);
		files.add(al);
	}

	/////////////////////
	// E-PRIME METHODS //
	/////////////////////

	@SuppressWarnings({ "unchecked" })
	private void processEprimeFiles(final File cacheLoc,final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oBoldScans) throws ClientException, ServerException {
		
		// Pull session times for eprime files
		final Map<File,Date> datedLinkedFile = new HashMap<File,Date>();
		final List<File> linkedFiles = getEprimeNonRecTextFiles(cacheLoc,true);
		for (File linkedF : linkedFiles) {
			datedLinkedFile.put(linkedF,getSessionTimeFromEprime(linkedF));
		}
		// Order eprime list by time
		final List<Map<File,Date>> oDatedLinkedFiles = getDateOrderedEprimeList(datedLinkedFile);
		
		if (oDatedLinkedFiles==null || oDatedLinkedFiles.size()<1) {
			return;
		}
		
		returnList.add("<br><b>BEGIN PROCESSING E-PRIME FILES</b>");
			
		
		if (oBoldScans==null || oBoldScans.size()<1) {
			throw new ClientException("Couldn't find any BOLD scans to upload e-prime files to.  Currently identifying the following scan types as BOLD scans (" +
					Arrays.toString(BOLD_TYPES) + ")");
		}
		
		// Pull session times for eprime _REC files
		final List<File> recFiles = getEprimeRecTextFiles(cacheLoc,true);
		
		// Pull session times for eprime PRACTICE files
		final List<File> practiceFiles = getEprimePracticeFiles(cacheLoc,true);
		
		
		// Check for e-prime files that appear to be from a different session.
		for (Map<File,Date> fMap : oDatedLinkedFiles) {
			for (File f : fMap.keySet()) {
				if (f.getName().matches(EPRIME_FN_REGEX) && !f.getName().matches(MOTION_FN_REGEX) && !f.getName().toLowerCase().startsWith(exp.getLabel().toLowerCase())) {
					if (f.getName().toLowerCase().endsWith("." + EPRIME_TXT_EXT) || f.getName().toLowerCase().endsWith("." + EPRIME_EDAT_EXT)) {
						returnList.add("ERROR:  File names suggest e-prime files may be from a different session (should start with experiment label)");
						throw new ClientException("ERROR:  File names suggest e-prime files may be from a different session (should start with experiment label)");
					}
				}
			}
		}
		
		// If fewer files than scans, remove unusable scans for matching
		List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> usableScans =  usableScanList(oBoldScans);
		
		anyEprime = true;
		
		try {
			
			if (oBoldScans.size()==oDatedLinkedFiles.size()) {
				// Use simple method for matching e-prime files to scans
				simpleEprimeMatch(oDatedLinkedFiles,oBoldScans,recFiles);
				
			} else if (usableScans.size()==oDatedLinkedFiles.size()) {
				// Use simple method for matching e-prime files to scans
				simpleEprimeMatch(oDatedLinkedFiles,usableScans,recFiles);
				
			} else if (oBoldScans.size()<oDatedLinkedFiles.size()) {
				throw new ClientException("ERROR:  Upload contains more e-prime files than BOLD scans.  Verify that upload is to the correct experiment and bold " + "" +
						"scan types are appropriately labeled (" + Arrays.toString(BOLD_TYPES) + ").  If necessary, upload data manually.");
			} else {
				throw new ClientException("ERROR:  Non-matching bold scan/e-prime file list.  Simple matching method cannot be used.  Try alternate matching method.");
			} 
		
		} catch (ClientException e) {
			
			try {
				if (oBoldScans.size()>oDatedLinkedFiles.size()) {
					returnList.clear();
					returnList.add("<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT = " + exp.getLabel() + ")</b>");
					returnList.add("<br><b>BEGIN PROCESSING E-PRIME FILES</b>");
					diffSizeEprimeMatch(oDatedLinkedFiles,oBoldScans,recFiles);
				} else {
					throw e;
				}
			} catch (ClientException cex) {
				if (usableScans.size()!=oBoldScans.size() && usableScans.size()>=oDatedLinkedFiles.size()) {
					returnList.clear();
					returnList.add("<b>BEGIN PROCESSING UPLOADED FILES (EXPERIMENT = " + exp.getLabel() + ")</b>");
					returnList.add("<br><b>BEGIN PROCESSING E-PRIME FILES</b>");
					diffSizeEprimeMatch(oDatedLinkedFiles,usableScans,recFiles);
				} else {
					throw cex;
				}
			}
			
		}
		
		// Upload practice files to session resource (Per Greg B).
		uploadEprimePracticeFilesToSession(exp, practiceFiles);
		
	}

	// Match e-prime files to scan when number of files matches number of BOLD scans or number of usable BOLD scans
	private void simpleEprimeMatch(List<Map<File, Date>> oDatedEprime, List<Map<XnatImagescandata, Map<AcquisitionTimes,Date>>> oBoldScans, List<File> recFiles) throws ClientException {
		boolean alreadyWarn1=false, alreadyWarn2=false, alreadyWarn3=false, alreadyWarn4=false, alreadyWarn5=false, alreadyWarn6=false;
		String scan1scanNumber=null;
		Date scan1scanTime=null;
		Date scan1eprimeTime=null;
		// As an initial step, check for time inconsistencies between e-prime files and scans and warn or halt processing depending on severity.
		// NOTE:  We must assume that scanner time and e-prime computer time may not be well synchronized, but the difference between
		// scanner time and e-prime computer time between scans should be very consistent.  Issue warnings for time time differences and
		// minor inconsistencies, but halt processing for major inconsistencies between scans (>3 minutes) or minor inconsistancies (>1 minute) 
		// coupled with a shift between e-prime and scanner time for all scans.
		
		// First check that names match what's expected
		
		boolean exactNameMatch = true;
		boolean typeNameMatch = true;
		for (int i=0;i<oBoldScans.size();i++) {
			
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> sMap = oBoldScans.get(i);
			final Map<File,Date> eMap = oDatedEprime.get(i);
			
			final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> sEntry = sMap.entrySet().iterator().next();
			final Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
			
			final String seriesDesc = sEntry.getKey().getSeriesDescription();
			final String eprimeName = eEntry.getKey().getName();
			
			if (exactNameMatch) {
				exactNameMatch = testExactName(seriesDesc,eprimeName);
			} 
			if (typeNameMatch && !exactNameMatch) {
				typeNameMatch = testFunctionalName(seriesDesc,eprimeName);
			}
			
		}
		for (int i=0;i<oBoldScans.size();i++) {
			
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> sMap = oBoldScans.get(i);
			final Map<File,Date> eMap = oDatedEprime.get(i);
			
			final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> sEntry = sMap.entrySet().iterator().next();
			final Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
			final XnatImagescandata thisScan = sEntry.getKey();
			final Date scanTime = sEntry.getValue().get(AcquisitionTimes.ACQ_START);
			final Date eprimeTime = eEntry.getValue();
			
			final String seriesDesc = sEntry.getKey().getSeriesDescription();
			final String eprimeName = eEntry.getKey().getName();
			
			if (i==0) {
				scan1scanNumber=thisScan.getId();
				scan1scanTime=scanTime;
				scan1eprimeTime=eprimeTime;
			}
			
			// Warn if acquisition dates differ by more than 10 minutes
			if (scanTime!=null && eprimeTime!=null) {
				// Make sure differences between scanner time and e-prime time are similar between scans otherwise halt processing or warn
				if (Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60*5)) {
					if (!alreadyWarn1 && typeNameMatch) {
						returnList.add("WARNING:  Time difference between scanner and e-prime computer time differs between scans by more than five minutes.  " +
									"The time difference between scans should be very consistent.  Please check the data and upload manually if necessary.  " +
									"Scan " + scan1scanNumber + " " +
									"(scanTime=" + scan1scanTime + ", eprimeTime=" + scan1eprimeTime + ", difference=" + ((scan1scanTime.getTime()-scan1eprimeTime.getTime())/1000) +
									" Sec.  Scan " + thisScan.getId() + 
									" (scanTime=" + scanTime + ", eprimeTime=" + eprimeTime + ", difference=" + ((scanTime.getTime()-eprimeTime.getTime())/1000) + " Sec.)" );
						alreadyWarn1 = true;
						
					} else if (!typeNameMatch) {
						throw new ClientException("ERROR:  Time difference between scanner and e-prime computer time differs between scans by more than five minutes.  " +
									"The time difference between scans should be very consistent.  Please check the data and upload manually if necessary.  " +
									"Scan " + scan1scanNumber + " " +
									"(scanTime=" + scan1scanTime + ", eprimeTime=" + scan1eprimeTime + ", difference=" + ((scan1scanTime.getTime()-scan1eprimeTime.getTime())/1000) +
									" Sec.  Scan " + thisScan.getId() + 
									" (scanTime=" + scanTime + ", eprimeTime=" + eprimeTime + ", difference=" + ((scanTime.getTime()-eprimeTime.getTime())/1000) + " Sec.)" );
					}
				}
				if (!alreadyWarn2 && Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60*3)) {
					returnList.add("WARNING:  Time difference between scanner and e-prime computer time differs between scans by more than three minutes.  " +
								"This should be checked. Scan " + scan1scanNumber + " " +
								"(scanTime=" + scan1scanTime + ", eprimeTime=" + scan1eprimeTime + ", difference=" + ((scan1scanTime.getTime()-scan1eprimeTime.getTime())/1000) +
								" Sec.  Scan " + thisScan.getId() + 
								" (scanTime=" + scanTime + ", eprimeTime=" + eprimeTime + ", difference=" + ((scanTime.getTime()-eprimeTime.getTime())/1000) + " Sec.)" );
					alreadyWarn2 = true;
				}
				// Issue warning if time difference between scanner and e-prime time is greater than 10 minutes
				if (!alreadyWarn3 && Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().getTime())>(1000*60*10)) {
					returnList.add("WARNING:  acquisition time differs between DICOM and EPRIME one or more scans by more than 10 minutes " +
							"(" + scanTime + " vs. " + eprimeTime + ").  " +
							"This could be due to clock differences between the scanner and e-prime computer, but should be checked.");
					alreadyWarn3 = true;
				}
				if (Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().getTime())>(1000*60*10) && 
						(Math.abs((scan1scanTime.getTime()-scan1eprimeTime.getTime())-(scanTime.getTime()-eprimeTime.getTime()))>(1000*60))) {
					if (!alreadyWarn4 && typeNameMatch) {
						returnList.add("WARNING:  Acquisition time differs between DICOM and EPRIME time in one or more scans by more than 10 minutes AND " + 
													"time difference between scanner time and acquisition time differs between scans.  " +
													"Please verify upload is to the correct experiment.  Upload manually if necessary");
						alreadyWarn4 = true;
					} else if (!typeNameMatch) {
						throw new ClientException("ERROR:  Acquisition time differs between DICOM and EPRIME time in one or more scans by more than 10 minutes AND " + 
													"time difference between scanner time and acquisition time differs between scans.  " +
													"Please verify upload is to the correct experiment.  Upload manually if necessary");
						
					}
				}
			} else {
				if (scanTime==null) {
					if (!alreadyWarn5 && typeNameMatch) {
						returnList.add("WARNING:  DICOM acquisition time could not be determined for one or more scans.  E-prime could not be matched to scans.  " + 	
													"Check the data and upload manually if necessary");
						alreadyWarn5 = true;
					} else if (!typeNameMatch) {
						throw new ClientException("ERROR:  DICOM acquisition time could not be determined for one or more scans.  E-prime could not be matched to scans.  " + 	
													"Check the data and upload manually if necessary");
						
					}
				}
				if (!alreadyWarn6 && typeNameMatch) {
					returnList.add("WARNING:  E-prime acquisition time could not be determined from one or more imported files.  E-prime could not be matched to scans.  " + 	
												"Check the data and upload manually if necessary");
					alreadyWarn6 = true;
				} else if (!typeNameMatch) {
					throw new ClientException("ERROR:  E-prime acquisition time could not be determined from one or more imported files.  E-prime could not be matched to scans.  " + 	
												"Check the data and upload manually if necessary");
				}
			}
			if (containsTask(seriesDesc) && containsTask(eprimeName) && !taskMatches(seriesDesc,eprimeName)) {
				throw new ClientException("ERROR:  Task indicator for file name doesn't match task indicator in series description for scan that file would have been " +
						"otherwise matched with (file=<b>" + eprimeName + "</b>, SD=<b>" + seriesDesc + "</b>)  Check the data and upload manually if necessary");
			}
		}
		// Upload as a separate step from above to prevent partial upload upon exception 
		for (int i=0;i<oBoldScans.size();i++) {
			
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> sMap = oBoldScans.get(i);
			final Map<File,Date> eMap = oDatedEprime.get(i);
			
			final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> sEntry = sMap.entrySet().iterator().next();
			final Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
			final XnatImagescandata thisScan = sEntry.getKey();
			final Date scanTime = sEntry.getValue().get(AcquisitionTimes.ACQ_START);
			final File eprimeFile = eEntry.getKey();
			final Date eprimeTime = eEntry.getValue();
			
			if (scanTime==null || eprimeTime==null) {
				throw new ClientException("ERROR:  could not find eprime files");
			}

			// Upload e-prime files as scan resource
			uploadEprimeFileToScan(thisScan,eprimeFile,false);
			
			// If _WM task and has _REC file, upload that.
			if (eprimeFile.getName().contains(WM_INDICATOR)) {
				uploadRecFiles(thisScan,eprimeFile,recFiles);
			}
			
		}
		
	}

	private boolean testExactName(String seriesDesc, String eprimeName) {
		eprimeName = eprimeName.replace("_run","");
		eprimeName = eprimeName.replaceFirst("[.][^.]*$","");
		String[] dparts = seriesDesc.split("_");
		String[] eparts = eprimeName.split("_");
		if (!(	(eparts[0] + "_" + eparts[1]).equalsIgnoreCase(exp.getLabel()) ||
				// Added to allow functioning under retest project
				(eparts[0] + "_" + eparts[1] + "_" + eparts[2]).equalsIgnoreCase(exp.getLabel()) 
			)) {
			return false;
		}
		for (String dpart : dparts) {
			for (String task : TASK_KEYS) {
				String[] otaskarr = task.split(":");
				for (String otask : otaskarr) {
					if (dpart.matches(otask + "[0-9]*")) {
						String dleftover = dpart.substring(otask.length());
						for (String otask2 : otaskarr) {
							for (String epart : Arrays.copyOfRange(eparts, 2, eparts.length)) {
								if (epart.matches(otask2 + "[0-9]*")) {
									String eleftover = epart.substring(otask2.length());
									if (dleftover.equalsIgnoreCase(eleftover)) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	private boolean testFunctionalName(String seriesDesc, String eprimeName) {
		eprimeName = eprimeName.replace("_run","");
		eprimeName = eprimeName.replaceFirst("[.][^.]*$","");
		String[] dparts = seriesDesc.split("_");
		String[] eparts = eprimeName.split("_");
		if (!(	(eparts[0] + "_" + eparts[1]).equalsIgnoreCase(exp.getLabel()) ||
				// Added to allow functioning under retest project
				(eparts[0] + "_" + eparts[1] + "_" + eparts[2]).equalsIgnoreCase(exp.getLabel()) 
			)) {
			return false;
		}
		for (String dpart : dparts) {
			for (String task : TASK_KEYS) {
				String[] otaskarr = task.split(":");
				for (String otask : otaskarr) {
					if (dpart.matches(otask + "[0-9]*")) {
						for (String otask2 : otaskarr) {
							for (String epart : Arrays.copyOfRange(eparts, 2, eparts.length)) {
								if (epart.matches(otask2 + "[0-9]*")) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	// Attempt match when fewer e-prime files than BOLD scans.  Requires closely calibrated scanner/eprime computer time.  Look for
	// cases where each given e-prime file matches to one and only one scan (attempt match allowing time variations of one minute,
	// two minutes, then three minutes.
	private void diffSizeEprimeMatch(final List<Map<File, Date>> oDatedEprime,final  List<Map<XnatImagescandata, Map<AcquisitionTimes,Date>>> oBoldScans,final  List<File> recFiles) throws ClientException {
		
		boolean toomany=false;
		final Map<XnatImagescandata,File> matchMap = new LinkedHashMap<XnatImagescandata,File>();
		// Loop over all e-prime files, add match if there is only one match where e-prime time and DICOM acquisition differ by less than X minutes
		for (int m=1;m<=6;m++) {
			// skip if already matched or scan has already matched to multiple e-prime files
			if (toomany || matchMap.entrySet().size()==oDatedEprime.size()) {
				continue;
			}
			for (int i=0;i<oBoldScans.size();i++) {
				final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> sMap = oBoldScans.get(i);
				final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> sEntry = sMap.entrySet().iterator().next();
				final XnatImagescandata thisScan = sEntry.getKey();
				final Date scanTime = sEntry.getValue().get(AcquisitionTimes.ACQ_START);
				
				boolean already=false;
				for (Map<File,Date> eMap : oDatedEprime) {
					Map.Entry<File,Date> eEntry =  eMap.entrySet().iterator().next();
					File eprimeFile = eEntry.getKey();
					Date eprimeTime = eEntry.getValue();
					if (scanTime!=null && eprimeTime!=null) {
						if (isValidEprimeName(eprimeFile.getName()) && taskMatches(thisScan.getSeriesDescription(),eprimeFile.getName()) && !toomany && !already && Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().getTime())<(1000*60*m)) {
							matchMap.put(thisScan,eprimeFile);
							already=true;
						} else if (isValidEprimeName(eprimeFile.getName()) && taskMatches(thisScan.getSeriesDescription(),eprimeFile.getName()) && already && Math.abs(sEntry.getValue().get(AcquisitionTimes.ACQ_START).getTime()-eEntry.getValue().getTime())<(1000*60*m)) {
							toomany=true;
							break;
						}
					}
				}
			}
		}
		// If there's a single match for each e-prime file, proceed with upload
		if (matchMap.entrySet().size()==oDatedEprime.size()) {
			for (Map<XnatImagescandata, File> oMap : getScanOrderedList(matchMap)) {
				for (Map.Entry<XnatImagescandata,File> mEntry : oMap.entrySet()) {
					final XnatImagescandata imageScan = mEntry.getKey();
					final File eprimeFile = mEntry.getValue();
					if (imageScan!=null && eprimeFile!=null) {
						// Upload e-prime files as scan resource
						if (getTime(oBoldScans,imageScan)==null || getTime(oDatedEprime,eprimeFile)==null) {
							throw new ClientException("ERROR:  could not find eprime files");
						}
						uploadEprimeFileToScan(imageScan,eprimeFile,false);
						// If _WM task and has _REC file, upload that.
						if (eprimeFile.getName().contains(WM_INDICATOR)) {
							uploadRecFiles(imageScan,eprimeFile,recFiles);
						}
					}
				}
			}
			return;
		}
		// Otherwise, throw ClientError exception
		throw new ClientException("ERROR:  Mismatch between number of (usable) scans and e-prime files,  and the e-prime files could not be matched reliably to existing " +
									"scans.  This could be due to a lack of synchronization between scanner time and e-prime computer time. However, please check " +
									"that upload is to correct experiment.  If necessary,upload data manually");
	}

	private boolean taskMatches(String seriesDescription, String eprimeName) {
		return testFunctionalName(seriesDescription, eprimeName);
	}
	
	private boolean containsTask(String name) {
		name = name.replace("_run","");
		name = name.replaceFirst("[.][^.]*$","");
		String[] dparts = name.split("_");
		for (String dpart : dparts) {
			for (String task : TASK_KEYS) {
				String[] otaskarr = task.split(":");
				for (String otask : otaskarr) {
					if (dpart.matches(otask + "[0-9]*")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private boolean isValidEprimeName(String name) {
		if (name.toLowerCase().startsWith(exp.getLabel().toLowerCase())) {
			return true;
		}
		return false;
	}

	private void uploadRecFiles(final XnatImagescandata thisScan,final File eprimeFile,final List<File> recFiles) throws ClientException {
		final String[] recParts = eprimeFile.getName().split(WM_INDICATOR);
		if (recParts.length==2) {
			final String recName = recParts[0] + REC_INDICATOR + recParts[1];
			for (File recFile : recFiles) {
				if (recFile.getName().equals(recName)) {	
					uploadEprimeFileToScan(thisScan,recFile,true);
				}
			}
		}
	}
	
	private void createResource(AutoXnatImagescandata thisScan) throws ClientException {
		
		final File scanDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + exp.getLabel() +
					File.separator + "SCANS" + File.separator + thisScan.getId() + File.separator + LINKED_RESOURCE_LABEL);
		if (!scanDir.exists()) {
			scanDir.mkdirs();
		}
		final File catFile = new File(scanDir,LINKED_CATXML_PREFIX + thisScan.getId() + LINKED_CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(LINKED_RESOURCE_LABEL);
		ecat.setFormat(LINKED_RESOURCE_FORMAT);
		ecat.setContent(LINKED_RESOURCE_CONTENT);
		// Save resource to scan
		final String eventStr = "Resource " + LINKED_RESOURCE_LABEL + " created under scan " + thisScan.getId() + " (SD=" + thisScan.getSeriesDescription() + ")" ;
		try {
			thisScan.addFile(ecat);
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(thisScan,user,false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
			returnList.add(eventStr); 
		} catch (Exception e) {
			throw new ClientException("ERROR:  Couldn't add resource to scan - " + e.getMessage(),new Exception());
		}
		
	}
	
	private void createSessionResourceIfNecessary(final XnatMrsessiondata thisSession,final DirectExptResourceImpl sessionModifier,final boolean createIfNecessary) throws ClientException {

		final XnatAbstractresourceI resource =  sessionModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
		if (createIfNecessary && resource==null) {
			createSessionResource(thisSession);
		}
		
	}
	
	private void createSessionResource(final AutoXnatMrsessiondata thisSession) throws ClientException {
		
		final File sessionDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + exp.getLabel() +
					File.separator + "RESOURCES" + File.separator + LINKED_RESOURCE_LABEL);
		if (!sessionDir.exists()) {
			sessionDir.mkdirs();
		}
		final File catFile = new File(sessionDir,LINKED_CATXML_PREFIX + thisSession.getId() + LINKED_CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		} catch (Exception e) {
			throw new ClientException("Couldn't write catalog XML file",e);
		}
		ecat.setLabel(LINKED_RESOURCE_LABEL);
		ecat.setFormat(LINKED_RESOURCE_FORMAT);
		ecat.setContent(LINKED_RESOURCE_CONTENT);
		// Save resource to session
		final String eventStr = "Resource " + LINKED_RESOURCE_LABEL + " created under session " + thisSession.getId();
		try {
			thisSession.addResources_resource(ecat);
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			if (SaveItemHelper.authorizedSave(thisSession,user,false,true,ci)) {
				PersistentWorkflowUtils.complete(wrk, ci);
			} else {
				PersistentWorkflowUtils.fail(wrk,ci);
			}
			returnList.add(eventStr); 
		} catch (Exception e) {
			throw new ClientException("ERROR:  Couldn't add resource to session - " + e.getMessage(),new Exception());
		}
		
	}
	
	private void createResourceIfNecessary(XnatImagescandata thisScan, DirectScanResourceImpl scanModifier, boolean createIfNecessary) throws ClientException {

		final XnatAbstractresourceI resource =  scanModifier.getResourceByLabel(LINKED_RESOURCE_LABEL, null);
		if (createIfNecessary && resource==null) {
			createResource(thisScan);
		}
		
	}

	private void uploadEprimeFileToScan(final XnatImagescandata thisScan,final File epFile,final boolean isRecFile) throws ClientException {
		
			
		final String type = isRecFile?"REC":"E-PRIME";
		final String dirLoc = ScanModType.EPRIME.toString();
		
		String fileCatPath = dirLoc + File.separator + epFile.getName();
		
		try {
			// See if file already exists
			boolean alreadyExists=doesExist(thisScan,epFile,ScanModType.EPRIME,fileCatPath);
			
			String eventStr;
			if (alreadyExists) {
				// Will replace it
				eventStr = "Replaced existing " +  type + " Files (<b>" + epFile.getName().replaceAll("[.].*", "") + "</b> for scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))" ;
			} else {
				// Will add it
				eventStr = "Uploaded " +  type + " Files (<b>" + epFile.getName().replaceAll("[.].*", "") + "</b> for scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))" ;
			}
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.ADDED_MISC_FILES, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			
			final DirectScanResourceImpl scanModifier = getScanModifier(thisScan,ci);
			
			createResourceIfNecessary(thisScan,scanModifier,true);
			
			// Add both text and e-prime file
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			fws.add(new StoredFile(epFile,alreadyExists));
			// Need separate addfile steps for each file because filename used in destination only when destination is null
			final File edatFile = new File(epFile.getParentFile(),epFile.getName().substring(0,epFile.getName().length()-3) + EPRIME_EDAT_EXT);
			boolean rc = true;
			try {
				List<String> duplicates = scanModifier.addFile(fws, LINKED_RESOURCE_LABEL, null, dirLoc + File.separator + fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
			} catch (Exception e) {
				rc = false;
			}
			if (rc && edatFile.exists()) {
				fws.clear();
				fws.add(new StoredFile(edatFile,alreadyExists));
				try {
					List<String> duplicates = scanModifier.addFile(fws, LINKED_RESOURCE_LABEL, null, dirLoc + File.separator + fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
				} catch (Exception e) {
					rc = false;
				}
			}
			
			setScanModified(thisScan,ScanModType.EPRIME);
			
			if (rc) {
				PersistentWorkflowUtils.complete(wrk, ci);
				returnList.add(eventStr); 
			} else {
				PersistentWorkflowUtils.complete(wrk, ci);
				returnList.add("WARNING:  Could not add " +  type + " Files (<b>" + epFile.getName().replaceAll("[.].*", "") + "</b> to scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))");
			}
			
			returnList.add(eventStr); 
			
		} catch (Exception e) {
			throw new ClientException("ERROR:  Could not add " + type + " files to resource");
		}
		
	}

	private void uploadEprimePracticeFilesToSession(final XnatMrsessiondata thisSession,final List<File> practiceFiles) throws ClientException {
		
		if (practiceFiles.size()==0) {
			return;
		}
		
		try {
			
			final String dirLoc = ScanModType.EPRIME.toString();
			
			final String eventStr = "<b>Uploaded Practice Files for session " + thisSession.getLabel() + "</b>";
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.ADDED_MISC_FILES, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			
			final DirectExptResourceImpl sessionModifier = getSessionModifier(thisSession,ci);
			
			createSessionResourceIfNecessary(thisSession,sessionModifier,true);
			
			// Iterate list and add both text and e-prime file
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			for (File pFile : practiceFiles) {
				
				String fileCatPath = dirLoc + File.separator + pFile.getName();
		
				// See if file already exists
				boolean alreadyExists=doesExist(thisSession,pFile,ScanModType.EPRIME,fileCatPath,false);
			
				fws.clear();
				fws.add(new StoredFile(pFile,alreadyExists));
				boolean rc = true;
				try {
					List<String> duplicates = sessionModifier.addFile(fws, LINKED_RESOURCE_LABEL, null, dirLoc + File.separator + fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
				} catch (Exception e) {
					rc = false;
				}
				
				if (!rc) {
					returnList.add("<b>WARNING: Unable to upload E-PRIME PRACTICE file [FILE=" + pFile.getName() + "] for session " + thisSession.getLabel() + "</b>");
					continue;
				}
				
				if (alreadyExists) {
					returnList.add("<b>Replaced existing E-PRIME PRACTICE file [FILE=" + pFile.getName() + "] in session " + thisSession.getLabel() + "</b>");
				} else {
					returnList.add("<b>Uploaded E-PRIME PRACTICE file [FILE=" + pFile.getName() + "] to session " + thisSession.getLabel() + "</b>");
				}
				
				final File edatFile = new File(pFile.getParentFile(),pFile.getName().substring(0,pFile.getName().length()-3) + EPRIME_EDAT_EXT);
				if (edatFile.exists()) {
					fws.clear();
					fws.add(new StoredFile(edatFile,alreadyExists));
					rc = true;
					try {
						List<String> duplicates = sessionModifier.addFile(fws, LINKED_RESOURCE_LABEL, null, dirLoc + File.separator + fws.get(0).getName(), new XnatResourceInfo(user,new Date(),new Date()), false);
					} catch (Exception e) {
						rc = false;
					}
					if (!rc) {
						returnList.add("<b>WARNING: Unable to upload E-PRIME PRACTICE file [FILE=" + edatFile.getName() + "] for session " + thisSession.getLabel() + "</b>");
						continue;
					}
					if (alreadyExists) {
						returnList.add("<b>Replaced existing E-PRIME PRACTICE file [FILE=" + edatFile.getName() + "] in session " + thisSession.getLabel() + "</b>");
					} else {
						returnList.add("<b>Uploaded E-PRIME PRACTICE file [FILE=" + edatFile.getName() + "] to session " + thisSession.getLabel() + "</b>");
					}
				}
			}
			
			PersistentWorkflowUtils.complete(wrk, ci);
			returnList.add(eventStr); 
			
		} catch (Exception e) {
			throw new ClientException("ERROR:  Could not add practice files to session resource");
		}
		
	}

	public List<File> getEprimeNonRecTextFiles(final File dir,final boolean recursive) throws ClientException {
		return getEprimeTextFiles(dir,recursive,true,false,true,false);
	}

	public List<File> getEprimeRecTextFiles(final File dir,final boolean recursive) throws ClientException {
		return getEprimeTextFiles(dir,recursive,false,true,true,false);
	}

	public List<File> getEprimePracticeFiles(final File dir,final boolean recursive) throws ClientException {
		return getEprimeTextFiles(dir,recursive,false,true,false,true);
	}

	public List<File> getEprimeTextFiles(final File dir,final boolean recursive,final boolean includeNonRec,final boolean includeRec,final boolean includeNonPractice,boolean includePractice) throws ClientException {
		final List<File> files=new ArrayList<File>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getEprimeTextFiles(f,true,includeNonRec,includeRec,includeNonPractice,includePractice));
				
			} else {
				if (f.getName().matches(EPRIME_FN_REGEX) && !f.getName().matches(MOTION_FN_REGEX) && !f.getName().startsWith(exp.getLabel() + "_")) {
					if (f.getName().toLowerCase().endsWith("." + EPRIME_TXT_EXT) || f.getName().toLowerCase().endsWith("." + EPRIME_EDAT_EXT)) {
						throw new ClientException("ERROR:  One or more EPRIME files follows naming convention, but according to the name appears to be for a different subject.  Please check" + 
													"files and correct file names or upload manually.");
					}
				}
				if (!includeNonRec) {
					if (!f.getName().contains(REC_INDICATOR)) {
						continue;
					}
				}
				if (!includeRec) {
					if (f.getName().contains(REC_INDICATOR)) {
						continue;
					}
				}
				if (!includeNonPractice) {
					if (!f.getName().contains(PRACTICE_INDICATOR)) {
						continue;
					}
				}
				if (!includePractice) {
					if (f.getName().contains(PRACTICE_INDICATOR)) {
						continue;
					}
				}
				if (f.getName().toLowerCase().endsWith("." + EPRIME_TXT_EXT) && !f.getName().matches(MOTION_FN_REGEX)) {
					// Must have matching .edat2 file in same directory
					File edatf = new File(f.getParentFile(),f.getName().substring(0,f.getName().length()-3) + EPRIME_EDAT_EXT);
					if (edatf.exists()) {
						files.add(f);
					} else if (isEdatTxtFile(f)) {
						files.add(f);
						returnList.add("WARNING:  E-prime text file (" + f.getName() + ") has no matching " + EPRIME_EDAT_EXT + " file.  Will be uploaded anyway.");
					}
				}
			}
		}
		return files;
	}
	
	private boolean isEdatTxtFile(File f) {
		if (getSessionTimeFromEprime(f)!=null) {
			return true;
		}
		return false;
	}

	private Date getSessionTimeFromEprime(final File eprimeF) {
		try {
			// MODIFIED 2013/08/07:  Had bug where two e-prime files that were obviously from the same session
			// had different session times.  The second run was recorded as having a session time one second earlier
			// than the later session.  As a fix, we're checking for the first available onset time and adding that
			// to the session time.  That way we'll be returning when procedures actually started.
			String dateStr = null;
			String timeStr = null;
			String onsetStr = null;
			Integer onsetMS = null;
			// NOTE:  E-prime text files seem usually to be stored in UTF-16le format.  Checking format here
			// in case someone modifies it. 
			final String fileEncoding = getFileEncoding(eprimeF);
			final LineIterator it = FileUtils.lineIterator(eprimeF, fileEncoding);
			try {
				while (it.hasNext()) {
					String lineStr = it.nextLine();
					if (lineStr!=null && lineStr.length()>12 && lineStr.substring(0,12).equalsIgnoreCase("SessionDate:")) {
						dateStr=lineStr.substring(13).trim();
					} else if (lineStr!=null && lineStr.length()>12 && lineStr.substring(0,12).equalsIgnoreCase("SessionTime:")) {
						timeStr=lineStr.substring(13).trim();
					} else if (onsetMS == null && lineStr!=null && lineStr.contains("OnsetTime:")) {
						onsetStr=lineStr.replaceFirst("^.*OnsetTime: *","");
						onsetMS = Integer.parseInt(onsetStr);
					}
					if (dateStr!=null && timeStr!=null && onsetMS!=null) {
						return new Date((new SimpleDateFormat(EPRIME_DATE_FORMAT)).parse(dateStr + " " + timeStr).getTime() + onsetMS);
					}
				}
			} finally {
				LineIterator.closeQuietly(it);
			}
			return null;
		} catch (Exception e){
			return null;
		}
	}
	
	////////////////////////////////
	// PHYSIO/HeadTracker METHODS //
	////////////////////////////////
	
	private void processOtherFiles(final File cacheLoc,final Map<String, XnatImagescandata> scans) throws ClientException, ServerException {
		
		// Pull session times for files
		final List<Map<File,ScanModType>> files = getOtherFiles(cacheLoc,true);
		if (files.size()>0) {
			returnList.add("<br><b>BEGIN PROCESSING PHYSIO/HEAD-TRACKER FILES</b>");
		}
		for (final Map.Entry<String,XnatImagescandata>  scan : scans.entrySet()) {
			ArrayList<Map<File,ScanModType>> removeList = new ArrayList<Map<File,ScanModType>>();
			for (final Map<File,ScanModType> fileM : files) {
				// Pull UUID from filename
				final File oFile = fileM.keySet().toArray(new File[0])[0];
				final ScanModType oFileType = fileM.get(oFile);
				final String oFileN = oFile.getName();
				final String fileUUID = oFileN.substring(0,oFileN.indexOf(".")).substring(oFileN.lastIndexOf("_")+1);
				if (scan.getKey().equals(fileUUID)) {
					if (!anyPhysio && oFileType.equals(ScanModType.PHYSIO)) {
						anyPhysio = true;
					}
					uploadOtherFileToScan(scan.getValue(),oFile,oFileType);
					removeList.add(fileM);
				} 
			}
			files.removeAll(removeList);
		}
		
	}
	
	public List<Map<File,ScanModType>> getOtherFiles(final File dir,final boolean recursive) {
		final List<Map<File,ScanModType>> files=new ArrayList<Map<File,ScanModType>>();
		if (!dir.isDirectory()) {
			return files;
		}
		for (final File f : dir.listFiles()) {
			if (f.isDirectory()) {
				files.addAll(getOtherFiles(f,true));
				
			} else {
				if (f.getName().toLowerCase().startsWith(PHYSIO_PREFIX.toLowerCase())) {
					for (final String s : PHYSIO_EXT) {
						if (f.getName().endsWith(s)) {
							final HashMap<File,ScanModType> mapF = new HashMap<File,ScanModType>();
							mapF.put(f, ScanModType.PHYSIO);
							files.add(mapF);
						}
					}
				}
				if (f.getName().toLowerCase().startsWith(HEADTRACKER_PREFIX.toLowerCase())) {
					for (final String s : HEADTRACKER_EXT) {
						if (f.getName().endsWith(s)) {
							final HashMap<File,ScanModType> mapF = new HashMap<File,ScanModType>();
							mapF.put(f, ScanModType.HEAD_TRACKER);
							files.add(mapF);
						}
					}
				}
			}
		}
		return files;
	}

	private void uploadOtherFileToScan(final XnatImagescandata thisScan,final File file,ScanModType type) throws ClientException {
		
		String fileCatPath = type + File.separator + file.getName();
		
		try {
			
			// See if file already exists
			boolean alreadyExists=doesExist(thisScan,file,type,fileCatPath);
			
			String eventStr;
			if (alreadyExists) {
				// Will replace it
				eventStr = "Replaced existing " +  type + " File (<b>" + file.getName() + "</b> for scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))" ;
			} else {
				// Will add it
				eventStr = "Uploaded " +  type + " File (<b>" + file.getName() + "</b> for scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))" ;
			}
			
			final PersistentWorkflowI wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, exp.getItem(),
					EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.ADDED_MISC_FILES, eventStr, null));
			final EventMetaI ci = wrk.buildEvent();
			
			final DirectScanResourceImpl scanModifier = getScanModifier(thisScan,ci);
			createResourceIfNecessary(thisScan,scanModifier,true);
			
			setScanModified(thisScan,type);
			
			// Add both text and e-prime file
			final ArrayList<StoredFile> fws = new ArrayList<StoredFile>();
			fws.add(new StoredFile(file,alreadyExists));
			boolean rc = true;
			try {
				List<String> duplicates = scanModifier.addFile(fws, LINKED_RESOURCE_LABEL, null,fileCatPath, new XnatResourceInfo(user,new Date(),new Date()), false);
			} catch(Exception e) {
				rc = false;
			}
			if (rc) {
				
				PersistentWorkflowUtils.complete(wrk, ci);
				returnList.add(eventStr); 
				
			} else {
				
				PersistentWorkflowUtils.fail(wrk, ci);
				returnList.add("WARNING:  Could not add " +  type + " File (<b>" + file.getName() + "</b> to scan " + thisScan.getId() + " (SD=<b>" + thisScan.getSeriesDescription() + "</b>))");
			
				
			}
			
		} catch (Exception e) {
			throw new ClientException("ERROR:  Could not add " + type + " files to resource");
		}
		
	}

	private boolean doesExist(XnatMrsessiondata thisSession, File pFile,
			ScanModType type, String fileCatPath,boolean includeScanResources) {
		List<XnatAbstractresourceI> thisFL = thisSession.getAllResources();
		if (!includeScanResources) {
			Iterator<XnatAbstractresourceI> flIter = thisFL.iterator();
			while (flIter.hasNext()) {
				XnatAbstractresourceI fl = flIter.next();
				if (fl instanceof XnatResourcecatalog) {
					XnatResourcecatalog fc = (XnatResourcecatalog) fl;
					ItemI fcp = fc.getParent();
					if (fcp!=null && fcp.getXSIType().equals("xnat:imageScanData")) {
						flIter.remove();
					}
				}
			}
		}
		return checkFileExist(thisFL,pFile,type,fileCatPath);
	}

	private boolean doesExist(XnatImagescandata thisScan, File file, ScanModType type, String fileCatPath) {
		List<XnatAbstractresourceI> thisFL = thisScan.getFile();
		return checkFileExist(thisFL,file,type,fileCatPath);
	}
	
	@SuppressWarnings("unchecked")
	private boolean checkFileExist(List<XnatAbstractresourceI> thisFL, File pFile,
			ScanModType type, String fileCatPath) {
		for (final XnatAbstractresourceI thisF : thisFL) {
			if (thisF instanceof XnatResourcecatalog) {
				if (thisF.getLabel().equals(LINKED_RESOURCE_LABEL)) {
					final XnatResourcecatalog thisC = (XnatResourcecatalog) thisF;
					final ArrayList<File> files = thisC.getCorrespondingFiles("/");
					for (final File f : files) {
						if (f.getPath().endsWith(fileCatPath)) {
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}

	///////////////////////////
	// LEFTOVER FILE METHODS //
	///////////////////////////
	
	private void reportLeftOvers(File cacheLoc) {
		if (!cacheLoc.isDirectory()) {
			return;
		}
		final File[] fList = cacheLoc.listFiles();
		Arrays.sort(fList, new Comparator<File>() {
				public int compare(final File o1,final File o2) {
					return (o1.getName().compareToIgnoreCase(o2.getName()));
				}});
		for (final File f : fList) {
			if (f.isDirectory()) {
				reportLeftOvers(f);
				
			} else {
				returnList.add("NOTE:  Uploaded file - <b>" + f.getName() + "</b> - was not uploaded to any scan"); 
			}
		}
	}
	
	///////////////////////////
	// SHARED/OTHER METHODS //
	//////////////////////////

	private String getFileEncoding(final File linkedF) {
		
		
		final byte[] buf = new byte[4096];
		java.io.FileInputStream fis;
		try {
			fis = new java.io.FileInputStream(linkedF);
		} catch (FileNotFoundException e) {
			return DEFAULT_ENCODING;
		}

		final UniversalDetector detector = new UniversalDetector(null);

		int nread;
		try {
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
				detector.handleData(buf, 0, nread);
			}
		} catch (IOException e) {
			return DEFAULT_ENCODING;
		}
		detector.dataEnd();

		String encoding = detector.getDetectedCharset();
		if (encoding == null) {
			encoding = DEFAULT_ENCODING;
		}
		
		detector.reset();
		return encoding;
		
	}
	
	private Object getTimeM(final List<Map<File, Map<AcquisitionTimes, Date>>> oDatedLinkedFile, File linkedFile) {
		for (final Map<File,Map<AcquisitionTimes,Date>> map : oDatedLinkedFile) {
			final Map.Entry<File,Map<AcquisitionTimes,Date>> entry =  map.entrySet().iterator().next();
			if (entry.getKey().equals(linkedFile)) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	private Date getTime(final List<Map<File, Date>> oDatedLinkedFile,final File linkedFile) {
		for (final Map<File,Date> map : oDatedLinkedFile) {
			final Map.Entry<File,Date> entry =  map.entrySet().iterator().next();
			if (entry.getKey().equals(linkedFile)) {
				return entry.getValue();
			}
		}
		return null;
	}

	private Date getTime(final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oDatedScans,final XnatImagescandata imageScan) {
		for (final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> map : oDatedScans) {
			final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>> entry =  map.entrySet().iterator().next();
			if (entry.getKey().equals(imageScan)) {
				return entry.getValue().get(AcquisitionTimes.ACQ_START);
			}
		}
		return null;
	}

	private Map<AcquisitionTimes,Date> getAcquisitionTimeFromDicomFiles(final ArrayList<File> fileList) {
		Date acqStartDT = null;
		Date acqEndDT = null;
		for (final File file : fileList) {
			try {
				if (file.getName().toLowerCase().endsWith(DICOM_EXT)) {
					final DicomObject dcmObj = DicomUtils.read(file, Tag.AcquisitionTime);
					Date thisDateTime = dcmObj.getDate(Tag.AcquisitionDateTime);
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.AcquisitionDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.SeriesDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.StudyDate,Tag.AcquisitionTime);
					}
					if (thisDateTime!=null && (acqStartDT==null || thisDateTime.before(acqStartDT))) {
						acqStartDT=thisDateTime;
					}
					if (thisDateTime!=null && (acqEndDT==null || thisDateTime.after(acqEndDT))) {
						acqEndDT=thisDateTime;
					}
				}
			} catch (IOException e) {
				// Do nothing for now
			}
		}
		Map<AcquisitionTimes,Date> rMap = new HashMap<AcquisitionTimes,Date>();
		rMap.put(AcquisitionTimes.ACQ_START, acqStartDT);
		rMap.put(AcquisitionTimes.ACQ_END, acqEndDT);
		return rMap;
	}

	private Map<AcquisitionTimes,Date> getAcquisitionTimeFromDicomFilesStudyDatePriority(final ArrayList<File> fileList) {
		Date acqStartDT = null;
		Date acqEndDT = null;
		for (final File file : fileList) {
			try {
				if (file.getName().toLowerCase().endsWith(DICOM_EXT)) {
					final DicomObject dcmObj = DicomUtils.read(file, Tag.AcquisitionTime);
					Date thisDateTime = null;
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.StudyDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.AcquisitionDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = getDateTimeByTags(dcmObj,Tag.SeriesDate,Tag.AcquisitionTime);
					}
					if (thisDateTime == null) {
						thisDateTime = dcmObj.getDate(Tag.AcquisitionDateTime);
					}
					if (thisDateTime!=null && (acqStartDT==null || thisDateTime.before(acqStartDT))) {
						acqStartDT=thisDateTime;
					}
					if (thisDateTime!=null && (acqEndDT==null || thisDateTime.before(acqEndDT))) {
						acqEndDT=thisDateTime;
					}
				}
			} catch (IOException e) {
				// Do nothing for now
			}
		}
		Map<AcquisitionTimes,Date> rMap = new HashMap<AcquisitionTimes,Date>();
		rMap.put(AcquisitionTimes.ACQ_START, acqStartDT);
		rMap.put(AcquisitionTimes.ACQ_END, acqEndDT);
		return rMap;
	}
	
	private Map<AcquisitionTimes, Date> getAcquisitionTimeFromScan(final XnatImagescandata scan) {
			final List<XnatAbstractresourceI> cats = scan.getFile();
			for (XnatAbstractresourceI cat : cats) {
				if (cat instanceof XnatResourcecatalog && cat.getLabel().equalsIgnoreCase(DICOM_RESOURCE_LABEL)) {
					final XnatResourcecatalog xcat = (XnatResourcecatalog)cat;
					final ArrayList<ResourceFile> fileResList = xcat.getFileResources(proj.getRootArchivePath());
					final ArrayList<File> fileList = new ArrayList<File>();
					for (ResourceFile rf : fileResList) {
						final File dcmFile = new File(rf.getAbsolutePath());
						if (dcmFile.exists()) {
							fileList.add(dcmFile);
						}
					}
					return getAcquisitionTimeFromDicomFilesStudyDatePriority(fileList);
				}
			}
			return null;
	}		
	
	@SuppressWarnings({ "rawtypes" })
	private String getScanUuidFromDicomFiles(final ArrayList<File> fileList) {

		final XFTTable t = new XFTTable();
		t.initTable(columns);
		
		for (final File file : fileList) {

			DicomObject header;
			try {
				header = getHeader(file);
				//final DicomObjectToStringParam formatParams = DicomObjectToStringParam.getDefaultParam();

				// NOTE: Only outputting SiemensShadowHeader information
				for (Iterator<DicomElement> it = header.iterator(); it.hasNext();) {
						final DicomElement e = it.next();
						if (fields.isEmpty() || fields.containsKey(e.tag())) {
							if (e.hasDicomObjects()) {
								/*
								for (int i = 0; i < e.countItems(); i++) {
										DicomObject o = e.getDicomObject(i);
										t.insertRow(makeRow(header, e, TagUtils.toString(e.tag()), formatParams.valueLength));
										for (Iterator<DicomElement> it1 = o.iterator(); it1.hasNext();) {
											DicomElement e1 = it1.next();
											t.insertRow(makeRow(header, e1, TagUtils.toString(e.tag()), formatParams.valueLength));
										}
								}
								*/
							} else if (SiemensShadowHeader.isShadowHeader(header, e)) {
								SiemensShadowHeader.addRows(t, header, e, fields.get(e.tag()));
							} else {
								//t.insertRow(makeRow(header, e, null, formatParams.valueLength));		
							}
						}
				}
				
			} catch (FileNotFoundException e2) {
				// Do nothing for now
			} catch (IOException e2) {
				// Do nothing for now
			}
			
			if (t.size()>0) {
				final ArrayList<Hashtable> rows = t.rowHashs();
				for (final Hashtable row : rows) {
					final String var = row.get("tag2").toString();
					final String val = row.get("value").toString();
					if (var.equals("FmriExternalInfo")) {
						if (val.indexOf('|')>0) {
							final String UUID = val.substring(0,val.indexOf('|'));
							if (UUID!=null && UUID.length()>0) {
								return UUID;
							}
						}
					}
				}
			}
		}
		return null;
	}
		
	
	 	/*
		 * Method lifted from DicomHeaderDump.java
		 */
	    private DicomObject getHeader(File f) throws IOException, FileNotFoundException {
	        final int stopTag;
	        if (fields.isEmpty()) {
	            stopTag = Tag.PixelData;
	        } else {
	            stopTag = 1 + Collections.max(fields.keySet());
	        }
	        final StopTagInputHandler stopHandler = new StopTagInputHandler(stopTag);

	        IOException ioexception = null;
	        final DicomInputStream dis = new DicomInputStream(f);
	        try {
	            dis.setHandler(stopHandler);
	            return dis.readDicomObject();
	        } catch (IOException e) {
	            throw ioexception = e;
	        } finally {
	            try {
	                dis.close();
	            } catch (IOException e) {
	                if (null != ioexception) {
	                    logger.error("unable to close DicomInputStream", e);
	                    throw ioexception;
	                } else {
	                    throw e;
	                }
	            }
	        }
	    }
	    /*
	     * Method lifted from DicomHeaderDump.java
	     */
	    String[] makeRow(DicomObject o, DicomElement e, String parentTag , int maxLen) {
	        String tag = TagUtils.toString(e.tag());
	        String value = "";

	        // If this element has nested tags it doesn't have a value and trying to 
	        // extract one using dcm4che will result in an UnsupportedOperationException 
	        // so check first.
	        if (!e.hasDicomObjects()) {
	            value = e.getValueAsString(null, maxLen);	
	        }
	        else {
	            value = "";
	        } 

	        String vr = e.vr().toString();
	        String desc = o.nameOf(e.tag());
	        List<String> l = new ArrayList<String>();
	        if (parentTag == null) {
	            String[] _s = {tag,"",vr,value,desc};
	            l.addAll(Arrays.asList(_s));
	        }
	        else {
	            String[] _s = {parentTag, tag, vr, value, desc};
	            l.addAll(Arrays.asList(_s));
	        }
	        String[] row = l.toArray(new String[l.size()]);
	        return row;
	    }



	private Date getDateTimeByTags(final DicomObject dcmObj,final int dteval,final int tmeval) {
		final Date thisDate = dcmObj.getDate(dteval);
		final Date thisTime = dcmObj.getDate(tmeval);
		if (thisDate!=null && thisTime!=null) {
			final Calendar cal = Calendar.getInstance();
			final Calendar dCal = Calendar.getInstance();
			cal.setTime(thisTime);
			dCal.setTime(thisDate);
			cal.set(dCal.get(Calendar.YEAR),dCal.get(Calendar.MONTH),dCal.get(Calendar.DAY_OF_MONTH));
			return cal.getTime(); 
		}
		return null;
	}




	private List<XnatImagescandata> getAllScans(final XnatMrsessiondata exp) {
		return exp.getScans_scan();
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getBoldScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : BOLD_TYPES) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getMotionScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getMotionTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private String[] getMotionTypes() {
		// Currently, only structural and diffusion are being collected, but this may change, so we'll add in 
		// task and resting state scans now.  These are in different sessions anyway.
		return MOTION_TYPES;
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getEyeTracker3tScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		if (proj.getId().startsWith("DMC")) {
			return boldList;
		}
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getEyeTracker3tTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getEyeTrackerDmcScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		if (!proj.getId().startsWith("DMC")) {
			return boldList;
		}
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getEyeTrackerDmcTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private String[] getEyeTracker3tTypes() {
		return EYETRACKER3T_TYPES;
	}

	private String[] getEyeTrackerDmcTypes() {
		return EYETRACKERDMC_TYPES;
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getMatlab7tScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getMatlab7tTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType) &&
							scanEntry.getKey().getSeriesDescription().matches(MATLAB7T_SD_REGEX)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private String[] getMatlab7tTypes() {
		return MATLAB7T_TYPES;
	}
	


	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getEyeTracker7tScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		if (proj.getId().startsWith("DMC")) {
			return boldList;
		}
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getEyeTracker7tTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType) &&
							scanEntry.getKey().getSeriesDescription().matches(EYETRACKER7T_SD_REGEX)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private String[] getEyeTracker7tTypes() {
		return EYETRACKER7T_TYPES;
	}

	private List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> getAudioScans(List<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> scanList) {
		final ArrayList <Map<XnatImagescandata, Map<AcquisitionTimes, Date>>> boldList = new ArrayList<Map<XnatImagescandata, Map<AcquisitionTimes, Date>>>();
		for (Map<XnatImagescandata, Map<AcquisitionTimes, Date>> scanMap : scanList) {
			for (Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> scanEntry : scanMap.entrySet()) {
				for (final String scanType : getAudioTypes()) {
					if (scanEntry.getKey().getType().equalsIgnoreCase(scanType) &&
							scanEntry.getKey().getSeriesDescription().matches(AUDIO_SD_REGEX)) {
						boldList.add(scanMap);
					}
				}
			}
		}
		return boldList;
	}

	private String[] getAudioTypes() {
		return AUDIO_TYPES;
	}
	

	private  List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> usableScanList(final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oDatedScans) {
		final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> returnList = new ArrayList<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>>(); 
		// Using iterator here because removing within for/next can be unpredictable
		final Iterator<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> sIter = oDatedScans.iterator();
		while (sIter.hasNext()) {
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> eMap = sIter.next();
			final XnatImagescandata scan = eMap.keySet().iterator().next();
			if (!(scan.getQuality().equalsIgnoreCase("unusable") || scan.getQuality().equalsIgnoreCase("poor"))) {
				returnList.add(eMap);
			}
		}
		return returnList;
	}


	private void removeUnusableScans(final List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> oDatedScans) {
		// Using iterator here because removing within for/next can be unpredictable
		final Iterator<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> sIter = oDatedScans.iterator();
		while (sIter.hasNext()) {
			final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> eMap = sIter.next();
			final XnatImagescandata scan = eMap.keySet().iterator().next();
			if (scan.getQuality().equalsIgnoreCase("unusable") || scan.getQuality().equalsIgnoreCase("poor")) {
				sIter.remove();
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List getDateOrderedEprimeList(final Map<File, Date> datedLinkedFile) {
		final LinkedList<Map.Entry<File,Date>> list = new LinkedList(datedLinkedFile.entrySet());
	    Collections.sort(list, new Comparator<Map.Entry<File,Date>>() {
			@Override
			public int compare(Map.Entry<File, Date> o1, Map.Entry<File, Date> o2) {
	               if (o1.getValue().before(o2.getValue())) {
	            	   return -1;
	               } else if (o1.getValue().equals(o2.getValue())) {
	            	   return 0;
	               } else {
	            	   return 1;
	               }
			}
	    });
	    List<Map<File,Date>> rList = new ArrayList<Map<File,Date>>();
	    for (final Map.Entry<File,Date>entry : list) {
	        final HashMap<File,Date> tMap = new HashMap<File,Date>();
	        tMap.put(entry.getKey(), entry.getValue());
	        rList.add(tMap);
	    }
	    return rList;
	}

	private List<Map<XnatImagescandata,File>> getScanOrderedList(Map<XnatImagescandata, File> inMap) {
		final LinkedList<Map.Entry<XnatImagescandata,File>> list = new LinkedList<Map.Entry<XnatImagescandata,File>>(inMap.entrySet());
	    Collections.sort(list, new Comparator<Map.Entry<XnatImagescandata,File>>() {
			@Override
			public int compare(Map.Entry<XnatImagescandata, File> o1, Map.Entry<XnatImagescandata, File> o2) {
				   return o1.getKey().getId().compareTo(o2.getKey().getId());
			}
	    });
	    List<Map<XnatImagescandata,File>> rList = new ArrayList<Map<XnatImagescandata,File>>();
	    for (final Map.Entry<XnatImagescandata,File>entry : list) {
	        final HashMap<XnatImagescandata,File> tMap = new HashMap<XnatImagescandata,File>();
	        tMap.put(entry.getKey(), entry.getValue());
	        rList.add(tMap);
	    }
	    return rList;
	}

	private List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> getDateOrderedScanList(final Map<XnatImagescandata,Map<AcquisitionTimes,Date>> inMap) {
		final LinkedList<Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>>> list = new LinkedList<Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>>>(inMap.entrySet());
	    Collections.sort(list, new Comparator<Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>>>() {
			@Override
			public int compare(Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> o1, Map.Entry<XnatImagescandata, Map<AcquisitionTimes, Date>> o2) {
	               if (o1.getValue().get(AcquisitionTimes.ACQ_START).before(o2.getValue().get(AcquisitionTimes.ACQ_START))) {
	            	   return -1;
	               } else if (o1.getValue().get(AcquisitionTimes.ACQ_START).equals(o2.getValue().get(AcquisitionTimes.ACQ_START))) {
	            	   return 0;
	               } else {
	            	   return 1;
	               }
			}
	    });
	    List<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>> rList = new ArrayList<Map<XnatImagescandata,Map<AcquisitionTimes,Date>>>();
	    for (final Map.Entry<XnatImagescandata,Map<AcquisitionTimes,Date>>entry : list) {
	        final HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>> tMap = new HashMap<XnatImagescandata,Map<AcquisitionTimes,Date>>();
	        tMap.put(entry.getKey(), entry.getValue());
	        rList.add(tMap);
	    }
	    return rList;
	}

	private List<Map<File,Map<AcquisitionTimes,Date>>> getDateOrderedMotionList(final Map<File,Map<AcquisitionTimes,Date>> inMap) {
		final LinkedList<Map.Entry<File,Map<AcquisitionTimes,Date>>> list = new LinkedList<Map.Entry<File,Map<AcquisitionTimes,Date>>>(inMap.entrySet());
	    Collections.sort(list, new Comparator<Map.Entry<File,Map<AcquisitionTimes,Date>>>() {
			@Override
			public int compare(Map.Entry<File, Map<AcquisitionTimes, Date>> o1, Map.Entry<File, Map<AcquisitionTimes, Date>> o2) {
	               if (o1.getValue().get(AcquisitionTimes.ACQ_START).before(o2.getValue().get(AcquisitionTimes.ACQ_START))) {
	            	   return -1;
	               } else if (o1.getValue().get(AcquisitionTimes.ACQ_START).equals(o2.getValue().get(AcquisitionTimes.ACQ_START))) {
	            	   return 0;
	               } else {
	            	   return 1;
	               }
			}
	    });
	    List<Map<File,Map<AcquisitionTimes,Date>>> rList = new ArrayList<Map<File,Map<AcquisitionTimes,Date>>>();
	    for (final Map.Entry<File,Map<AcquisitionTimes,Date>>entry : list) {
	        final HashMap<File,Map<AcquisitionTimes,Date>> tMap = new HashMap<File,Map<AcquisitionTimes,Date>>();
	        tMap.put(entry.getKey(), entry.getValue());
	        rList.add(tMap);
	    }
	    return rList;
	}

	private ZipI getZipper(final String fileName) {
		
		// Assume file name represents correct compression method
		String file_extension = null;
		if (fileName!=null && fileName.indexOf(".")!=-1) {
			file_extension = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
			if (Arrays.asList(ZIP_EXT).contains(file_extension)) {
				return new ZipUtils();
			} else if (file_extension.equalsIgnoreCase(".tar")) {
				return new TarUtils();
			} else if (file_extension.equalsIgnoreCase(".gz")) {
				TarUtils zipper = new TarUtils();
				zipper.setCompressionMethod(ZipOutputStream.DEFLATED);
				return zipper;
			}
		}
		// Assume zip-compression for unnamed inbody files
		return new ZipUtils();
		
	}
	
	
	private DirectScanResourceImpl getScanModifier(final XnatImagescandata scan,final EventMetaI ci) {
		if (scanModifiers.containsKey(scan.getId())) {
			return scanModifiers.get(scan.getId());
		} else {
			final DirectScanResourceImpl scanModifier = new DirectScanResourceImpl(scan,exp,true,user,ci);
			scanModifiers.put(scan.getId(),scanModifier);
			return scanModifier;
		}
	}

	private DirectExptResourceImpl getSessionModifier(final XnatMrsessiondata session,final EventMetaI ci) {
		if (sessionModifier != null) {
			return sessionModifier;
		} else {
			final DirectExptResourceImpl sessionModifier = new DirectExptResourceImpl(proj,session,true,user,ci);
			return sessionModifier;
		}
	}
	
}


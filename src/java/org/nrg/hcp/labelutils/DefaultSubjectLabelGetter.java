package org.nrg.hcp.labelutils;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.turbine.services.rundata.RunDataService;
import org.apache.turbine.services.rundata.TurbineRunDataFacade;
import org.apache.turbine.util.RunData;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.db.PoolDBUtils;

public class DefaultSubjectLabelGetter implements SubjectLabelGetterI {

	static org.apache.log4j.Logger logger = Logger.getLogger(DefaultSubjectLabelGetter.class);

	private HttpServletRequest request;
	private HttpServletResponse response;
	private ServletConfig sc;
	
	public DefaultSubjectLabelGetter(HttpServletRequest request,HttpServletResponse response,ServletConfig sc) {
		this.request = request;
		this.response = response;
		this.sc = sc;
	}
	
	@Override
	public void getNextLabel(String pattern,boolean setReadOnly) throws Exception {
		
			final String project = request.getParameter("project");
			
			// Split pattern into components
			pattern=pattern.replaceAll("#+$",",$0");
			String[] parray=pattern.split(",");
			if (parray.length!=2 && !(parray.length==1 && parray[0].matches("[^#]"))) {
				logger.error("Invalid pattern in IdGetter (" + this.getClass().getName() + ") - " + pattern);	
				response.getWriter().write("");
				return;
			}
			
			// Run query to retrieve latest (max) ID based on pattern
			final RunDataService rundataService = TurbineRunDataFacade.getService();
			final RunData data = rundataService.getRunData(request, response, sc);
			final XDATUser user=TurbineUtils.getUser(data);
			
			final StringBuilder querySB=new StringBuilder("select max(label) as maxID from xnat_subjectdata where project='");
			querySB.append(project);
			querySB.append("' and label similar to '"); 
			querySB.append(parray[0]);  
			querySB.append("\\\\d{");  
			querySB.append(parray[1].length()); 
			querySB.append("}'");
			final String maxID=(String)PoolDBUtils.ReturnStatisticQuery(querySB.toString(), "maxID", user.getDBName(), user.getLogin());

			final String newID;
			
			if (maxID!=null && maxID.length()>0) {
				// ID's already exist, return next one
				int incNum=Integer.valueOf(maxID.substring(parray[0].length())).intValue()+1;
				newID=parray[0] + String.format("%0" + parray[1].length() + "d",incNum);
			} else {
				// No ID's yet exist, return first one
				newID=parray[0] + String.format("%0" + parray[1].length() + "d",1);
				
			}
     	
			// write response (content type set by calling program)
			response.getWriter().write(newID + "," + setReadOnly);
     	
	}

}

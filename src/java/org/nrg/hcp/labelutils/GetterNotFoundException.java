package org.nrg.hcp.labelutils;

public class GetterNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public GetterNotFoundException(String string,
			IllegalArgumentException illegalArgumentException) {
		super(string,illegalArgumentException);
	}

}
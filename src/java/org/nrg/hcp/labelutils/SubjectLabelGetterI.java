package org.nrg.hcp.labelutils;

public interface SubjectLabelGetterI {

	void getNextLabel(String pattern, boolean setReadOnly) throws Exception;

}

package org.nrg.hcp.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.services.AliasTokenService;
import org.nrg.xdat.turbine.utils.TurbineUtils;

/**
 * Provides execution methods for resource scripts.  Ideally, these would be run as pipelines, but the current preference
 * is for these to run locally and for the page to wait for results.  May want to reconsider this later, especially
 * as changes/improvements are made to the pipeline engine to support wait and/or local execution.
 * @author Mike Hodge <hodgem@mir.wustl.edu>
 *
 */

public class HCPScriptExecUtils {
	
	static Logger logger = Logger.getLogger(HCPScriptExecUtils.class);
	
	private static final Map<XDATUser,AliasToken> _tokenMap = new HashMap();
	private static AliasTokenService _service;
	
	// This should be moved to configuration service when possible
	//static final String EV_PHYSIO_SCRIPT = "/opt/shared/workspace/ReleasePipeline/intradb/runEVandPhysioScript.sh";
	//static final String MATLAB_CONVERSION_SCRIPT = "/opt/shared/workspace/ReleasePipeline/intradb/runMatlabConversion.sh";
	//static final String EDF_CONVERSION_SCRIPT = "/opt/shared/workspace/ReleasePipeline/intradb/runEdfConversion.sh";
	//static final String SANITY_CHECK_SCRIPT = "/opt/shared/workspace/ReleasePipeline/intradb/runSanityChecks.sh";
	static final String EV_PHYSIO_SCRIPT = "/nrgpackages/tools/intradb/runEVandPhysioScript.sh";
	static final String MATLAB_CONVERSION_SCRIPT = "/nrgpackages/tools/intradb/runMatlabConversion.sh";
	static final String EDF_CONVERSION_SCRIPT = "/nrgpackages/tools/intradb/runEdfConversion.sh";
	static final String SANITY_CHECK_SCRIPT = "/nrgpackages/tools/intradb/runSanityChecks.sh";
	
	public static ScriptResult generateEVandPhysioTxtFiles(XDATUser user,XnatProjectdata proj,XnatMrsessiondata exp,Collection<XnatImagescandataI> scans,boolean anyEprime,boolean anyPhysio) {
		if (scans==null || scans.isEmpty()) {
			return new ScriptResult(false,null,new ArrayList<String>());
		}
		final AliasToken token = returnValidToken(user);
		final String EV_PHYSIO_ARGS = String.format(" -s %s -u %s -p %s -P %s -S %s -E %s -C %s -o%s%s", 
				new Object[]{   TurbineUtils.GetFullServerPath(),token.getAlias(),token.getSecret(),
								proj.getId(),exp.getSubjectData().getLabel(),exp.getLabel(),returnScanListString(scans),
								anyEprime ? "" : " -X", anyPhysio ? "" : " -Y" 
								} );
		return execRuntimeCommand(user,EV_PHYSIO_SCRIPT + EV_PHYSIO_ARGS);
	}
	
	public static ScriptResult generateEyetrackerConvertedFiles(XDATUser user,XnatProjectdata proj,XnatMrsessiondata exp,List<XnatImagescandataI> scans) {
		if (scans==null || scans.isEmpty()) {
			return new ScriptResult(false,null,new ArrayList<String>());
		}
		final AliasToken token = returnValidToken(user);
		final String EDF_ARGS = String.format(" -s %s -u %s -p %s -P %s -S %s -E %s -C %s", 
				new Object[]{   TurbineUtils.GetFullServerPath(),token.getAlias(),token.getSecret(),
								proj.getId(),exp.getSubjectData().getLabel(),exp.getLabel(),returnScanListString(scans)
								} );
		return execRuntimeCommand(user,EDF_CONVERSION_SCRIPT + EDF_ARGS);
	}
	
	public static ScriptResult generateMatlabConvertedFiles(XDATUser user,XnatProjectdata proj,XnatMrsessiondata exp,List<XnatImagescandataI> scans) {
		if (scans==null || scans.isEmpty()) {
			return new ScriptResult(false,null,new ArrayList<String>());
		}
		final AliasToken token = returnValidToken(user);
		final String MATLAB_ARGS = String.format(" -s %s -u %s -p %s -P %s -S %s -E %s -C %s", 
				new Object[]{   TurbineUtils.GetFullServerPath(),token.getAlias(),token.getSecret(),
								proj.getId(),exp.getSubjectData().getLabel(),exp.getLabel(),returnScanListString(scans)
								} );
		return execRuntimeCommand(user,MATLAB_CONVERSION_SCRIPT + MATLAB_ARGS);
	}

	public static ScriptResult runSanityChecks(XDATUser user,XnatProjectdata proj,XnatMrsessiondata exp,boolean skipHilemanChecks) {
		String skipChecks = (skipHilemanChecks) ? "-S" : "";
		final AliasToken token = returnValidToken(user);
		final String SANITY_CHECK_ARGS = String.format(" -s %s -u %s -p %s -P %s -S %s -E %s %s", 
				new Object[]{ TurbineUtils.GetFullServerPath(),token.getAlias(),token.getSecret(),
								proj.getId(),exp.getSubjectData().getLabel(),exp.getLabel(),skipChecks} );
		return execRuntimeCommand(user,SANITY_CHECK_SCRIPT + SANITY_CHECK_ARGS);
	}
	
	private static String returnScanListString(Collection<XnatImagescandataI> scans) {
		StringBuilder sb = new StringBuilder();
		Iterator<XnatImagescandataI> it = scans.iterator();
		while (it.hasNext()) {
			XnatImagescandataI scan = it.next();
			sb.append(scan.getId());
			if (it.hasNext()) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	private static ScriptResult execRuntimeCommand(XDATUser user,String cmd) {
		Integer returnCode = null;
		boolean returnStatus = false;
		List<String> returnList = new ArrayList<String>();;	
		try {
			final Process process = Runtime.getRuntime().exec(cmd);
		    final LineStreamConsumer stdout = new LineStreamConsumer(process.getInputStream()),
		    		stderr = new LineStreamConsumer(process.getErrorStream());
		    try {
		        stdout.start();
		        stderr.start();
		        if (logger.isTraceEnabled()) {
		        	String notifyStr =  String.format("executing command as %s with stdout, stderr consumers %s %s",
		                    new Object[]{cmd.toString(), stdout, stderr});
		            logger.trace(notifyStr);
		            //returnList.add(notifyStr);
		        }
		        final int rc = process.waitFor();
		        returnCode = rc;
		        returnList.addAll(stdout.getLines());
		        returnList.addAll(stderr.getLines());
		        if (0 == rc) {
		        	returnStatus = true;
		        } else if (98 == rc) {
		        	returnList.add("ERROR:  Duplicate process detected.  May only execute one instance of this script.");
		        } else {
		        	returnList.add("Process failed.");
		        }
		    } catch (InterruptedException e) {
					e.printStackTrace();
			} finally {
		       stdout.close();
		       stderr.close();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ScriptResult(returnStatus,returnCode,returnList);
	}
	
	private static AliasToken returnValidToken(XDATUser user) {
		AliasToken token = _tokenMap.get(user);
		if (token==null || getService().validateToken(token.getAlias(),token.getSecret())==null) {
			token = getService().issueTokenForUser(user);
			_tokenMap.put(user,token);
		}
		return token;
	}

    private static AliasTokenService getService() {
        if (_service == null) {
            _service = XDAT.getContextService().getBean(AliasTokenService.class);
        }
        return _service;
    }
    
}

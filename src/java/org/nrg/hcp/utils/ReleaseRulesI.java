package org.nrg.hcp.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;

public interface ReleaseRulesI {
	
	public List<String> applyRulesToScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, String selectionCriteria, boolean errorOverride) throws ReleaseRulesException;
	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta, SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap, String selectionCriteria,
		 HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt, boolean errorOverride) throws ReleaseRulesException;
	public boolean isStructuralScan(XnatImagescandataI scan);
	public ArrayList<XnatSubjectassessordata> filterExptList(ArrayList<XnatSubjectassessordata> projectExpts,String selectionCriteria) throws ReleaseRulesException;
	
}


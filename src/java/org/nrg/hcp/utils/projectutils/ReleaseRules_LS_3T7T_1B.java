package org.nrg.hcp.utils.projectutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.nrg.hcp.utils.ReleaseRulesI;
import org.nrg.hcp.utils.ReleaseRulesException;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatSubjectassessordata;

import com.google.common.collect.Lists;

public class ReleaseRules_LS_3T7T_1B implements ReleaseRulesI {
	
	public static final String  LOCALIZER_RGX = "^Localizer.*", 
								SCOUT_RGX = "^AAH.*Scout.*",
								T1WMEG_RGX = "^T1w_MEG.*",
								BIAS_RGX = "(?i)^BIAS_.*", 
								FIELDMAP_RGX = "^FieldMap.*",
								ANYFIELDMAP_RGX = "(?i)^.*FieldMap.*",
								GEFIELDMAP_RGX = "(?i)^FieldMap_((Ma)|(Ph)).*",
								SEFIELDMAP_RGX = "^FieldMap_SE_.*",
								SPINECHO_RGX = "(?i)^SpinEchoFieldMap.*",
								STRUCT_RGX = "(?i)^T[12]w.*",
								T1_RGX = "(?i)^T1w.*",
								T2_RGX = "(?i)^T2w.*", 
								NORM_RGX = "(?i)^_Norm.*", 
								NORMTYPE_ENDING = "_Norm", 
								NORMPARM_INDICATOR = "\\NORM", 
								T1MPR_RGX = "(?i)^T1w_MPR[0-9]*.*$",
								T2SPC_RGX = "(?i)^T2w_SPC[0-9]*.*$",
								RFMRI_RGX = "(?i)^rfMRI.*", 
								DWI_RGX = "(?i)^DWI.*", 
								DMRI_RGX = "(?i)^dMRI.*", 
								SBREF_RGX = "(?i)^.*_SBRef$", 
								AP_RGX = "(?i)^.*_AP(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								PA_RGX = "(?i)^.*_PA(((_dir[0-9][0-9])?_SBRef)|(_SB_SE)|(_dir[0-9][0-9]))?$", 
								APPA_RGX = "(?i)_((PA)|(AP))(($)|(_.*$))", 
								APPAONLY_RGX = "(?i)_((AP)|(PA))_", 
								MAINSCAN_RGX = "(?i)((T[12]w)|(tfMRI)|(rfMRI)|(dMRI))",
								STRUCTMAIN_RGX = "(?i)((T[12]w))",
								MAINSCANDESC_RGX = "(?i)^((T[12]w)|(tfMRI)|(rfMRI)|(DWI)).*$",
								STRUCTSCANDESC_RGX = "(?i)^((T[12]w)).*$",
								VNAV_RGX = "(?i)^((T[12]w)).*_vNav_.*$",
								OTHER_RGX = "(?i)^.*MPR.*Collection.*$"
								;
	
	public String SELECTCRITERIA;
	public String SELECTCRITERIA_RGX;
	
	public static final String[] validQualityRatings = {"undetermined","usable","unusable","excellent","good","fair","poor"};
	public static final String[] structuralQualityRatings = {"excellent","good","fair","poor"};
	
	public final List<String> errorList = Lists.newArrayList();
	public final List<String> warningList = Lists.newArrayList();
	private boolean errorOverride;
	
	@Override
	public List<String> applyRulesToScans(
			SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans,
			String selectionCriteria, boolean errorOverride)
			throws ReleaseRulesException {
		setSelectCriteriaRgx(selectionCriteria);
		// Remove scans from sessions that do not meet selection criteria
		Iterator<Map.Entry<XnatMrscandata,XnatMrsessiondata>> i = subjectScans.entrySet().iterator();
		while (i.hasNext()) {
			final String sessionLabel = i.next().getValue().getLabel();
			if (sessionLabel == null || !sessionLabel.matches(SELECTCRITERIA_RGX)) {
				i.remove();
			}
		}
		if (subjectScans.size()<1) {
			throw new ReleaseRulesException("No sessions meet selection criteria");
		}
		// NOTE:  Order of these calls is VERY important
		this.errorOverride = errorOverride;
		nullPriorValues(subjectScans);
		setCountScanAndSession(subjectScans);
		checkForNormalizedScans(subjectScans);
		if (selectionCriteria.equalsIgnoreCase("3T")) {
			setTargetForReleaseAndScanID_3T(subjectScans);
		} else if (selectionCriteria.equalsIgnoreCase("7T")) {
			setTargetForReleaseAndScanID_7T(subjectScans);
		}
		setTypeDescriptionAndOthers(subjectScans);
		setGroupValues(subjectScans);
		if (selectionCriteria.equalsIgnoreCase("3T")) {
			updateT1T2InclusionAndDesc(subjectScans);
		}
		updaterfMRIDescAndFlagError(subjectScans);
		useLaterBiasGroupIfMissing(subjectScans);
		excludeUnneededBiasScans(subjectScans);
		rearrangeDWIDesc(subjectScans);
		setScanOrderAndScanComplete(subjectScans);
		flagDwiError(subjectScans);
		hardCodedValues(subjectScans);
		fixBackslashProblem(subjectScans);
		if (!errorOverride && !errorList.isEmpty()) {
			throw new ReleaseRulesException(errorList.toString());
		}
		return warningList;
	}

	private void setSelectCriteriaRgx(String selectionCriteria) throws ReleaseRulesException {
		this.SELECTCRITERIA=selectionCriteria;
		if (selectionCriteria!=null && selectionCriteria.equalsIgnoreCase("3T")) {
			this.SELECTCRITERIA_RGX="(?i)^.*_3T.*"; 
		} else if (selectionCriteria!=null && selectionCriteria.equalsIgnoreCase("7T")) {
			this.SELECTCRITERIA_RGX="(?i)^.*_7T.*"; 
		} else {
			throw new ReleaseRulesException("Selection criteria must be supplied and have a valid value (valid values = 3T or 7T).");
		}
	}

	@Override
	public void applyRulesToSubjectMetaData(HcpSubjectmetadata subjMeta,
			SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap,
			String selectionCriteria, HcpToolboxdata tboxexpt, NtScores ntexpt,
			HcpvisitHcpvisitdata visitexpt, boolean errorOverride)
			throws ReleaseRulesException {
		// No metadata has been defined for this project
	}

	@Override
	public ArrayList<XnatSubjectassessordata> filterExptList(ArrayList<XnatSubjectassessordata> projectExpts, String selectionCriteria) throws ReleaseRulesException {
		setSelectCriteriaRgx(selectionCriteria);
		Iterator<XnatSubjectassessordata> i = projectExpts.iterator();
		while (i.hasNext()) {
			final String sessionLabel = i.next().getLabel();
			if (sessionLabel == null || !sessionLabel.matches(SELECTCRITERIA_RGX)) {
				i.remove();
			}
		}
		return projectExpts;
	}

	private List<String> getErrorOrWarningList() {
		return (errorOverride) ? warningList : errorList;
	}

	private void fixBackslashProblem(
			final SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			// Replace multiple backslash instances with a single backslash
			if (scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains("\\\\")) {
				final String imageType = scan.getParameters_imagetype().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_imagetype(imageType);
			}
			if (scan.getParameters_seqvariant()!=null && scan.getParameters_seqvariant().contains("\\\\")) {
				final String seqVariant = scan.getParameters_seqvariant().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_seqvariant(seqVariant);
			}
			if (scan.getParameters_scansequence()!=null && scan.getParameters_scansequence().contains("\\\\")) {
				final String scanSequence = scan.getParameters_scansequence().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scansequence(scanSequence);
			}
			if (scan.getParameters_scanoptions()!=null && scan.getParameters_scanoptions().contains("\\\\")) {
				final String scanOptions = scan.getParameters_scanoptions().replaceAll("[\\\\]+","\\\\");
				scan.setParameters_scanoptions(scanOptions);
			}
		}
	}

	private void nullPriorValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		errorList.clear();
		warningList.clear();
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			scan.setSubjectsessionnum(null);
			scan.setDatarelease(null);
			scan.setDbsession(null);
			scan.setDbid(null);
			scan.setDbtype(null);
			scan.setDbdesc(null);
			scan.setReleasecountscan(null);
			scan.setReleasecountscanoverride(null);
			scan.setTargetforrelease(null);
			// IMPORTANT:  Do not null out releaseOverride.  It is set by users, not by this class to force publish/non-publish.
			scan.setParameters_protocolphaseencodingdir(null);
			scan.setParameters_shimgroup(null);
			scan.setParameters_sefieldmapgroup(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_gefieldmapgroup(null);
			scan.setParameters_perotation(null);
			scan.setParameters_peswap(null);
			scan.setParameters_pedirection(null);
			scan.setParameters_readoutdirection(null);
			scan.setParameters_eprimescriptnum(null);
			scan.setParameters_biasgroup(null);
			scan.setParameters_scanorder(null);
			scan.setScancomplete(null);
			scan.setViewscan(null);
		}
	}

	private void setCountScanAndSession(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// This section was recoded to allow for some early problems in the setting of acquisition times
		// These should actually be corrected by the time of release, but trying obtain counts by 
		// moving sequentially through the scans was problematic, since the acquisition times sometimes
		// intermingled scans from different sessions in the combined session.
		final ArrayList<XnatMrsessiondata> scList = new ArrayList<XnatMrsessiondata>();
		final ArrayList<Object> dayList = new ArrayList<Object>();
		final HashMap<XnatMrsessiondata,Integer> dayMap = new HashMap<XnatMrsessiondata,Integer>();
		// Get and date sessions
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			final XnatMrsessiondata currSess = subjectScans.get(scan);
			if (!scList.contains(currSess)) {
				scList.add(currSess);
				final Object currSessionDate = currSess.getDate();
				boolean containsDate = false;
				for (final Object compareDate : dayList) {
					if (currSessionDate.equals(compareDate)) {
						containsDate = true;
						dayMap.put(currSess, dayList.indexOf(compareDate));
						break;
					}
				}
				if (!containsDate) {
					dayList.add(currSessionDate);
					dayMap.put(currSess, dayList.indexOf(currSessionDate));
				}
			}
			scan.setSubjectsessionnum(scList.indexOf(currSess)+1);
			scan.setSessionday("Day " + Integer.toString(dayMap.get(currSess)+1));
			if (
					// 1)  Exclude specific types/descriptions
					scan.getType() == null || scan.getType().matches(LOCALIZER_RGX) || scan.getType().matches(SCOUT_RGX) || 
					scan.getType().matches(OTHER_RGX) || scan.getSeriesDescription().matches(OTHER_RGX) || 
					scan.getType().matches(T1WMEG_RGX) || scan.getSeriesDescription().matches(T1WMEG_RGX) || 
					scan.getType().matches(VNAV_RGX) ||  scan.getSeriesDescription().matches(VNAV_RGX) || 
					// 2)  Exclude scans with unusable quality (IMPORTANT!!!! Do not set fair/poor here or override flag will not work!!!)
					scan.getQuality() == null || scan.getQuality().equalsIgnoreCase("unusable") 
				) {
				scan.setReleasecountscan(false);
				continue;
			}
			// throw error or exclude scan if it isn't rated with an expected quality rating
			if (isStructuralScan(scan)) {
				if (!(scan.getQuality()!=null && Arrays.asList(structuralQualityRatings).contains(scan.getQuality()))) {
					getErrorOrWarningList().add("RULESERROR:  Structural scan quality rating is invalid or scan has not been rated (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			} else {
				if (!(scan.getQuality()!=null && Arrays.asList(validQualityRatings).contains(scan.getQuality()))) {
					getErrorOrWarningList().add("RULESERROR:  Scan quality rating is invalid (SCAN=" + scan.getId() +
								", DESC=" + scan.getSeriesDescription() + ", QUALITY=" + scan.getQuality() + ")");
					scan.setReleasecountscan(false);
					continue;
				}
			}
			scan.setReleasecountscan(true);
		}
	}

	private void checkForNormalizedScans(final SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Throw error if Structural scan doesn't contain a normalized scan as the following scan
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			if (structNotVnav(scan,false) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
				if (!(isNormalizedScan(nextScan) && scan.getQuality().equalsIgnoreCase(nextScan.getQuality()))) {
					getErrorOrWarningList().add("RULESERROR:  CDB Structural scan is not followed by a normalized scan of the same quality (SCAN=" + subjectScans.get(scan).getLabel() + ")");
				}
			}
		}
	}

	private boolean isNormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(STRUCT_RGX) && !scan.getType().matches(VNAV_RGX) && !scan.getSeriesDescription().matches(VNAV_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains(NORMPARM_INDICATOR)) {
			
			return true;
		}
		return false;
	}

	private XnatMrscandata getNextScan(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.keySet().toArray(new XnatMrscandata[subjectScans.keySet().size()]);
		// NOTE:  Returns null for last scan in array
		for (int i=0; i<(scanArray.length-1); i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i+1];
			}
		}
		return null;
	}

	/*
	private XnatMrscandata getPreviousScan(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, XnatMrscandata scan) {
		final XnatMrscandata[] scanArray = subjectScans.keySet().toArray(new XnatMrscandata[subjectScans.keySet().size()]);
		// NOTE:  Returns null for first scan in array
		for (int i=1; i<scanArray.length; i++) {
			if (scan.equals(scanArray[i])) {
				return scanArray[i-1];
			}
		}
		return null;
	}
	*/

	private void setTargetForReleaseAndScanID_3T(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
				scan.setTargetforrelease(false);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					scan.getQuality().equalsIgnoreCase("poor") ||
					// 3)  COUNTSCAN - Exclude fair quality T1 or T2 
					(scan.getQuality().equalsIgnoreCase("fair") && structNotVnav(scan,true)) ||
					// 4)  COUNTSCAN - Exclude GE FieldMaps except for structural sessions
					//     UPDATE:  Per Mike Harms 2014/06, exclude all GE FieldMaps for LifeSpan
					(scan.getSeriesDescription().matches(FIELDMAP_RGX))
				) {
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
		// Second pass - For LifeSpan will count fair scans when we don't have good scans for release.
		if (!hasValidT1AndT2(subjectScans)) {
			boolean hasGoodT1 = false, hasGoodT2 = false;
			for (final XnatMrscandata scan : subjectScans.keySet()) {
				if ((scan.getQuality().equalsIgnoreCase("good") || scan.getQuality().equalsIgnoreCase("excellent")) && structNotVnav(scan,false)) {
					if (isT1Type(scan,false)) {
						hasGoodT1 = true;
					} else if (isT2Type(scan,false)) {
						hasGoodT2 = true;
					}
				}
				if (scan.getQuality().equalsIgnoreCase("fair") && structNotVnav(scan,false)) {
					scan.setTargetforrelease(true);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isNormalizedScan(nextScan)) {
						nextScan.setTargetforrelease(true);
					}
				}
			}
			// Per Mike Harms 2014/06, don't keep fair scans if we have a good in the group.
			if (hasGoodT1 || hasGoodT2) {
				for (final XnatMrscandata scan : subjectScans.keySet()) {
					if (hasGoodT1 && scan.getQuality().equalsIgnoreCase("fair") && isT1Type(scan,false)) {
						scan.setTargetforrelease(false);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							nextScan.setTargetforrelease(false);
						}
					} else if (hasGoodT2 && scan.getQuality().equalsIgnoreCase("fair") && isT2Type(scan,false)) {
						scan.setTargetforrelease(false);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							nextScan.setTargetforrelease(false);
						}
					}
				}
			}
		}
		// Third pass - set Targetforrelease to FALSE if no valid T1 AND T2 scans
		if (!hasValidT1AndT2(subjectScans)) {
			for (final XnatMrscandata scan : subjectScans.keySet()) {
				scan.setTargetforrelease(false);
			}
		}
	}

	private void setTargetForReleaseAndScanID_7T(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Sets targetForRelease and some other fields dependent on scanCount
		// Initial pass
		Integer prevSess = null;
		int ones = 0;
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			// 1)  COUNTSCAN - Exclude scans we're not counting
			if (!scan.getReleasecountscan()) { 
				scan.setTargetforrelease(false);
				scan.setDbid(null);
				continue;
						
			}
			if (prevSess == null || scan.getSubjectsessionnum() == null || !scan.getSubjectsessionnum().equals(prevSess)) {
				ones = 0;
				prevSess = scan.getSubjectsessionnum();
			}
			ones++;
			scan.setDbid(Integer.toString(scan.getSubjectsessionnum() * 100 + ones));
			if (
					// 2)  COUNTSCAN - Exclude scans with poor quality
					scan.getQuality().equalsIgnoreCase("poor") ||
					// 4)  COUNTSCAN - Exclude GE FieldMaps except for structural sessions
					//     UPDATE:  Per Mike Harms 2014/06, exclude all GE FieldMaps for LifeSpan
					(scan.getSeriesDescription().matches(FIELDMAP_RGX))
				) {
				scan.setTargetforrelease(false);
				continue;
			}
			scan.setTargetforrelease(true);
		}
	}
	
	private boolean isT1NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T1_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains(NORMPARM_INDICATOR)) {
			return true;
		}
		return false;
	}
	
	private boolean isT1Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T1_RGX) &&
				(normAsStruc ||  
				(scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().contains(NORMPARM_INDICATOR))
				)) {
			return true;
		}
		return false;
	}
	
	private boolean isT2NormalizedScan(XnatMrscandata scan) {
		if (scan.getType()!=null && scan.getType().matches(T2_RGX) &&
				scan.getParameters_imagetype()!=null && scan.getParameters_imagetype().contains(NORMPARM_INDICATOR)) {
			return true;
		}
		return false;
	}
	
	private boolean isT2Type(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(T2_RGX) &&
				(normAsStruc ||  
				(scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().contains(NORMPARM_INDICATOR))
				)) {
			return true;
		}
		return false;
	}
	
	private boolean structNotVnav(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getType()!=null && scan.getType().matches(STRUCT_RGX) &&
				(scan.getType()==null || !scan.getType().matches(VNAV_RGX)) && 
				(scan.getSeriesDescription()==null || !scan.getSeriesDescription().matches(VNAV_RGX)) && 
				(normAsStruc ||		
				((scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().contains(NORMPARM_INDICATOR))))) {
			return true;
		}
		return false;
	}
	
	private boolean mainNotVnav(XnatMrscandata scan,boolean normAsStruc) {
		if (scan.getDbdesc()!=null && scan.getDbdesc().matches(MAINSCANDESC_RGX) &&
				(scan.getType()==null || !scan.getType().matches(VNAV_RGX)) && 
				(scan.getSeriesDescription()==null || !scan.getSeriesDescription().matches(VNAV_RGX)) && 
				(normAsStruc ||		
				((scan.getParameters_imagetype()==null || !scan.getParameters_imagetype().contains(NORMPARM_INDICATOR))))) {
			return true;
		}
		return false;
	}

	private boolean hasValidT1AndT2(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		boolean hasT1 = false;
		boolean hasT2 = false;
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			if (isTargetOrOverrideAll(scan)) {
				if (isT1Type(scan,false)) {
					hasT1 = true;
				} else if (isT2Type(scan,false)) {
					hasT2 = true;
				}
				if (hasT1 && hasT2) {
					return true;
				}
			}
		}
		return false;
	}

	private void setTypeDescriptionAndOthers(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// These are set only for scans targeted for release
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			if (!isTargetOrOverrideAll(scan)) {
				// clear out any current values
				scan.setDbtype(null);
				scan.setDbdesc(null);
				continue;
			}
			// Currently no changes for scan type
			scan.setDbtype(scan.getType());
			// SpinEchoFieldmaps
			final String currSeriesDesc = scan.getSeriesDescription();
			// Initially set Dbdesc to current series description
			scan.setDbdesc(currSeriesDesc);
			if (currSeriesDesc.contains("SpinEchoFieldMap")) {
				scan.setDbdesc(currSeriesDesc);
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(RFMRI_RGX)) {
				scan.setDbdesc(currSeriesDesc);
				setPEFields(scan,currSeriesDesc);
			}
			if (scan.getType().matches(DMRI_RGX)) {
				setPEFields(scan,currSeriesDesc);
			}
			scan.setViewscan(!scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")|(" + SBREF_RGX + ")"));
		}
	}

	private void updaterfMRIDescAndFlagError(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		boolean inRest = false, groupHasAP = false, groupHasPA = false;
		int restCount = 1;
		Integer prevRestSession = null;
		int intradbSessionRestCount = 0;
		int restGroupCount = 0;
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (currDbdesc.matches(RFMRI_RGX) && !scan.getQuality().equalsIgnoreCase("unusable")) {
				if (!currDbdesc.matches(SBREF_RGX)) {
					if (inRest && ((currDbdesc.matches(AP_RGX) && groupHasAP) || (currDbdesc.matches(PA_RGX) && groupHasPA))) {
						// Handle case of multiple REST groups with no intervening scans (not sure that this would ever occur).
						restCount++;
						restGroupCount=0;
						groupHasAP = false;
						groupHasPA = false;
					} 
					if (prevRestSession == null || !scan.getSubjectsessionnum().equals(prevRestSession)) {
						prevRestSession = scan.getSubjectsessionnum();
						intradbSessionRestCount=1;
						restGroupCount++;
						if (currDbdesc.matches(AP_RGX)) {
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							groupHasPA = true;
						}
					} else {
						intradbSessionRestCount++;
						restGroupCount++;
						if (currDbdesc.matches(AP_RGX)) {
							groupHasAP = true;
							
						} else if (currDbdesc.matches(PA_RGX)) {
							groupHasPA = true;
						}
						if (restCount>2) {
							getErrorOrWarningList().add("RULESERROR:  CDB session contains more than two usable groups of Resting State scans (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
						if (intradbSessionRestCount>4) {
							getErrorOrWarningList().add("RULESERROR:  Intradb session contains more than four usable Resting State scans (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
						if (restGroupCount>2) {
							getErrorOrWarningList().add("RULESERROR:  Intradb session group contains more than two usable Resting State scans (SESSION=" + subjectScans.get(scan).getLabel() + ")");
						}
					}
				}
				inRest=true;
				// Update series description based on restCount
				scan.setDbdesc(currDbdesc.replaceFirst("(?i)rfMRI_REST","rfMRI_REST" + Integer.toString(restCount)));
			} else if ((!currDbdesc.matches(RFMRI_RGX) && 
					((scan.getViewscan()!=null && scan.getViewscan() && inRest) || 
						(restGroupCount>1 && scan.getType().matches("(" + ANYFIELDMAP_RGX + ")|(" + BIAS_RGX + ")")))) && inRest) {
				inRest=false;
				restCount++;
				restGroupCount=0;
				groupHasAP = false;
				groupHasPA = false;
			}
		}
	}
	
	private void updateT1T2InclusionAndDesc(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Target T1, T2 from session that has both
		final ArrayList<Integer> t1List = new ArrayList<Integer>();
		final ArrayList<Integer> t2List = new ArrayList<Integer>();
		final ArrayList<Integer> bothList = new ArrayList<Integer>();
		// Generate list of sessions with both a usable T1 and T2
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			String currSeriesDesc = scan.getSeriesDescription();
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t1List.contains(session)) {
						t1List.add(session);
					}
					if (t2List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				final Integer session = scan.getSubjectsessionnum();
				if (session!=null) {
					if (!t2List.contains(session)) {
						t2List.add(session);
					}
					if (t1List.contains(session) && !bothList.contains(session)) {
						bothList.add(session);
					}
				}
			}
		}
		if (bothList.size()>1) {
			// Per MHarms and DVE spreadsheet, throw error when multiple sessions contain T1 and T2
			// One set should be flagged as unusable
			getErrorOrWarningList().add("RULESERROR:  Multiple sessions contain usable T1/T2 scans");
		}
		// If more than one session contains T1's or T2's keep from session that contains both
		if (bothList.size()>0 && (t1List.size()>1 || t2List.size()>1)) {
			for (final XnatMrscandata scan : subjectScans.keySet()) {
				final String currSeriesDesc = scan.getSeriesDescription();
				if ((currSeriesDesc.matches(T1MPR_RGX) || currSeriesDesc.matches(T2SPC_RGX)) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
					Integer session = scan.getSubjectsessionnum();
					if (!bothList.contains(session)) {
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isNormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					}
				}
			}
		}
		// NOTE:  Per DB-1841, now doing this in two separate iterations, because if we have multiple scans we can use, we want to give 
		// preference to scans that have shim groups matching the fieldmaps.  So, we'll pass through once forwards only really processing 
		// if shim matches the fieldmaps, then we'll pass through descending, not looking at shim matches, and delete if we still have multiple.
		// In that pass through, the earlier scan that didn't match fieldmaps will be removed.
		int t1Count=0;
		int t2Count=0;
		char shimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				if (t1Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t1Count++;
				}
				if (t1Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					if (t1Count>2 || shimGroup!=getShimGroup(scan)) {
						t1Count--;
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT1NormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					} else {
						final String part1 = "T1w_MPR";
						final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t1Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t1Count);
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT1NormalizedScan(nextScan)) {
							nextScan.setDbdesc(part1+part2 + "_Norm");
							nextScan.setDbtype(nextScan.getType() + "_Norm");
							setRDFields(nextScan);
						}
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				if (t2Count>0 || scanShimMatchesFieldMaps(subjectScans,scan)) {
					t2Count++;
				}
				if (t2Count!=0) {
					if (shimGroup == Character.MIN_VALUE) {
						shimGroup = getShimGroup(scan);
					}
					if (t2Count>2 || shimGroup!=getShimGroup(scan)) {
						t2Count--;
						setNoRelease(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT2NormalizedScan(nextScan)) {
							setNoRelease(nextScan);
						}
					} else {
						final String part1 = "T2w_SPC";
						final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t2Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t2Count);
						scan.setDbdesc(part1+part2);
					    setRDFields(scan);
						final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
						if (isT2NormalizedScan(nextScan)) {
							nextScan.setDbdesc(part1+part2 + "_Norm");
							nextScan.setDbtype(nextScan.getType() + "_Norm");
							setRDFields(nextScan);
						}
					}
				}
			}
		}
		t1Count=0;
		t2Count=0;
		shimGroup = Character.MIN_VALUE;
		for (final XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) { 
			final String currSeriesDesc = scan.getSeriesDescription();
			// Keeping only 2 T1/T2 scans and numbering them 1 & 2 
			// Keeping second only if in same shim group as first
			if (currSeriesDesc.matches(T1MPR_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				t1Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t1Count>2 || shimGroup!=getShimGroup(scan)) {
					t1Count--;
					setNoRelease(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT1NormalizedScan(nextScan)) {
						setNoRelease(nextScan);
					}
				} else {
					String part1 = "T1w_MPR";
					String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t1Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t1Count);
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT1NormalizedScan(nextScan)) {
						nextScan.setDbdesc(part1+part2 + "_Norm");
						nextScan.setDbtype(nextScan.getType() + "_Norm");
						setRDFields(nextScan);
					}
				}
			}
			if (currSeriesDesc.matches(T2SPC_RGX) && !isNormalizedScan(scan) && isTargetOrOverrideStruc(scan)) {
				t2Count++;
				if (shimGroup == Character.MIN_VALUE) {
					shimGroup = getShimGroup(scan);
				}
				if (t2Count>2 || shimGroup!=getShimGroup(scan)) {
					t2Count--;
					setNoRelease(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT2NormalizedScan(nextScan)) {
						setNoRelease(nextScan);
					}
				} else {
					final String part1 = "T2w_SPC";
					final String part2 = currSeriesDesc.trim().length()>part1.trim().length() ? String.valueOf(t2Count) + currSeriesDesc.substring(part1.trim().length()+1) : String.valueOf(t2Count);
					scan.setDbdesc(part1+part2);
				    setRDFields(scan);
					final XnatMrscandata nextScan = getNextScan(subjectScans,scan);
					if (isT2NormalizedScan(nextScan)) {
						nextScan.setDbdesc(part1+part2 + "_Norm");
						nextScan.setDbtype(nextScan.getType() + "_Norm");
						setRDFields(nextScan);
					}
				}
			}
		}
	}		
	
	private boolean scanShimMatchesFieldMaps(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans, XnatMrscandata comparescan) {
		for (final XnatMrscandata scan : new TreeMap<XnatMrscandata, XnatMrsessiondata>(subjectScans).descendingKeySet()) {
			if (scan.getSeriesDescription().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) &&
						scan.getParameters_sefieldmapgroup().equals(comparescan.getParameters_sefieldmapgroup())) {
				if (scan.getParameters_shimgroup().equals(comparescan.getParameters_shimgroup())) {
					return true;
				} else {
					return false;
				}
			} 
		}
		return false;
	}
	
	private boolean isTargetOrOverrideAll(XnatMrscandata scan) {
		if (scan.getSeriesDescription()!=null && (scan.getSeriesDescription().matches(STRUCTSCANDESC_RGX) &&
					!scan.getSeriesDescription().matches(VNAV_RGX))) { if (isTargetOrOverrideStruc(scan)) {
				return true;
			}
		} else if (scan.getTargetforrelease()!=null) {
			return scan.getTargetforrelease();
		}
		return false;
	}
	
	private boolean isTargetOrOverrideStruc(XnatMrscandata scan) {
		if ( (scan.getTargetforrelease()!=null && scan.getTargetforrelease() && (scan.getReleaseoverride()==null || !(scan.getReleaseoverride()<0))) ||
				(scan.getTargetforrelease()!=null && !scan.getTargetforrelease() && scan.getReleaseoverride()!=null && (scan.getReleaseoverride()>0))
			) {
			return true;
		}
		return false;
	}

	private void setNoRelease(XnatMrscandata scan) {
		// Do this instead of just setting targetforrelease to false to clear out other fields that
		// may have been set (Note that a few aren't included here that should remain)
		scan.setTargetforrelease(false);
		scan.setSubjectsessionnum(null);
		scan.setDatarelease(null);
		scan.setDbsession(null);
		scan.setDbid(null);
		scan.setDbtype(null);
		scan.setDbdesc(null);
		scan.setParameters_protocolphaseencodingdir(null);
		scan.setParameters_sefieldmapgroup(null);
		scan.setParameters_gefieldmapgroup(null);
		scan.setParameters_perotation(null);
		scan.setParameters_peswap(null);
		scan.setParameters_pedirection(null);
		scan.setParameters_readoutdirection(null);
		scan.setParameters_eprimescriptnum(null);
		scan.setParameters_biasgroup(null);
	}

	private char getShimGroup(XnatMrscandata scan) {
		try {
			if (scan.getParameters_shimgroup() == null || scan.getParameters_shimgroup().length()<1) {
				return '0';
			}
			return scan.getParameters_shimgroup().charAt(0);
		} catch (Exception e) {
			return '0';
		}
	}
	
	private void setGroupValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		
		int biasFieldGroup = 0;
		int biasSession = 0;
		boolean inBiasGroup = false;
		
		int seFieldmapGroup = 0;
		int seSession = 0;
		boolean inSEFieldmapGroup = false;
		
		ArrayList<String> shimList =  new ArrayList<String>();
		
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			
			// SHIM GROUP 
			if (isTargetOrOverrideAll(scan)) {
				final List<XnatAddfield> addList = scan.getParameters_addparam();
				String shim_tablepos = "";
				String shim_loffset = "";
				String shim_shimcurr = "";
				for (final XnatAddfield addParam : addList) {
					if (addParam.getName() == null || addParam.getAddfield() == null) {
						continue;
					}
					if (addParam.getName().contains("ShimCurrent")) {
						shim_shimcurr = addParam.getAddfield();
					} else  if (addParam.getName().contains("lOffset")) {
						shim_loffset = addParam.getAddfield();
					} else  if (addParam.getName().contains("Table Position")) {
						shim_tablepos = addParam.getAddfield();
					}
				}
				final String shim_compare = shim_tablepos + shim_loffset + shim_shimcurr;
				int inx = shimList.indexOf(shim_compare);
				if (inx>=0) {
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+inx)));
				} else {
					shimList.add(shim_compare);
					scan.setParameters_shimgroup(String.valueOf((char)((int)'A'+shimList.indexOf(shim_compare))));
				}
			}
			
			// BIAS FIELD GROUP
			if (scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && !inBiasGroup) {
				inBiasGroup = true;
				biasSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				biasFieldGroup++;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && scan.getReleasecountscan() && scan.getSubjectsessionnum().intValue() == biasSession) {
				inBiasGroup = false;
				scan.setParameters_biasgroup(biasFieldGroup);
			} else if (!scan.getSeriesDescription().matches(BIAS_RGX) && inBiasGroup) {
				inBiasGroup = false;
			} else if (scan.getReleasecountscan() && inBiasGroup && scan.getSubjectsessionnum().intValue() == biasSession) {
				scan.setParameters_biasgroup(biasFieldGroup);
			}
			
			// SE FIELDMAP GROUP
			if (scan.getType().matches(SEFIELDMAP_RGX) && isTargetOrOverrideAll(scan) && !inSEFieldmapGroup) {
				inSEFieldmapGroup = true;
				seFieldmapGroup++;
				seSession = scan.getSubjectsessionnum()!=null ? scan.getSubjectsessionnum() : 0;
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && !scan.getType().matches(BIAS_RGX) &&
						isTargetOrOverrideAll(scan) && scan.getSubjectsessionnum().intValue() == seSession) {
				inSEFieldmapGroup = false;
				if (seFieldmapGroup>0) {
					scan.setParameters_sefieldmapgroup(seFieldmapGroup);
				}
			} else if (!scan.getType().matches(SEFIELDMAP_RGX) && inSEFieldmapGroup) {
				inSEFieldmapGroup = false;
			} else if (isTargetOrOverrideAll(scan) && inSEFieldmapGroup && scan.getSubjectsessionnum().intValue() == seSession) {
				scan.setParameters_sefieldmapgroup(seFieldmapGroup);
			}
			
		}
		
	}		

	private void rearrangeDWIDesc(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null || !currDbdesc.matches(DWI_RGX)) {
				continue;
			}
			if (currDbdesc.indexOf("_AP_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_AP_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_AP_SBRef") : currDbdesc + "_AP";
			}
			if (currDbdesc.indexOf("_PA_")>0) {
				currDbdesc = currDbdesc.replaceFirst("_PA_","_");
				currDbdesc = currDbdesc.matches(SBREF_RGX) ? currDbdesc.replaceFirst("(?i)_SBRef","_PA_SBRef") : currDbdesc + "_PA";
			}
			scan.setDbdesc(currDbdesc);
		}
	}
	
	private void useLaterBiasGroupIfMissing(
			final SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// This method is to fix an issue first discovered with session 429040_3T where the initial set of bias scans
		// was unusable and a set was captured after the main scans.  These, having occurred in the same session,
		// should be usable, so when bias group is missing for a main scan, we'll use the later bias group in the
		// same session
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if (mainNotVnav(scan,false) && isTargetOrOverrideAll(scan)) {
				final Integer currBias = scan.getParameters_biasgroup();
				if (currBias != null ) {
					continue;
				}
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && (compareDbdesc.matches(BIAS_RGX) || compareDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan2)) {
						Integer compareBias = scan2.getParameters_biasgroup();
						if (compareBias!=null) {
							scan.setParameters_biasgroup(compareBias);
							break;
						}
					}
				}
			}
		}
	}
	
	private void excludeUnneededBiasScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Exclude BIAS or FieldMap scans from groups that don't have an associated main scan
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			final String currDbdesc = scan.getDbdesc();
			if (currDbdesc == null) {
				continue;
			}
			if ((currDbdesc.matches(BIAS_RGX) || currDbdesc.equalsIgnoreCase("AFI")) && isTargetOrOverrideAll(scan)) {
				final Integer currBias = scan.getParameters_biasgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareBias = scan2.getParameters_biasgroup();
						if (currBias!=null && compareBias!=null && compareBias.equals(currBias)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(GEFIELDMAP_RGX) && isTargetOrOverrideAll(scan)) {
				final Integer currGE = scan.getParameters_gefieldmapgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareGE = scan2.getParameters_gefieldmapgroup();
						if (currGE!=null && compareGE!=null && compareGE.equals(currGE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
			if (currDbdesc.matches(SPINECHO_RGX) && isTargetOrOverrideAll(scan)) {
				final Integer currSE = scan.getParameters_sefieldmapgroup();
				boolean hasMatch = false;
				for (final XnatMrscandata scan2 : subjectScans.keySet()) {
					final String compareDbdesc = scan2.getDbdesc();
					if (compareDbdesc!=null && mainNotVnav(scan2,false) && isTargetOrOverrideAll(scan2)) {
						final Integer compareSE = scan2.getParameters_sefieldmapgroup();
						if (currSE!=null && compareSE!=null && compareSE.equals(currSE)) {
							hasMatch = true;
							break;
						}
					}
				}
				if (!hasMatch) {
					scan.setTargetforrelease(false);
				}	
				
			}
		}
	}

	private void setScanOrderAndScanComplete(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
        final HashMap<String,Integer> countMap = new HashMap<String,Integer>();
        final HashMap<String,Double> pctMap = new HashMap<String,Double>();
        for (final XnatMrscandata scan : subjectScans.keySet()) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	// ScanOrder NOTE:  Want DWI ordered 1 to 6
               	String part1=(currDbdesc.matches(DWI_RGX)) ? "DWI" : currDbdesc.replaceFirst(APPA_RGX,"");
                if (countMap.containsKey(part1)) {
                	int mapval = countMap.get(part1);
                	mapval++;
                	scan.setParameters_scanorder(mapval);
                	countMap.put(part1,mapval);
                } else {
                	scan.setParameters_scanorder(1);
                	countMap.put(part1,1);
                }
            	// ScanComplete
                if (scan.getFrames() == null) {
                	continue;
                }
                int frames = scan.getFrames().intValue();
                String scanComplete = null;
                double scanPct = 0;
                if (currDbdesc.matches(RFMRI_RGX)) { 
                	int expectedFrames = (SELECTCRITERIA.equalsIgnoreCase("7T")) ? 300 : 420;
                	scanComplete = (frames>=expectedFrames) ? "Y" : "N";
               		scanPct = (double)frames/expectedFrames;
                } else if (currDbdesc.matches(DWI_RGX)) { 
                	final String dirVal = currDbdesc.replaceFirst("^.*_dir","").replaceFirst("_.*$", "");
                	try {
                		int dirNum = Integer.parseInt(dirVal);
                		scanComplete = (frames>=dirNum) ? "Y" : "N";
                		scanPct = (double)frames/dirNum;
                	} catch (NumberFormatException e) {
                		// Do nothing for now
                	}
                }
                // Per Greg, tfMRI should have at least 75% of frames to have been considered usable.
                if (scanPct<.75) {
                	getErrorOrWarningList().add("RULESERROR:  tfMRI, rfMRI and dMRI scans should have at least 75% of frames to have been marked usable (SCAN=" + scan.getId() + ", DESC=" + currDbdesc + ")");
                }
                if (scanPct>0) {
                	scanPct = (scanPct>100) ? 100 : scanPct;
                	scan.setPctcomplete(scanPct*100);
                	pctMap.put(currDbdesc,scanPct);
                }
                scan.setScancomplete(scanComplete);
            }
        }
        // Set scan pctComplete
        for (final XnatMrscandata scan : subjectScans.keySet()) {
            final String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(RFMRI_RGX) || currDbdesc.matches(DWI_RGX)) { 
            	final Double currScanPct = pctMap.get(currDbdesc);
	            final String thatDbdesc = (currDbdesc.matches(AP_RGX)) ? currDbdesc.replace("_AP","_PA") : currDbdesc.replace("_PA","_AP");  
	            Double thatScanPct = null;
	            if (!thatDbdesc.equals(currDbdesc)) {
	            	thatScanPct = pctMap.get(thatDbdesc);
	            }
	            // Per M. Harms e-mail (2013-02-01), we want the pair complete percent to be the minimum of the pair
	            // of values rather than an average of the two.
	            if (currScanPct!=null && thatScanPct!=null) {
	            	scan.setPctpaircomplete(Math.min(currScanPct, thatScanPct)*100);
	            }
            }
        }
	}
	
	private void setRDFields(XnatMrscandata scan) {
		Float ipe_rotation;
		try {
			if (scan.getParameters_inplanephaseencoding_rotation() != null) {
				ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			} else {
				ipe_rotation = Float.valueOf(0);
			}
		} catch (Exception e) {
			ipe_rotation = Float.valueOf(0);
		}
		if (scan.getParameters_orientation().equals("Sag") && scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
				ipe_rotation>=-0.53 && ipe_rotation<=0.53) {
				scan.setParameters_readoutdirection("+z");
				return;
		}
		getErrorOrWarningList().add("RULESERROR:  Values do not match expected for setting READOUT DIRECTION (SCAN=" + scan.getId() + ", DESC=" + scan.getSeriesDescription() + ")");
	}
	
	
	private void setPEFields(XnatMrscandata scan,String seriesDesc) {
		Float ipe_rotation;
		try {
			if (scan.getParameters_inplanephaseencoding_rotation() != null) {
				ipe_rotation = Float.parseFloat(scan.getParameters_inplanephaseencoding_rotation());
			} else {
				ipe_rotation = Float.valueOf(0);
			}
		} catch (Exception e) {
			ipe_rotation = Float.valueOf(0);
		}
		try {
			final Integer ipe_dirpos = Integer.parseInt(scan.getParameters_inplanephaseencoding_directionpositive());
			if (scan.getParameters_orientation().equals("Tra")) {
				if (scan.getParameters_inplanephaseencoding_direction().equals("ROW") &&
						ipe_rotation>=1.05 && ipe_rotation<=2.10) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("-x");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("+x");
						checkPESeriesDesc(scan);
						return;
					}
				} else if (scan.getParameters_inplanephaseencoding_direction().equals("COL") &&
						ipe_rotation>=-0.52 && ipe_rotation<=0.52) {
					if (ipe_dirpos==0)	{
						scan.setParameters_pedirection("+y");
						checkPESeriesDesc(scan);
						return;
					} else if (ipe_dirpos==1) {
						scan.setParameters_pedirection("-y");
						checkPESeriesDesc(scan);
						return;
					}
				}
			}
		} catch (Exception e) {
			// Do nothing
		}
		getErrorOrWarningList().add("RULESERROR:  Values do not match expected for setting PHASE ENCODING DIRECTION (SCAN=" + scan.getId() + ", DESC=" + seriesDesc + ")");
	}

	private void checkPESeriesDesc(XnatMrscandata scan) {
		if (!(scan.getParameters_pedirection().equals("+x") && scan.getSeriesDescription().contains("_RL") ||
			scan.getParameters_pedirection().equals("-x") && scan.getSeriesDescription().contains("_LR") ||
			scan.getParameters_pedirection().equals("+y") && scan.getSeriesDescription().contains("_PA") ||
			scan.getParameters_pedirection().equals("-y") && scan.getSeriesDescription().contains("_AP"))) {
			getErrorOrWarningList().add("RULESERROR:  Series Description does not reflect PHASE ENCODING DIRECTION (SCAN=" + scan.getId() +
					", DESC=" + scan.getSeriesDescription() + ",PE=" + scan.getParameters_pedirection() + ")");
		}
	}

	public boolean isStructuralScan(XnatImagescandataI scan) {
		// NOTE:  Important here to include nav scans so override is considered for release
		if (scan instanceof XnatMrscandata && structNotVnav((XnatMrscandata)scan,true)) {
			return true;
		} 
		return false;
	}

	/*
	private boolean notStructuralSession(XnatImagesessiondata session) {
		for (final XnatImagescandataI scan : session.getScans_scan()) {
			if (scan instanceof XnatMrscandata && structNotVnav((XnatMrscandata)scan,false)) {
				return false;
			}
		}
		return true;
	}
	*/
	
	private void hardCodedValues(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		for (final XnatMrscandata scan : subjectScans.keySet()) {
			final XnatMrsessiondata currSess = subjectScans.get(scan);
			if (currSess.getLabel() == null || scan.getSeriesDescription() == null) {
				continue;
			}
		}
	}

	private void flagDwiError(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) {
		// Flag error if multiple bias groups in TO BE RELEASED DWI scans 
		Integer prevBias = null; 
		for (final XnatMrscandata scan : subjectScans.keySet()) {
            String currDbdesc = scan.getDbdesc();
            if (currDbdesc == null || !isTargetOrOverrideAll(scan) || currDbdesc.matches(SBREF_RGX)) {
                    continue;
            }
            if (currDbdesc.matches(DWI_RGX)) { 
            	final Integer thisBias = scan.getParameters_biasgroup();
            	if (prevBias!=null && thisBias!=null && !thisBias.equals(prevBias)) {
           			getErrorOrWarningList().add("RULESERROR:  DWI contains multiple bias groups in scans to be released");
            	} else if (prevBias==null && thisBias!=null) {
            		prevBias = thisBias;
            	}
            }
		}
	}

}



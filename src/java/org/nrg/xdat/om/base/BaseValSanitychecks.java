/*
 * GENERATED FILE
 * Created on Mon Jan 06 12:22:07 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseValSanitychecks extends AutoValSanitychecks {

	public BaseValSanitychecks(ItemI item)
	{
		super(item);
	}

	public BaseValSanitychecks(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValSanitychecks(UserI user)
	 **/
	public BaseValSanitychecks()
	{}

	public BaseValSanitychecks(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}

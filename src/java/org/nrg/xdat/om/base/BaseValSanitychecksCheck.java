/*
 * GENERATED FILE
 * Created on Mon Jan 06 12:22:07 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseValSanitychecksCheck extends AutoValSanitychecksCheck {

	public BaseValSanitychecksCheck(ItemI item)
	{
		super(item);
	}

	public BaseValSanitychecksCheck(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValSanitychecksCheck(UserI user)
	 **/
	public BaseValSanitychecksCheck()
	{}

	public BaseValSanitychecksCheck(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}

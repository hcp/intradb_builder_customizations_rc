/*
 * GENERATED FILE
 * Created on Mon Jan 06 12:22:07 CST 2014
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseValSanitychecksScanChecksCheck extends AutoValSanitychecksScanChecksCheck {

	public BaseValSanitychecksScanChecksCheck(ItemI item)
	{
		super(item);
	}

	public BaseValSanitychecksScanChecksCheck(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValSanitychecksScanChecksCheck(UserI user)
	 **/
	public BaseValSanitychecksScanChecksCheck()
	{}

	public BaseValSanitychecksScanChecksCheck(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}

package org.nrg.xnat.restlet.extensions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.ArrayUtils;
import org.nrg.hcp.utils.ReleaseRulesI;
import org.nrg.hcp.utils.ReleaseRulesException;
import org.nrg.hcp.utils.ReleaseRulesUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.schema.Wrappers.GenericWrapper.GenericWrapperElement;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.search.QueryOrganizer;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.helpers.xmlpath.XMLPathShortcuts;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.ItemXMLRepresentation;
import org.nrg.xnat.restlet.resources.QueryOrganizerResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;


/* 
 * NOTE:  org.xnat.restlet.resources.ScanList was a starting point for this class.  Couldn't just extend it.  Needed access
 * 			to its protected variables.   
 */

@XnatRestlet({	"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/releaseTarget",
				"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/releaseTarget",
				"/experiments/{EXP_ID}/releaseTarget",
				"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/experiments/{EXP_ID}/scans/{SCAN_ID}/releaseTarget",
				"/experiments/{EXP_ID}/scans/{SCAN_ID}/releaseTarget"
			})
public class ReleaseTargetResource extends QueryOrganizerResource {
	
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ReleaseTargetResource.class);

	XnatProjectdata proj=null;
	XnatSubjectdata subj=null;
	ArrayList<XnatImagesessiondata> sessionList=new ArrayList<XnatImagesessiondata>();
	ArrayList<XnatImagescandata> scanList=new ArrayList<XnatImagescandata>();
	private final ReleaseRulesI releaseRules;
	
	private static final SimpleDateFormat timeformatter = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd");
	
	public ReleaseTargetResource(Context context, Request request, Response response) {
		super(context, request, response);

		String projID = (String)getParameter(request,"PROJECT_ID");
		String subjID = null;
		String exptID = null;
		if (projID!=null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projID, user, false);

			subjID= (String)getParameter(request,"SUBJECT_ID");
			if(subjID!=null){
				subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(), subjID, user, false);

				if(subj==null){
					subj = XnatSubjectdata.getXnatSubjectdatasById(subjID, user, false);
					if (subj != null && (proj != null && !subj.hasProject(proj.getId()))) {
						subj = null;
					}
				}

				if(subj!=null){
					exptID = (String) getParameter(request, "EXP_ID");
					if (exptID == null || exptID.length()<1) {
						
						final CriteriaCollection criteria=new CriteriaCollection("AND");

						final CriteriaCollection cc= new CriteriaCollection("OR");
						//// Currently we'll only retrieve MR session types
						cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/project", proj.getId());
						cc.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME+"/sharing/share/project", proj.getId());
						criteria.addClause(cc);
						criteria.addClause(XnatMrsessiondata.SCHEMA_ELEMENT_NAME + "/subject_ID", subj.getId());
						
						sessionList.addAll(XnatImagesessiondata.getXnatImagesessiondatasByField(criteria, user, false));
						
					} else {
						XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(exptID, user, false);
						if (session != null && (proj != null && !session.hasProject(proj.getId()))) {
							session = null;
						}
						if(session==null){
							session = (XnatImagesessiondata) XnatImagesessiondata
									.GetExptByProjectIdentifier(proj.getId(),
											exptID, user, false);
						}
						if(session!=null){
							this.getVariants().add(
									new Variant(MediaType.APPLICATION_JSON));
							this.getVariants()
									.add(new Variant(MediaType.TEXT_HTML));
								this.getVariants().add(new Variant(MediaType.TEXT_XML));
							sessionList.add(session);
						}else{
							response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
						}
					}
				}else{
					response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				}
			}else{
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}
		}else{
			exptID= (String)getParameter(request,"EXP_ID");
			XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(exptID, user, false);

			if(session==null){
				session = (XnatImagesessiondata) XnatImagesessiondata
						.GetExptByProjectIdentifier(proj.getId(), exptID, user,
								false);
			}

			if(session!=null){
					this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
					this.getVariants().add(new Variant(MediaType.TEXT_HTML));
					this.getVariants().add(new Variant(MediaType.TEXT_XML));
					sessionList.add(session);
			}else{
				response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			}
		}
		this.fieldMapping.putAll(XMLPathShortcuts.getInstance().getShortcuts(XnatImagescandata.SCHEMA_ELEMENT_NAME,true));
		
		Iterator<XnatImagesessiondata> i = sessionList.iterator();
		while (i.hasNext()) {
			XnatImagesessiondata session = i.next();
			if (!(session instanceof XnatMrsessiondata)) {
				i.remove();
			}
		}
		
		if (sessionList.size()<1) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
				"Rules currently have been defined only for MR Session data.  The experiment specified is not an MR Session (EXPT_ID=" + 
				exptID + ", TYPE=" + sessionList.get(0).getClass().getName() + ")");
		}

		final String scanID=(String)getParameter(request,"SCAN_ID");
		String[] scanIDs = null;
		if (!(scanID == null || scanID.equals("*"))) {
			scanIDs = scanID.split(",");
		}
		/// Only populate scanList if necessary
		if (scanIDs != null) {
			for (final XnatImagescandataI scan : sessionList.get(0).getScans_scan()) {
				// The current getRepresentation method doesn't really support wild cards.  We'll support them for POST
				if (request.getMethod().equals(Method.GET) && scanIDs!=null && scanIDs.length!=1) {
					continue;
				}
				if (ArrayUtils.contains(scanIDs, scan.getId())) {
					scanList.add((XnatImagescandata)scan);
				}
			}
		}
	
		if (proj == null) {
			proj = sessionList.get(0).getProjectData();
		}
		if (subj == null) {
			subj = sessionList.get(0).getSubjectData();
		}
		releaseRules = ReleaseRulesUtil.getReleaseRulesForProject(proj,this.getResponse());
		if (releaseRules == null) {
			return;
		}
		
	}


	@Override
	public boolean allowPost() {
		return true;
	}

	@Override
	public void handlePost() {
		// First, create ordered list of scans with their associated sessions
		final SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans = getOrderedScanMap();
		final List<String> warningList;
		try {
			warningList = processScans(subjectScans);
			this.getResponse().setEntity(new StringRepresentation((warningList.size()>0) ? listString(warningList) : "Release target processing complete.  No warnings or errors reported"));
		} catch (ReleaseRulesException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,e.getMessage());
		}
	}

	private String listString(List<String> l) {
		StringBuilder sb = new StringBuilder();
		for (String it : l) {
			sb.append(it + System.getProperty("line.separator"));
		}
		return sb.toString();
	}


	private SortedMap<XnatMrscandata, XnatMrsessiondata> getOrderedScanMap() {
			
		final HashMap<XnatMrscandata,XnatMrsessiondata> sssMap = new HashMap<XnatMrscandata,XnatMrsessiondata>();
			
		final Comparator<XnatMrscandata> scanCompare = new Comparator<XnatMrscandata>() {
		
			@Override
			public int compare(XnatMrscandata arg0, XnatMrscandata arg1) {
				try {
					arg0.getImageSessionData();
					final Date d0 = dateformatter.parse(sssMap.get(arg0).getDate().toString());
					final Date d1 = dateformatter.parse(sssMap.get(arg1).getDate().toString());
					if (d0.before(d1)) {
						return -1;
					} else if (d1.before(d0)) {
						return 1;
					} 
				} catch (Exception e) {
					// Do nothing for now
				}
				try {
					final Date t0 = timeformatter.parse(arg0.getStarttime().toString());
					final Date t1 = timeformatter.parse(arg1.getStarttime().toString());
					if (t0.before(t1)) {
						return -1;
					} else if (t1.before(t0)) {
						return 1;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
				try {
					if (Integer.parseInt(arg0.getId()) <  Integer.parseInt(arg1.getId())) {
						return -1;
					} else if (Integer.parseInt(arg1.getId()) <  Integer.parseInt(arg0.getId())) {
						return 1;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
				return 0;
			}
		};
		
		final SortedMap<XnatMrscandata, XnatMrsessiondata> returnMap = new TreeMap<XnatMrscandata,XnatMrsessiondata>(scanCompare);
		for (final XnatImagesessiondata session : sessionList) {
			if (!(session instanceof XnatMrsessiondata)) {
				continue;
			}
			XnatMrsessiondata mrsession = (XnatMrsessiondata)session;
			if (scanList==null || scanList.size()<1) {
				for (final XnatImagescandataI scanI : mrsession.getScans_scan()) {
					if (scanI instanceof XnatMrscandata) {
						XnatMrscandata scan = (XnatMrscandata)scanI;
						sssMap.put((XnatMrscandata)scan, mrsession);
						returnMap.put((XnatMrscandata) scan, mrsession);
					}
				}
			} else {
				for (final XnatImagescandata scan : scanList) {
					if (!(scan instanceof XnatMrscandata)) {
						continue;
					}
					sssMap.put((XnatMrscandata)scan, mrsession);
					returnMap.put((XnatMrscandata) scan, mrsession);
				}
			}
		}
		return returnMap;
	}

	
	private List<String> processScans(SortedMap<XnatMrscandata, XnatMrsessiondata> subjectScans) throws ReleaseRulesException {
		final List<String> warningList;
	 	if (hasQueryVariable("errorOverride") && getQueryVariable("errorOverride").matches("(?i)^[TY].*$")) {
	 		warningList = releaseRules.applyRulesToScans(subjectScans,getQueryVariable("selectionCriteria"),true);
	 	} else {
	 		warningList = releaseRules.applyRulesToScans(subjectScans,getQueryVariable("selectionCriteria"),false);
	 	}
	 	String dbLabel = hasQueryVariable("label") ? getQueryVariable("label") : subj.getLabel() + "_MR";
	 	String dataRelease = hasQueryVariable("dataRelease") ? getQueryVariable("dataRelease") : "Not Specified";
		for (XnatMrscandata scan : subjectScans.keySet()) {
			scan.setDatarelease(dataRelease);
			scan.setDbsession(dbLabel);
			PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, scan.getItem(),
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.STORE_XML, EventUtils.MODIFY_VIA_WEB_SERVICE, null, null));
				final EventMetaI ci = wrk.buildEvent();
				if (SaveItemHelper.authorizedSave(scan,user,false,true,ci)) {
					PersistentWorkflowUtils.complete(wrk, ci);
				} else {
					PersistentWorkflowUtils.fail(wrk,ci);
				}
			} catch (Exception e) {
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"One or more scans could not be updated. (EXCEPTION=" + e.toString() + ")");
				//System.out.println(scan.getItem().toXML_String());
				e.printStackTrace();
			}
		}
		return warningList;
	}

	@Override
	public ArrayList<String> getDefaultFields(GenericWrapperElement e) {
		final ArrayList<String> al=new ArrayList<String>();
		al.add("xnat_imagescandata_id");
		al.add("ID");
		al.add("type");
		al.add("series_description");
		al.add("quality");
		al.add("xnat_imagescandata_targetForRelease");
		al.add("xnat:imagescandata/targetForRelease");
		al.add("xnat_imagescandata_releaseCountScan");
		al.add("xnat:imagescandata/releaseCountScan");
		al.add("xsiType");
		return al;
	}

	public String getDefaultElementName(){
		return "xnat:imageScanData";
	}

	@Override
	public Representation getRepresentation(Variant variant) {
		if (scanList.size()==1 && scanList.get(0)!=null) {
			final String requested_format = getQueryVariable("format");
			if (requested_format!=null && (requested_format.equalsIgnoreCase("string") || requested_format.equalsIgnoreCase("text") || 
				requested_format.equalsIgnoreCase("plain_text") || requested_format.equalsIgnoreCase("text/plain"))) {
				final Boolean targetForRelease = scanList.get(0).getTargetforrelease();
				final String target = (targetForRelease!=null)?targetForRelease.toString():"";
				return new StringRepresentation(target,MediaType.TEXT_PLAIN);
			} else {
				return new ItemXMLRepresentation(scanList.get(0).getItem(),MediaType.TEXT_XML);
			}
		}else if (sessionList.size()==1){
			final Representation rep=super.getRepresentation(variant);
			if (rep!=null) {
				return rep;
			}

			XFTTable table;
			try {
				final String re=this.getRootElementName();
				final QueryOrganizer qo = new QueryOrganizer(re, user, ViewManager.ALL);
				this.populateQuery(qo);

				final CriteriaCollection cc= new CriteriaCollection("AND");
				cc.addClause(re+"/image_session_id", sessionList.get(0).getId());
				qo.setWhere(cc);

				final String query = qo.buildQuery();

				table=XFTTable.Execute(query, user.getDBName(), userName);

				table = formatHeaders(table, qo, "xnat:imageScanData/ID",
						String.format("/data/experiments/%s/scans/",sessionList.get(0).getId()));
			} catch (Exception e) {
				logger.error("",e);
				getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
				return null;
			}

			final MediaType mt = overrideVariant(variant);
			final Hashtable<String, Object> params = new Hashtable<String, Object>();
			if (table != null) {
				params.put("totalRecords", table.size());
			}
			return this.representTable(table, mt, params);
		} else {
			return new StringRepresentation("NOTE:  This program currently doesn not support reporting on multiple sessions",MediaType.TEXT_PLAIN);
		}
	}

}

	

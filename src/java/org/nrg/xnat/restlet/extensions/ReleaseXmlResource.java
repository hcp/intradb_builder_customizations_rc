package org.nrg.xnat.restlet.extensions;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

import org.nrg.action.ClientException;
import org.nrg.hcp.utils.ReleaseRulesI;
import org.nrg.hcp.utils.ReleaseRulesException;
import org.nrg.hcp.utils.ReleaseRulesUtil;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.HcpvisitHcpvisitdata;
import org.nrg.xdat.om.NtScores;
import org.nrg.xdat.om.HcpToolboxdata;
import org.nrg.xdat.om.HcpSubjectmetadata;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.ItemResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Variant;

@XnatRestlet({"/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/releaseXML","/projects/{PROJECT_ID}/subjects/{SUBJECT_ID}/releaseXml"})
public class ReleaseXmlResource extends ItemResource {
	
	XnatProjectdata proj = null;
	XnatSubjectdata subj = null;
	ArrayList<String> returnList = new ArrayList<String>();
	String combSessLabel = null;
	String combSessDate = null;
	//boolean doPublish = false;
	String dataRelease = null;
	private ReleaseRulesI releaseRules = null;
	
	private static final String MR_SESSION_TYPE = "xnat:mrSessionData";
	private static final String SUBJECT_META_TYPE = "hcp:subjectMetaData";
	private static final String NONTOOLBOX_TYPE = "nt:scores";
	private static final String TOOLBOX_TYPE = "hcp:ToolboxData";
	private static final String VISIT_TYPE = "hcpvisit:HCPVisitData";
	private static final String DEFAULT_TYPE = MR_SESSION_TYPE;
	private static final SimpleDateFormat timeformatter = new SimpleDateFormat("HH:mm:ss");
	private static final SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd");
	

	public ReleaseXmlResource(Context context, Request request,
			Response response) {
		super(context, request, response);

		final String projID = getParameter(request, "PROJECT_ID").toString();
		if (projID != null) {
			proj = XnatProjectdata.getProjectByIDorAlias(projID, user, false);
		}
		
		if (proj == null) {
			this.getResponse().setStatus(
					Status.CLIENT_ERROR_BAD_REQUEST,
					"Project not specified or does not exist (PROJECT_ID="
							+ projID + ")");
			return;
		}
		
		releaseRules = ReleaseRulesUtil.getReleaseRulesForProject(proj,this.getResponse());
		if (releaseRules == null) {
			return;
		}

		final String subjID = getParameter(request, "SUBJECT_ID").toString();
		subj = XnatSubjectdata.GetSubjectByProjectIdentifier(proj.getId(),
				subjID, user, true);

		if (subj == null) {
			subj = XnatSubjectdata.getXnatSubjectdatasById(subjID, user, true);
			if (subj != null
					&& (proj != null && !subj.hasProject(proj.getId()))) {
				subj = null;
			}
		}

		if (subj == null) {
			this.getResponse().setStatus(
					Status.CLIENT_ERROR_BAD_REQUEST,
					"Subject not specified or does not exist (SUBJECT_ID="
							+ subjID + ")");
			return;
		}

		try {
			if (!user.canRead(subj)) {
				this.getResponse()
						.setStatus(
								Status.CLIENT_ERROR_FORBIDDEN,
								"Specified user account has insufficient priviledges for subjects in this project.");
				return;
			}
		} catch (Exception e) {
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,
					"Server threw exception:  " + e.toString());
			return;
		}

		this.getVariants().add(new Variant(MediaType.TEXT_XML));

	}

	@Override
	public boolean allowGet() {
		return true;
	}

	@Override
	public void handleGet() {
		String dataType = DEFAULT_TYPE;
		if (hasQueryVariable("xsiType")) {
			dataType = getQueryVariable("xsiType");
		}
		if (dataType.equals(MR_SESSION_TYPE)) {
			this.setResponseHeader("Cache-Control", "must-revalidate");
			// this.getResponse().setEntity(getMRSessionXML());
			// this.representItem(item, mt);
			try {
				this.returnString(getMRSessionXML(), Status.SUCCESS_OK);
			} catch (ClientException e) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"BAD REQUEST: (" + e.toString() + ")");
			} catch (Exception e) {
				this.getResponse().setStatus(
						Status.SERVER_ERROR_INTERNAL,
						"SERVER ERROR:  Could not build combined session - ("
								+ e.toString() + ")");
			}
			return;
		} else if (dataType.equals(SUBJECT_META_TYPE)) {
			this.setResponseHeader("Cache-Control", "must-revalidate");
			// this.getResponse().setEntity(getMRSessionXML());
			// this.representItem(item, mt);
			try {
				this.returnString(getSubjectMetaXML(), Status.SUCCESS_OK);
			} catch (ClientException e) {
				this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
						"BAD REQUEST: (" + e.toString() + ")");
			} catch (Exception e) {
				this.getResponse().setStatus(
						Status.SERVER_ERROR_INTERNAL,
						"SERVER ERROR:  Could not build combined session - ("
								+ e.toString() + ")");
			}
			return;
		}
		this.getResponse().setStatus(
				Status.CLIENT_ERROR_BAD_REQUEST,
				"Specified xsiType (" + dataType
						+ ") is invalid or not supported by this resource.");
		return;

	}

	private String getSubjectMetaXML() throws Exception {
		final ArrayList<XnatSubjectassessordata> expts = releaseRules.filterExptList(getProjectExpts(subj.getExperiments_experiment(MR_SESSION_TYPE)),getQueryVariable("selectionCriteria"));
		final ArrayList<XnatSubjectassessordata> tboxs = getProjectExpts(subj.getExperiments_experiment(TOOLBOX_TYPE));
		final ArrayList<XnatSubjectassessordata> nts = getProjectExpts(subj.getExperiments_experiment(NONTOOLBOX_TYPE));
		final ArrayList<XnatSubjectassessordata> visits = getProjectExpts(subj.getExperiments_experiment(VISIT_TYPE));
		HcpToolboxdata tboxexpt = toolboxFromList(tboxs);
		NtScores ntexpt = nonToolboxFromList(nts);
		HcpvisitHcpvisitdata visitexpt = visitFromList(visits);
		
		final HcpSubjectmetadata subjectMeta = buildAndSubjectMetaData(expts,tboxexpt,ntexpt,visitexpt);
		final StringWriter strout = new StringWriter();
		subjectMeta.getItem().toXML(strout, false);
		final String rtStr = strout.toString();
		strout.close();
		return rtStr;
	}

	private ArrayList<XnatSubjectassessordata> getProjectExpts(ArrayList<XnatSubjectassessordata> expts) {
		Iterator<XnatSubjectassessordata> i = expts.iterator();
		while (i.hasNext()) {
			XnatSubjectassessordata expt = i.next();
			if (!expt.getProject().equals(proj.getId())) {
				i.remove();
			}
		}
		return expts;
	}

	private HcpToolboxdata toolboxFromList(ArrayList<XnatSubjectassessordata> list) {
		Collections.sort(list,new Comparator<XnatSubjectassessordata>() {
			public int compare(XnatSubjectassessordata arg0, XnatSubjectassessordata arg1) {
				if (arg0 instanceof HcpToolboxdata && !(arg1 instanceof HcpToolboxdata)) {
					return -1;
				} else if (!(arg0 instanceof HcpToolboxdata) && arg1 instanceof HcpToolboxdata) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_toolbox")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_toolbox")) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_tbox")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_tbox")) {
					return 1;
				}	
				return 0;
			}
		});
		if (list.size()>0 && list.get(0) instanceof HcpToolboxdata) {
			return (HcpToolboxdata)list.get(0);
		}
		return null;
	}	

	private NtScores nonToolboxFromList(ArrayList<XnatSubjectassessordata> list) {
		Collections.sort(list,new Comparator<XnatSubjectassessordata>() {
			public int compare(XnatSubjectassessordata arg0, XnatSubjectassessordata arg1) {
				if (arg0 instanceof NtScores && !(arg1 instanceof NtScores)) {
					return -1;
				} else if (!(arg0 instanceof NtScores) && arg1 instanceof NtScores) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_nontoolbox")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_nontoolbox")) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_nt")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_nt")) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_gurscoring")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_gurscoring")) {
					return 1;
				}
				return 0;
			}
		});
		if (list.size()>0 && list.get(0) instanceof NtScores) {
			return (NtScores)list.get(0);
		}
		return null;
	}	

	private HcpvisitHcpvisitdata visitFromList(ArrayList<XnatSubjectassessordata> list) {
		Collections.sort(list,new Comparator<XnatSubjectassessordata>() {
			public int compare(XnatSubjectassessordata arg0, XnatSubjectassessordata arg1) {
				if (arg0 instanceof HcpvisitHcpvisitdata && !(arg1 instanceof HcpvisitHcpvisitdata)) {
					return -1;
				} else if (!(arg0 instanceof HcpvisitHcpvisitdata) && arg1 instanceof HcpvisitHcpvisitdata) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_visit")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_visit")) {
					return 1;
				} else if (arg0.getLabel().toLowerCase().endsWith("_hcp")) {
					return -1;
				} else if (arg1.getLabel().toLowerCase().endsWith("_hcp")) {
					return 1;
				}
				return 0;
			}
		});
		if (list.size()>0 && list.get(0) instanceof HcpvisitHcpvisitdata) {
			return (HcpvisitHcpvisitdata)list.get(0);
		}
		return null;
	}	

	private HcpSubjectmetadata buildAndSubjectMetaData(ArrayList<XnatSubjectassessordata> expts, HcpToolboxdata tboxexpt, NtScores ntexpt, HcpvisitHcpvisitdata visitexpt) throws ReleaseRulesException {
		final HcpSubjectmetadata subjMeta = new HcpSubjectmetadata();
		if (hasQueryVariable("exptID")) {
			subjMeta.setId(getQueryVariable("exptID"));
		}
		if (hasQueryVariable("date")) {
			final String dateStr = getQueryVariable("date");
			try {
				parseAndSetDate(dateStr, subjMeta);
			} catch (Exception e) {
				subjMeta.setDate(null);

			}
		} else {
			subjMeta.setDate(null);
		}
		final SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap = getOrderedScanMap(expts);
	 	boolean errOverride = (hasQueryVariable("errorOverride") && getQueryVariable("errorOverride").matches("(?i)^[TY].*$"));
	 	releaseRules.applyRulesToSubjectMetaData(subjMeta,scanMap,getQueryVariable("selectionCriteria"),tboxexpt,ntexpt,visitexpt,errOverride);
		return subjMeta;
	}

	private String getMRSessionXML() throws Exception {
		final ArrayList<XnatSubjectassessordata> expts = releaseRules.filterExptList(getProjectExpts(subj.getExperiments_experiment(MR_SESSION_TYPE)),getQueryVariable("selectionCriteria"));
		final XnatMrsessiondata combSess = buildAndReturnCombinedSession(expts);
		final StringWriter strout = new StringWriter();
		combSess.getItem().toXML(strout, false);
		final String rtStr = strout.toString();
		strout.close();
		return rtStr;
	}

	private XnatMrsessiondata buildAndReturnCombinedSession(ArrayList<XnatSubjectassessordata> expts) throws Exception {
		final XnatMrsessiondata combSess = new XnatMrsessiondata();
		if (hasQueryVariable("exptID")) {
			combSess.setId(getQueryVariable("exptID"));
		}
		if (hasQueryVariable("date")) {
			final String dateStr = getQueryVariable("date");
			try {
				parseAndSetDate(dateStr, combSess);
			} catch (Exception e) {
				combSess.setDate(null);

			}
		} else {
			combSess.setDate(null);
		}
		combSess.setDcmpatientname(subj.getLabel());
		combSess.setDcmpatientid(combSessLabel);
		combSess.setDcmpatientbirthdate(null);
		
		boolean anyRelease = false;
		// Per meeting 2012-12-18, ordering all scans by acquisition time
		final SortedMap<XnatMrscandata, XnatMrsessiondata> scanMap = getOrderedScanMap(expts);
		XnatMrsessiondata prevSess = null;
		String intraLabel = null;
		for (final XnatMrscandata intraScan : scanMap.keySet()) {
			final XnatMrsessiondata intraSess = scanMap.get(intraScan);
			if (intraLabel == null) {
				intraLabel = intraScan.getDbsession();
			}
			if (!(intraSess == null || intraSess.equals(prevSess))) {
				prevSess = intraSess;
				if (combSess.getAcquisitionSite() == null
						|| combSess.getAcquisitionSite().length() < 1) {
					combSess.setAcquisitionSite((String) getCombinedAttr(
							combSess.getAcquisitionSite(),
							intraSess.getAcquisitionSite(), "Acqusition Site"));
				}
				if (combSess.getScanner() == null
						|| combSess.getScanner().length() < 1) {
					combSess.setScanner((String) getCombinedAttr(
							combSess.getScanner(), intraSess.getScanner(),
							"Scanner"));
				}
				if (combSess.getOperator() == null
						|| combSess.getOperator().length() < 1) {
					combSess.setOperator((String) getCombinedAttr(
							combSess.getOperator(), intraSess.getOperator(),
							"Operator"));
				}
			}

			// TO BE SET LATER
			// combSess.setId()
			// whole data release
			// Should vary - leave blank
			// combSess.setDcmaccessionnumber()
			// combSess.setDcmpatientid()
			// combSess.setDcmpatientname()
			// combSess.setTime()
			// Not typically set - leave blank
			// combSess.setDuration()
			// HIPAA: Leave blank
			// combSess.setDcmpatientbirthdate()

			// continue only if targeted for export
			final Boolean isTarget = intraScan.getTargetforrelease();
			final Integer isOverride;
			if (!releaseRules.isStructuralScan(intraScan) || intraScan.getReleaseoverride()==null) {
				isOverride = 0;
			} else {
				isOverride = intraScan.getReleaseoverride();
			}
			// Override is currently only applicable for rated structural scans 
			if (!((isOverride>0) || (isOverride>=0 && (isTarget!=null && isTarget)))) {
				continue;
			}
			anyRelease = true;
			final XnatMrscandata dbScan;
			dbScan = intraScan;
			try {
				dbScan.setId(dbScan.getDbid());
			} catch (NumberFormatException e) {
				throw new ClientException("ERROR:  Couldn't generate scan ID", e);
			}
			dbScan.setImageSessionId(combSess.getId());
			dbScan.setDatarelease(dataRelease);
			dbScan.setReleasecountscan(null);
			dbScan.setReleasecountscanoverride(null);
			dbScan.setTargetforrelease(null);
			dbScan.setReleaseoverride(null);
			dbScan.setDbsession(null);
			dbScan.setDbid(null);
			// Keep Intradb description for now
			String idbDesc = dbScan.getSeriesDescription();
			dbScan.setSeriesDescription(dbScan.getDbdesc());
			dbScan.setDbdesc(idbDesc);
			dbScan.setType(dbScan.getDbtype());
			dbScan.setDbtype(null);
			// Per meeting, for now we won't populate scan notes
			dbScan.setNote(null);
			combSess.setScans_scan(dbScan.getItem());
			// Need to remove file objects from dbScan to permit upload (references Intradb file path)
			while (true) {
				try { 
				   dbScan.removeFile(0);
			    } catch (Exception e) {
			    	break;
			    }
			}
		}
		if (hasQueryVariable("label")) {
			combSess.setLabel(getQueryVariable("label"));
		} else if (intraLabel!=null && intraLabel.length()>0) {
			combSess.setLabel(intraLabel);
			
		} else {
			combSess.setLabel(subj.getLabel() + "_MR");
		}
		if (!anyRelease) {
			throw new ClientException(
					"ERROR:  Session includes no scans marked for export");
		}
		return combSess;
	}
		
	/*
	private Map<XnatMrscandata, XnatMrsessiondata> getScanMap(ArrayList<XnatSubjectassessordata> expts) {
		final HashMap<XnatMrscandata,XnatMrsessiondata> returnMap = new HashMap<XnatMrscandata,XnatMrsessiondata>();
		for (final XnatSubjectassessordata expt : expts) {
			if (expt instanceof XnatMrsessiondata) {
				final XnatMrsessiondata mrSess = (XnatMrsessiondata) expt;
				for (final XnatImagescandataI scanI : mrSess.getScans_scan()) {
					if (scanI instanceof XnatMrscandata) {
						returnMap.put((XnatMrscandata)scanI, mrSess);
					}
				}
			}
		}
		return returnMap;
	}
	*/

	private SortedMap<XnatMrscandata, XnatMrsessiondata> getOrderedScanMap(
			ArrayList<XnatSubjectassessordata> expts) {
		
		final HashMap<XnatImagescandata,XnatImagesessiondata> sssMap = new HashMap<XnatImagescandata,XnatImagesessiondata>();
		
		final Comparator<XnatImagescandata> scanCompare = new Comparator<XnatImagescandata>() {
		
			@Override
			public int compare(XnatImagescandata arg0, XnatImagescandata arg1) {
				try {
					arg0.getImageSessionData();
					final Date d0 = dateformatter.parse(sssMap.get(arg0).getDate().toString());
					final Date d1 = dateformatter.parse(sssMap.get(arg1).getDate().toString());
					if (d0.before(d1)) {
						return -1;
					} else if (d1.before(d0)) {
						return 1;
					} 
				} catch (Exception e) {
					// Do nothing for now
				}
				try {
					final Date t0 = timeformatter.parse(arg0.getStarttime().toString());
					final Date t1 = timeformatter.parse(arg1.getStarttime().toString());
					if (t0.before(t1)) {
						return -1;
					} else if (t1.before(t0)) {
						return 1;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
				try {
					if (Integer.parseInt(arg0.getId()) <  Integer.parseInt(arg1.getId())) {
						return -1;
					} else if (Integer.parseInt(arg1.getId()) <  Integer.parseInt(arg0.getId())) {
						return 1;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
				return 0;
			}
		};
		
		final SortedMap<XnatMrscandata, XnatMrsessiondata> returnMap = new TreeMap<XnatMrscandata,XnatMrsessiondata>(scanCompare);
		for (final XnatSubjectassessordata expt : expts) {
			if (expt instanceof XnatMrsessiondata) {
				final XnatMrsessiondata mrSess = (XnatMrsessiondata) expt;
				for (final XnatImagescandataI scanI : mrSess.getScans_scan()) {
					if (scanI instanceof XnatMrscandata) {
						sssMap.put((XnatImagescandata)scanI, mrSess);
						returnMap.put((XnatMrscandata) scanI, mrSess);
					}
				}
			}
		}
		return returnMap;
	}

	private void parseAndSetDate(String dateStr, XnatSubjectassessordata sess) {
		// Per Sandy, 2013-01-17, do not set anything as a session date
		sess.setDate(null);
		/*
		final String[] dateArr = dateStr.split("[.\\-/]");
		final Calendar cal = Calendar.getInstance();
		if (dateArr.length >= 3) {
			if (Integer.parseInt(dateArr[0]) > 1900) {
				cal.set(Integer.parseInt(dateArr[0]),
						Integer.parseInt(dateArr[1]) - 1,
						Integer.parseInt(dateArr[2]), 0, 0, 0);
			} else if (Integer.parseInt(dateArr[2]) > 1900) {
				cal.set(Integer.parseInt(dateArr[2]),
						Integer.parseInt(dateArr[0]) - 1,
						Integer.parseInt(dateArr[1]), 0, 0, 0);
			}
		} else {
			if (dateStr.length() == 8) {
				if (Integer.parseInt(dateStr.substring(0, 4)) > 2000) {
					cal.set(Integer.parseInt(dateStr.substring(0, 4)),
							Integer.parseInt(dateStr.substring(4, 6)) - 1,
							Integer.parseInt(dateStr.substring(6)), 0, 0, 0);

				} else if (Integer.parseInt(dateStr.substring(4)) > 2000) {
					cal.set(Integer.parseInt(dateStr.substring(4)),
							Integer.parseInt(dateStr.substring(0, 2)) - 1,
							Integer.parseInt(dateStr.substring(2, 4)), 0, 0, 0);
				}
			}
		}
		sess.setDate(cal.getTime());
		*/
	}

	private Object getCombinedAttr(Object combVal, Object mrVal,
			String attrString) {
		if (combVal == null) {
			combVal = mrVal;
		} else if (!combVal.equals(mrVal)) {
			final String returnString = "WARNING:  " + attrString
					+ " value differs between sessions (CURRENT="
					+ mrVal.toString() + ",PREVIOUS=" + combVal.toString()
					+ ").";
			if (!returnList.contains(returnString)) {
				returnList.add(returnString);
			}
		}
		return combVal;
	}
	
}

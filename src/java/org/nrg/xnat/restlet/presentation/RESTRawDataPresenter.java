//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Copyright (C) 2005 Washington University
 */
/*
 * Created on Jan 5, 2005
 *
 */
package org.nrg.xnat.restlet.presentation;
import java.sql.Timestamp;
import java.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.collections.DisplayFieldCollection.DisplayFieldNotFoundException;
import org.nrg.xdat.display.*;
import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.search.DisplayFieldWrapper;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.SecurityValues;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.XFTTableI;
import org.nrg.xft.db.ViewManager;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.utils.StringUtils;
/**
 * @author Tim
 *
 */
public class RESTRawDataPresenter extends PresentationA {
    static org.apache.log4j.Logger logger = Logger.getLogger(RESTRawDataPresenter.class);
    public String getVersionExtension(){return "";}
    private String server = null;
    private boolean clickableHeaders = true;
    String searchURI=null;
    XDATUser user=null;
    public String sortBy=null;
    public int tier = 0;
    private boolean keepXsiNames = false;

    public RESTRawDataPresenter(String serverLocal, boolean canClickHeaders,String searchURI,XDATUser u,String sortBy)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        clickableHeaders = canClickHeaders;
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
    }

    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        clickableHeaders = true;
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
    }

    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy, int tier, boolean keepXsiNames)
    {
    	this(serverLocal,searchURI,u,sortBy,tier);
    	this.keepXsiNames = keepXsiNames;
    }

    public RESTRawDataPresenter(String serverLocal,String searchURI,XDATUser u,String sortBy, int tier)
    {
        server = serverLocal;
        if (! server.endsWith("/"))
            server = server + "/";
        clickableHeaders = true;
        this.searchURI=searchURI;
        this.user=u;
        this.sortBy=sortBy;
        this.tier = tier;
    }


    public XFTTableI formatTable(XFTTableI table, DisplaySearch search) throws Exception
    {
        return formatTable(table,search,true);
    }
    
    public XFTTableI formatTable(final XFTTableI table,final DisplaySearch search,final boolean allowDiffs) throws Exception {
    	return formatTable(table,search,allowDiffs,true);
    }

    public XFTTableI formatTable(final XFTTableI table, final DisplaySearch search,final boolean allowDiffs,final boolean returnVisibleOnly) throws Exception
    {
        logger.debug("BEGIN HTML FORMAT");
        XFTTable csv = new XFTTable();
        final ElementDisplay ed = DisplayManager.GetElementDisplay(getRootElement().getFullXMLName());
        final List<DisplayFieldReferenceI> visibleFields = search.getVisibleFields(this.getVersionExtension());

        //need to remove fields that aren't visible (XDAT code needs to be fixed)
        CollectionUtils.filter(visibleFields, new Predicate() {
            @Override
            public boolean evaluate(Object arg0) {
                if(arg0 instanceof DisplayFieldWrapper){
                    try {
                        return ( !returnVisibleOnly || ((DisplayFieldWrapper)arg0).isVisible());
                    } catch (DisplayFieldNotFoundException e) {
                    }
                }
                return true;
            }
        });


		final ArrayList columnHeaders = new ArrayList();
		ArrayList diffs = new ArrayList();
		
		if (search.getInClauses().size()>0)
		{
		    for(int i=0;i<search.getInClauses().size();i++)
		    {
		        columnHeaders.add("");
		    }
		}
		
		//POPULATE HEADERS
		
		Iterator fields = visibleFields.iterator();
		int counter = search.getInClauses().size();
		while (fields.hasNext())
		{
			DisplayFieldReferenceI df = (DisplayFieldReferenceI)fields.next();
			if (allowDiffs)
			{
				if (!diffs.contains(df.getElementName()))
				{
				    diffs.add(df.getElementName());
				    SchemaElementI foreign = SchemaElement.GetElement(df.getElementName());
				    if (search.isMultipleRelationship(foreign))
				    {
					    String temp = StringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
					    Integer index = ((XFTTable)table).getColumnIndex(temp);
					    if (index!=null)
					    {
						    columnHeaders.add((keepXsiNames) ? df.getId() : "Diff");
					    }
				    }
				}
			}
			
			if (!df.isHtmlContent())
			{
				String value = df.getHeader();
				if ("ID".equals(value)){
					// Workaround for MS SYLK issue:
					// http://support.microsoft.com/kb/215591
					// http://nrg.wustl.edu/fogbugz/default.php?418
					value = value.toLowerCase();
				}
				columnHeaders.add((keepXsiNames) ? df.getId() : value);
			}
		}
		csv.initTable(columnHeaders);
		
		//POPULATE DATA
		table.resetRowCursor();
		
		while (table.hasMoreRows())
		{
			Hashtable row = table.nextRowHash();
			Object[] newRow = new Object[columnHeaders.size()];
			fields = visibleFields.iterator();

			diffs = new ArrayList();
			if (search.getInClauses().size()>0)
			{
			    for(int i=0;i<search.getInClauses().size();i++)
			    {
			        Object v = row.get("search_field"+i);
			        if (v!=null)
			        {
				        newRow[i] = v;
			        }else{
				        newRow[i] = "";
			        }
			    }
			}
			
			counter = search.getInClauses().size();
			while (fields.hasNext())
			{
			    DisplayFieldReferenceI dfr = (DisplayFieldReferenceI)fields.next();
				if(!dfr.isHtmlContent()) {

					try {
					    if (allowDiffs)
					    {
		                    if (!diffs.contains(dfr.getElementName()))
		                    {
		                        diffs.add(dfr.getElementName());
		                        SchemaElementI foreign = SchemaElement.GetElement(dfr.getElementName());
		                        if (search.isMultipleRelationship(foreign))
		                        {
		                    	    String temp = StringUtils.SQLMaxCharsAbbr(search.getRootElement().getSQLName() + "_" + foreign.getSQLName() + "_DIFF");
		                    	    Integer index = ((XFTTable)table).getColumnIndex(temp);
		                    	    if (index!=null)
		                    	    {
		                    		    String diff = "";
		                    		    Object d = row.get(temp.toLowerCase());
		                    	        if (d!=null)
		                    	        {
		                    		        diff=  (String)d.toString();
		                    	        }else{
		                    	            diff="";
		                    	        }
		                    		    newRow[counter++]=diff;
		                    	    }
		                        }
		                    }
					    }
	                    
	                    Object v = null;
	                    if (dfr.getElementName().equalsIgnoreCase(search.getRootElement().getFullXMLName()))
	                    {
	                    	v = row.get(dfr.getRowID().toLowerCase());
	                    }else{
	                    	v = row.get(dfr.getElementSQLName().toLowerCase() + "_" + dfr.getRowID().toLowerCase());
	                    }
	                    if (v != null)
	                    {
	                    	newRow[counter] = v;
	                    }
	                } catch (XFTInitException e) {
	                    logger.error("",e);
	                } catch (ElementNotFoundException e) {
	                    logger.error("",e);
	                }

					counter++;
				}
			}
			csv.insertRow(newRow);
		}
		
		logger.debug("END RAW DATA FORMAT");
        return csv;
    }
}

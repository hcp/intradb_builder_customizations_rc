

var evPhysioURI;
var _lduwait;

////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////
// RERUN EV/PHYSIO GENERATION METHODS //
////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////

function rerunEvPhysio() {
	_lduwait = true;
	projectCheckURI = window.location.protocol + "//" + window.location.host + "/REST/config/linkedDataUploader/project.lst?version=current&contents=true";
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(projectCheckURI), this.lduProjectCheckCallback, null, this);
}


function doRunEvPhysio() {
		
		this.confirm = xModalConfirm;
		this.confirm.okAction = function(){ 
			lduxModalRunningOpen();
			var workflowURI = evPhysioURI + '?XNAT_CSRF=' + csrfToken + '&returnStatusText=true'; 
			workflowURI+="&ev=" + document.getElementById('_doRunEV').checked;
			workflowURI+="&physio=" + document.getElementById('_doRunPhysio').checked;
			workflowURI+="&sendEmail=" + document.getElementById('_doSendEmail').checked;
			workflowURI+="&runSanityChecks=" + document.getElementById('_doRunSanityChecks').checked;
			YAHOO.util.Connect.asyncRequest('PUT', encodeURI(workflowURI), this.lduRerunCallback, null, this);
		 }
		this.confirm.content  = '<p><h4>(Re)run EV/PhysioTxt file generation?</h4></p>' + 
					'<div style="padding-left:10px">' +
					'<br/><input type="checkbox" id="_doRunEV" checked> generate EV Files' +
					'<br/><input type="checkbox" id="_doRunPhysio" checked> generate Physio Files' +
					'<br/><input type="checkbox" id="_doRunSanityChecks" checked> run sanity checks (if configured)' + 
					'<br/><input type="checkbox" id="_doSendEmail"> send e-mail upon completion' +
					'</div>' 
					;
		this.confirm.cancelAction = function(){ return; };
		this.confirm.width = 280;
		this.confirm.height = 290;
		xModalOpenNew(this.confirm);

}

this.lduCheckOK = function(o){ 
	projList = o.responseText.replace(/[\W]/g,':').split(':');
	for (var i=0;i<projList.length;i++) {
		if (projList[i].length>0) {
			if (window.projectScope == projList[i]) {
				doRunEvPhysio();
				return;
			}
		}
	}
	xModalMessage('Error','The linked data uploader has not been configured for this project.','OK', null);
}
this.lduCheckFailed = function(o){ 
	ckRun();
}

this.lduProjectCheckCallback = {
	success : lduCheckOK,
	failure : lduCheckFailed,
	scope : this
};

this.lduRerunOK = function(o){ 
	if (_lduwait) {
		xModalMessage('Processing complete',o.responseText,'OK', { width: 760, height: 460 /*, action: function(){window.location.reload();}*/ });
		xModalCloseNew("lduRunningModal",'');
	}
}
		
this.lduRerunFailed = function(o){
	if (_lduwait) {
		xModalMessage('Error','An unexpected error has occured. Please contact your administrator.' + "<br/><br/>   STATUS:  " + o.status + "<br/><br/>   " + o.responseText,'OK', null);
		xModalCloseNew("lduRunningModal",'');
		_lduwait = false;
	}
}

this.lduRerunCallback = {
	success : lduRerunOK,
	failure : lduRerunFailed,
	scope : this
};



/////////////////////
/////////////////////
/////////////////////
// UTILITY METHODS //
/////////////////////
/////////////////////
/////////////////////

function lduxModalRunningOpen(){

	lduRunningModal = {
	    id: 'lduRunningModal',
	    kind: 'fixed',
	    width: 260,
	    height: 132,
	    title: 'Running EV/Physio file generation',
	    content: '<img src="'+serverRoot+'/images/loading_bar.gif" alt="loading">',
	    ok: 'show',
	    okLabel: 'Don\'t Wait',
	    okAction: function(){_lduwait = false;},
	    okClose: 'yes',
	    cancel: 'hide'
		};
	xModalOpenNew(lduRunningModal);

}


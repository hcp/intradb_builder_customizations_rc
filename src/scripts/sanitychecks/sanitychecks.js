
var sanityCheckURI;
var omID;

function acceptFail(scanno) {
	if (scanno==null) {
		alert("Accept session-level failure (SESSION=" + omID + ")");
	} else {
		alert("Accept scan-level failure (SESSION=" + omID + ", SCAN=" + scanno + ")");
	}
}

function markUnusable(scanno) {
	alert("Scan marked unusable (SESSION=" + omID + ", SCAN=" + scanno + ")");
}

function doShowBtn(str) {
	if (str.length>0) {
		$('#regSubmit').removeClass('disabled'); 
	} else {
		$('#regSubmit').addClass('disabled'); 
	}
}

//////////////////////////
//////////////////////////
//////////////////////////
// ACCEPT CHECK METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

function acceptThis(v) {

	<!-- (re)initialize registration form -->
	$('#action-all-div').addClass('hidden'); 
	$('#action-div').removeClass('hidden'); 
	$('#regSubmit').addClass('disabled');
	$('.required_text').removeClass('hidden');
	if (v!=null) {
	   $('#scanSpan').html(document.getElementById("descspan" + v).innerHTML);
	   var failspan = document.getElementById("failspan" + v).innerHTML;
	   failspan = failspan.replace(/[,\s]*$/,"");
	   $('#checkSpan').html(failspan);
	} else {
	   //$('#scanSpan').html(document.getElementById("descspan").innerHTML);
	   //var failspan = document.getElementById("failspan").innerHTML;
	}
	
        var action_modal_opts = {
            id: 'action_modal',
            width: 560,
            height: 285,
            title: 'Accept Failed Sanity Check?',
            content: 'existing',
            ok: 'show',
            okLabel: 'Register',
            okAction: function(){
		doAccept(v,document.getElementById('justification').value);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            },
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();
	$('#action_modal .footer').css('background-color','#ffffff');

}
this.acceptOK = function(){ 
	getOverallStatus();
	xModalLoadingClose();
	if (this.acceptCallback.scan!=null) {
		$('#currentdiv_' + this.acceptCallback.scan).html(document.getElementById("accepteddiv_" + this.acceptCallback.scan).innerHTML);
		$('#currentdiv2_' + this.acceptCallback.scan).html(document.getElementById("accepteddiv2_" + this.acceptCallback.scan).innerHTML);
	} else {
		$('#currentdiv').html(document.getElementById("accepteddiv").innerHTML);
	}
}
		
this.acceptFailed = function(){
	xModalLoadingClose();
	xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

this.acceptCallback = {
	scan : "X",
	success : acceptOK,
	failure : acceptFailed,
	scope : this
};

function doAccept(v,justification) {
	xModalLoadingOpen({title:'Please wait...'});
	if (v!=null) {
		var workflowURI = sanityCheckURI + '/acceptScanChecks?XNAT_CSRF=' + csrfToken + '&scanList=' + v + '&justification=' + encodeURIComponent(justification);
		this.acceptCallback.scan=v;
	} else {
		var workflowURI = sanityCheckURI + 'acceptSessionChecks?XNAT_CSRF=' + csrfToken + '&justification=' + encodeURIComponent(justification);
		this.acceptCallback.scan=null;
	}
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), this.acceptCallback, null, this);
}


//////////////////////////
//////////////////////////
//////////////////////////
// ACCEPT ALL METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

function acceptAll() {

	<!-- (re)initialize registration form -->
	$('#action-div').addClass('hidden'); 
	$('#action-all-div').removeClass('hidden'); 
	$('#regSubmit').addClass('disabled');
	$('.required_text').removeClass('hidden');
	
        var action_modal_opts = {
            id: 'action_modal',
            width: 560,
            height: 285,
            title: 'Accept Failed Sanity Checks?',
            content: 'existing',
            ok: 'show',
            okLabel: 'Register',
            okAction: function(){
		doAcceptAll(document.getElementById('justification').value);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            },
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();
	$('#action_modal .footer').css('background-color','#ffffff');

}
this.acceptAllOK = function(){ 
	window.location.reload();
}
		
this.acceptAllFailed = function(){
	xModalLoadingClose();
	xModalMessage('Error','This functionality is not yet implemented.','OK', null);
	//xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

this.acceptAllCallback = {
	success : acceptAllOK,
	failure : acceptAllFailed,
	scope : this
};

function doAcceptAll(justification) {
	xModalLoadingOpen({title:'Please wait...'});
	var workflowURI = sanityCheckURI + '/acceptAllChecks?XNAT_CSRF=' + csrfToken + '&justification=' + encodeURIComponent(justification);
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), this.acceptAllCallback, null, this);
}

///////////////////////////
///////////////////////////
///////////////////////////
// MARK UNUSABLE METHODS //
///////////////////////////
///////////////////////////
///////////////////////////

function markThisUnusable(v) {

	<!-- (re)initialize registration form -->
	$('#action-all-div').addClass('hidden'); 
	$('#action-div').removeClass('hidden'); 
	$('#regSubmit').addClass('disabled');
	$('.required_text').removeClass('hidden');
	if (v!=null) {
	   $('#scanSpan').html(document.getElementById("descspan" + v).innerHTML);
	   var failspan = document.getElementById("failspan" + v).innerHTML;
	   failspan = failspan.replace(/[,\s]*$/,"");
	   $('#checkSpan').html(failspan);
	} else {
	   //$('#scanSpan').html(document.getElementById("descspan").innerHTML);
	   //var failspan = document.getElementById("failspan").innerHTML;
	}
	
        var action_modal_opts = {
            id: 'action_modal',
            width: 560,
            height: 285,
            title: 'Mark scan unusable?',
            content: 'existing',
            ok: 'show',
            okLabel: 'Register',
            okAction: function(){
		doMarkUnusable(v);
		document.getElementById('justification').value="";
            },
            cancel: 'show',
            cancelLabel: 'Cancel',
            cancelAction: function(){
		document.getElementById('justification').value="";
            },
        };

        xModalOpenNew(action_modal_opts);
	$("#justification").focus();
	$('#action_modal .footer').css('background-color','#ffffff');

}
this.markUnusableOK = function(){ 
	xModalLoadingClose();
	if (this.markUnusableCallback.scan!=null) {
		$('#currentdiv_' + this.markUnusableCallback.scan).html(document.getElementById("unusablediv_" + this.markUnusableCallback.scan).innerHTML);
		$('#currentdiv2_' + this.markUnusableCallback.scan).html(document.getElementById("unusablediv2_" + this.markUnusableCallback.scan).innerHTML);
	} 
	getOverallStatus();
}
		
this.markUnusableFailed = function(){
	getOverallStatus();
	xModalLoadingClose();
	xModalMessage('Error','An unexpected error has occured. Please contact your administrator.','OK', null);
}

this.markUnusableCallback = {
	scan : "X",
	success : markUnusableOK,
	failure : markUnusableFailed,
	scope : this
};

function doMarkUnusable(v) {
	xModalLoadingOpen({title:'Please wait...'});
	if (v!=null) {
		var workflowURI = sanityCheckURI + '/markScansUnusable?XNAT_CSRF=' + csrfToken + '&scanList=' + v;
		this.markUnusableCallback.scan=v;
		YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), this.markUnusableCallback, null, this);
	} 
}

//////////////////////////
//////////////////////////
//////////////////////////
// RERUN CHECKS METHODS //
//////////////////////////
//////////////////////////
//////////////////////////

function rerunChecks(toUri) {
	_wait = true;
	projectCheckURI = window.location.protocol + "//" + window.location.host + "/REST/config/sanityChecks/project.lst?version=current&contents=true";
	this.projectCheckCallback.toUri = toUri;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(projectCheckURI), this.projectCheckCallback, null, this);
}


function ckRun(toUri) {
		
		this.confirm = xModalConfirm;
		this.confirm.okAction = function(){ 
			xModalRunningOpen();
			var workflowURI = sanityCheckURI + '/runSanityChecks?XNAT_CSRF=' + csrfToken;
			YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), this.rerunCallback, null, this);
			this.projectCheckCallback.toUri = toUri;
		 }
		this.confirm.content  = '<p>Run sanity checks?</p>';
		this.confirm.cancelAction = function(){ return; };
		this.confirm.width = 260;
		this.confirm.height = 132;
		xModalOpenNew(this.confirm);

}

this.checkOK = function(o){ 
	projList = o.responseText.replace(/[\W]/g,':').split(':');
	for (var i=0;i<projList.length;i++) {
		if (projList[i].length>0) {
			if (window.projectScope == projList[i]) {
				ckRun(this.projectCheckCallback.toUri);
				return;
			}
		}
	}
	xModalMessage('Error','Sanity checks have not been configured for this project.','OK', null);
}
this.checkFailed = function(o){ 
	ckRun(this.projectCheckCallback.toUri);
}

this.projectCheckCallback = {
	success : checkOK,
	success : checkOK,
	failure : checkFailed,
	scope : this
};

this.rerunOK = function(o){ 
	if (_wait) {
		if (this.projectCheckCallback.toUri != undefined) {
			window.location.assign(window.location.protocol + "//" + window.location.host + this.projectCheckCallback.toUri);
		} else {
			window.location.reload();
		}
	}
}
		
this.rerunFailed = function(o){
	if (_wait) {
		xModalMessage('Error','An unexpected error has occured. Please contact your administrator.' + "<br/><br/>   STATUS:  " + o.status + "<br/><br/>   " + o.responseText,'OK', null);
		xModalCloseNew("runningModal",'');
		_wait = false;
	}
}

this.rerunCallback = {
	success : rerunOK,
	failure : rerunFailed,
	scope : this
};


///////////////////////////
///////////////////////////
///////////////////////////
// REQUEST EMAIL METHODS //
///////////////////////////
///////////////////////////
///////////////////////////

function requestEmail() {
	_wait = false;
	var emailURI = sanityCheckURI + '/emailOnRun?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(emailURI), this.requestEmailCallback, null, this);
}
this.requestEmailOK = function(o){ 
	alert("You will be sent an e-mail upon completion");
}
		
this.requestEmailFailed = function(o){ }

this.requestEmailCallback = {
	success : requestEmailOK,
	failure : requestEmailFailed,
	scope : this
};

////////////////////////////
////////////////////////////
////////////////////////////
// OVERALL STATUS METHODS //
////////////////////////////
////////////////////////////
////////////////////////////

var _currentOverallStatus;

var updateModal = {
        id: 'updateModal',
        kind: 'fixed',  // REQUIRED - options: 'dialog','fixed','large','med','small','xsmall','custom'
        width: 420, // width in px - used for 'fixed','custom','static'
        height: 220, // height in px - used for 'fixed','custom','static'
        scroll: 'yes', // true/false - does content need to scroll?
        title: 'Message', // text for title bar
	content: "WARNING:  The overall status appears to be out of date (this can occur, for example, if a failed scan has been set to unusable outside this report). <br/></br><b>Update Overal Status?</b>",
        // footer not specified, will use default footer
        ok: 'show',  // show the 'ok' button
        okLabel: 'Update Overall Status',
        okAction: function(){ updateOverallStatus();},
        cancel: 'show' // do NOT show the 'Cancel' button
};

this.calculateStatusOK = function(o){ 
	if (o.responseText!=_currentOverallStatus) {
	    xModalOpenNew(updateModal);
	}
}
		 
this.calculateStatusFailed = function(){
	// Do nothing for now
}

this.calculateStatusCallback = {
	success : function(o){ calculateStatusOK(o) },
	failure : calculateStatusFailed,
	scope : this
};

function calculateOverallStatus() {
	var workflowURI = sanityCheckURI + '/calculateOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(workflowURI), this.calculateStatusCallback, null, this);
}

this.updateStatusOK = function(o){ 
	getOverallStatus();
}
		
this.updateStatusFailed = function(){
	xModalMessage('Warning','Couldn\'t update overall status.','OK', null);
}

this.updateStatusCallback = {
	success : function(o){ updateStatusOK(o) },
	failure : updateStatusFailed,
	scope : this
};

function updateOverallStatus() {
	var workflowURI = sanityCheckURI + '/updateOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('POST', encodeURI(workflowURI), this.updateStatusCallback, null, this);
}

this.overallStatusOK = function(o){ 
	_currentOverallStatus = o.responseText;
	$('#overallStatusSpan').html('<font class="status field' + o.responseText + '">' + o.responseText + "</font>");
	calculateOverallStatus();
}
		
this.overallStatusFailed = function(){
	xModalMessage('Warning','Couldn\'t update overall status.','OK', null);
}

this.overallStatusCallback = {
	success : function(o){ overallStatusOK(o) },
	failure : overallStatusFailed,
	scope : this
};

function getOverallStatus() {
	var workflowURI = sanityCheckURI + '/getOverallStatus?XNAT_CSRF=' + csrfToken;
	YAHOO.util.Connect.asyncRequest('GET', encodeURI(workflowURI), this.overallStatusCallback, null, this);
}

/////////////////////
/////////////////////
/////////////////////
// UTILITY METHODS //
/////////////////////
/////////////////////
/////////////////////

var _wait;

function xModalRunningOpen(){

	runningModal = {
	    id: 'runningModal',
	    kind: 'fixed',
	    width: 260,
	    height: 132,
	    title: 'Running checks',
	    content: '<img src="'+serverRoot+'/images/loading_bar.gif" alt="loading">',
	    ok: 'show',
	    okLabel: 'Don\'t Wait',
	    okAction: function(){_wait = false;requestEmail();},
	    okClose: 'yes',
	    cancel: 'hide'
		};
	xModalOpenNew(runningModal);

}

